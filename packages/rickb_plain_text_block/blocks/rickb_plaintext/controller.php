<?php  
defined('C5_EXECUTE') or die("Access Denied.");

Loader::block('library_file');

class RickbPlaintextBlockController extends BlockController {

	protected $btTable = 'btContentPlainText';
	protected $btInterfaceWidth = "600";
	protected $btInterfaceHeight = "465";
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
	
	public $content = "";	
	
	public function getBlockTypeDescription() {
		return t("For adding plain text (e.g. computer program source code) to a page.");
	}
	
	public function getBlockTypeName() {
		return t("Plain Text");
	}	 
	
	public function __construct($obj = null) {		
		parent::__construct($obj); 
	}
	
	public function view(){ 
		$this->set('content', $this->content); 
	} 
	
	public function getSearchableContent() {
		return $this->content;
	}
	
	public function save($data) { 
		$args['content'] = isset($data['content']) ? $data['content'] : '';
		parent::save($args);
	}
}

?>