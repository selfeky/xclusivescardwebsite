<?php   

defined('C5_EXECUTE') or die(_("Access Denied."));

class RickbPlainTextBlockPackage extends Package {

	protected $pkgHandle = 'rickb_plain_text_block';
	protected $appVersionRequired = '5.4.1';
	protected $pkgVersion = '1.0.1';
	
	public function getPackageDescription() {
		return t("Adds a block that contains plain text, such as computer program source code.");
	}
	
	public function getPackageName() {
		return t("Plain Text Block");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('rickb_plaintext', $pkg);
	}

	public function uninstall() {
	    $btTable = 'btContentPlainText';
	    $pkg = parent::uninstall();
	    $db = Loader::db();		
	    $db->Execute('drop table '.$btTable);
	}
}