<?php     

defined('C5_EXECUTE') or die(_("Access Denied."));

class NalbertoFblikePackage extends Package {

	protected $pkgHandle = 'nalberto_fblike'; 
	protected $appVersionRequired = '5.3.2';
	protected $pkgVersion = '1.1'; 
	
	public function getPackageName() {
		return t("Facebook Like Button"); 
	}	
	
	public function getPackageDescription() {
		return t("Let your visitors use the popular Facebook Like button on your pages to promote your site.");
	}
	
	public function install() {
		$pkg = parent::install();
		
		// install block		
		BlockType::installBlockTypeFromPackage('nalberto_fblike', $pkg);		
	}

}