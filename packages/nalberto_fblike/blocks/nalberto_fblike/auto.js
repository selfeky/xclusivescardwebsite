	fblikeTabSetup = function() {
		$('ul#ccm-fblike-tabs li a').each( function(num,el){ 
			el.onclick=function(){
				var pane=this.id.replace('ccm-fblike-tab-','');
				fblikeShowPane(pane);
			}
		});		
	}
	
	fblikeShowPane = function (pane){
		$('ul#ccm-fblike-tabs li').each(function(num,el){ $(el).removeClass('ccm-nav-active') });
		$(document.getElementById('ccm-fblike-tab-'+pane).parentNode).addClass('ccm-nav-active');
		$('div.ccm-fblikePane').each(function(num,el){ el.style.display='none'; });
		$('#ccm-fblikePane-'+pane).css('display','block');
	}
	
	$(function() {	
		fblikeTabSetup();		
		}
	);
	
var fblikeBlock ={

	validate:function(){
		var failed=0; 
 
		var api_keyF=$('#ccm_fblike_block_api_key');
		var api_keyV=api_keyF.val();
		if(!api_keyV || api_keyV.length==0 ){
			alert(ccm_t('fblike-api-key'));
			api_keyF.focus();
			failed=1;
		}
 


		
		if(failed){
			ccm_isBlockError=1;
			return false;
		}
		return true;
	} 
}

ccmValidateBlockForm = function() { return fblikeBlock.validate(); }