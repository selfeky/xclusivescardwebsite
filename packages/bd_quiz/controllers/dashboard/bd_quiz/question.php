<?php 
    defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardBdQuizQuestionController extends Controller {
    public $helpers = array('html','form');

    public function on_start() {
        Loader::library('item_list');
        $this->error = Loader::helper('validation/error');
    }
    public function view() {
        $db = Loader::db();
        $QList = new QuestList();
        $QResults = $QList->getPage();
        $qtypes = $db->getAssoc('SELECT id,name FROM QzQuestTypes ORDER BY id');
        $this->set('quest', $QResults);
        $this->set('questList', $QList);
        $this->set('qtypes',$qtypes);
    }
    public function add(){
        $moderid = isset($_GET['eId'])?intval($_GET['eId']):0;
        $qtype = isset($_POST['qtype'])?intval($_POST['qtype']):1;

        $db = Loader::db();
        $qcats = $db->GetAll("SELECT qc.qc_id,qc.qcName FROM QzQcat as qc WHERE qc.qc_id NOT IN "
                        . " (SELECT q.qc_id FROM QzQuestCat as c, QzQcat as q"
                        . " WHERE q.qc_id=c.cat_id AND c.quest_id=?)"
                        . " ORDER BY qc.qcName", array($moderid));

        $qcats_in = $db->GetAll("SELECT q.qc_id,q.qcName FROM QzQuestCat as c, QzQcat as q "
                        . " WHERE q.qc_id=c.cat_id AND c.quest_id=?"
                        . " ORDER BY q.qcName", array($moderid));

        $quiz  = $db->GetAll("SELECT qu.qzid,qu.qzName FROM QzQuiz as qu WHERE qu.qzid NOT IN "
                        . " (SELECT q.qzid FROM QzQuestQuiz as c, QzQuiz as q "
                        . " WHERE q.qzid=c.quiz_id AND c.quest_id=?)"
                        . " ORDER BY qu.qzName", array($moderid));

        $quizess_in  = $db->GetAll("SELECT q.qzid,q.qzName FROM QzQuestQuiz as c, QzQuiz as q"
                        . " WHERE q.qzid=c.quiz_id AND c.quest_id=?"
                        . " ORDER BY q.qzName", array($moderid));

        $quest = $db->GetAll("Select * FROM QzQuestion WHERE q_id = ?", array($moderid));
        $quest_choice = $db->GetAll("Select * FROM QzQuestChoice WHERE c_question_id = ? ORDER BY ordering",
                        array($moderid));
        $this->set('eQuest', isset($quest[0])?$quest[0]:NULL);
        $this->set('choices', isset($quest_choice)?$quest_choice:NULL);
        if($moderid){
            $qtype = $quest[0]['q_type'];
        }
        $this->set('qtype',$qtype);
        $this->set('qcats',$qcats);
        $this->set('quizess',$quiz);
        $this->set('qcats_in',$qcats_in);
        $this->set('quizess_in',$quizess_in);
    }

    public function save(){
        $db = Loader::db();
        $c_type = isset($_POST['c_type'])?intval($_POST['c_type']):0;
        $q_id = isset($_POST['q_id'])?intval($_POST['q_id']):0;
        if (strlen(trim($_POST['qName'])) < 1) {
            $this->error->add(t('Question body required.'));
        }
        if ($this->error->has()) {
            $this->set('error',$this->error);
            $this->set('qtype',$c_type);
        }else{

            $qchoice = isset($_POST['qchoice'])?intval($_POST['qchoice']):0;
            $req_answer = isset($_POST['require_answer'])?intval($_POST['require_answer']):0;

            if($q_id){
                $v = array($_POST['qName'], intval($_POST['q_point']), $c_type, $qchoice, intval($_POST['q_random']),
                    $req_answer, $q_id);
                $r = $db->prepare("UPDATE QzQuestion SET qName = ?, q_point = ?, q_type = ?, qchoice = ?,
                q_random = ?, require_answer = ? WHERE q_id = ?");
            }else{
                $v = array($_POST['qName'], intval($_POST['q_point']), $c_type, $qchoice, intval($_POST['q_random']),
                    $req_answer);
                $r = $db->prepare("insert into QzQuestion (qName, q_point, q_type, qchoice, q_random, require_answer)"
                        . " values (?, ?, ?, ?, ?, ?)");
            }

            $db->execute($r, $v);
            $c_question_id = $q_id ? $q_id : $db->Insert_ID();

            if($c_type == 1 || $c_type == 2){
                $field_order = 0;
                $ans_right = array();
                if (isset($_REQUEST['jq_checked'])) {
                    foreach ($_REQUEST['jq_checked'] as $sss) {
                        $ans_right[] = $sss;
                    }
                }
                if (isset($_POST['jq_hid_fields'])) {
                    $mcounter = 0;
                    $fids_arr = array();
                    foreach ($_POST['jq_hid_fields'] as $f_row) {
                        $c_choice = stripslashes($f_row);
                        $c_right = in_array(($field_order + 1), $ans_right)?1:0;
                        $ordering = $field_order;

                        if(intval($_POST['jq_hid_fields_ids'][$mcounter])){
                            $new_c_id = intval($_POST['jq_hid_fields_ids'][$mcounter]);
                            $v = array($c_choice, $c_right, $ordering, $new_c_id);
                            $r = $db->prepare("UPDATE QzQuestChoice SET c_choice = ?, c_right = ?, ordering = ? "
                                            . " WHERE c_id = ?");
                            $db->execute($r, $v);
                        }else{
                            $v = array($c_choice, $c_right, $c_question_id, $ordering);
                            $r = $db->prepare("INSERT INTO QzQuestChoice(c_choice,c_right,c_question_id,ordering)"
                                            . " VALUES(?,?,?,?)");
                            $db->execute($r, $v);
                            $new_c_id = $db->Insert_ID();
                        }

                        $fids_arr[] = $new_c_id;
                        $field_order ++ ;
                        $mcounter ++ ;

                    }
                    $fieldss = implode(",",$fids_arr);
                    $v = array($c_question_id);
                    $r = $db->prepare("DELETE FROM QzQuestChoice WHERE c_question_id = ? AND c_id NOT IN ($fieldss)");
                    $db->execute($r, $v);
                }
                else
                {

                    $v = array($c_question_id);
                    $r = $db->prepare("DELETE FROM QzQuestChoice WHERE c_question_id = ?");
                    $db->execute($r, $v);
                    if($this->c_type == 1 || $this->c_type == 2){
                        $this->error->add(t('Question not complete. Answer not specify.'));
                        $this->set('error',$this->error);
                        return false;
                    }
                }
            }

            $v = array($c_question_id);
            $r = $db->prepare("DELETE FROM QzQuestCat WHERE quest_id = ?");
            $db->execute($r, $v);
            $r = $db->prepare("DELETE FROM QzQuestQuiz WHERE quest_id = ?");
            $db->execute($r, $v);

            if(isset($_POST["qcats_in"]) && count($_POST["qcats_in"])){
                foreach($_POST["qcats_in"] as $qcatin){
                    $v = array($c_question_id, $qcatin);
                    $r = $db->prepare("INSERT INTO QzQuestCat(quest_id, cat_id) VALUES(?,?)");
                    $db->execute($r, $v);
                }
            }
            $ordering = 0;
            if(isset($_POST["quizess_in"]) && count($_POST["quizess_in"])){
                foreach($_POST["quizess_in"] as $qcatin){
                    $v = array($c_question_id, $qcatin, $ordering);
                    $r = $db->prepare("INSERT INTO QzQuestQuiz(quest_id, quiz_id, ordering) VALUES(?,?,?)");
                    $db->execute($r, $v);
                    $ordering ++;
                }
            }

            $this->redirect('/dashboard/bd_quiz/question');
        }

    }
    public function delete(){
        $db = Loader::db();
        $qIds = $_GET['qID'];
        if(count($qIds)){
            foreach($qIds as $delID){
                $db->Execute("delete from QzQuestion where q_id = ?", array($delID));
                $db->Execute("delete from QzQuestCat where quest_id = ?", array($delID));
            }

        }

        $this->view();
    }
}
class QuestList extends DatabaseItemList {
    protected $itemsPerPage = 10;
    protected $autoSortColumns = array('qName','q_point');
    function __construct() {
        $this->setQuery(
            'select q_id, qName, q_type, q_point FROM QzQuestion');
    }

}