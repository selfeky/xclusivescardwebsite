<?php 
    defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardBdQuizQcatController extends Controller {
    public $helpers = array('html','form');

    public function on_start() {
        Loader::library('item_list');
        $this->error = Loader::helper('validation/error');
    }
    public function view() {

        $QList = new QcatList();
        $QResults = $QList->getPage();
        $this->set('qcat', $QResults);
        $this->set('qcatList', $QList);
    }
    public function qcat_add(){
        $moderid = isset($_GET['eId'])?intval($_GET['eId']):0;
        $db = Loader::db();
        $qcat = $db->GetAll("Select qc_id,qcName,qcDescr FROM QzQcat WHERE qc_id = ?", array($moderid));
        $this->set('eQcat', isset($qcat[0])?$qcat[0]:NULL);
        //$this->view();
    }
    public function save(){
        $qc_id = isset($_POST['qc_id'])?intval($_POST['qc_id']):0;

        $db = Loader::db();
        if (strlen(trim($_POST['c_title'])) < 1) {
            $this->error->add(t('Category Name required.'));

        }
        if ($this->error->has()) {
            $this->set('error',$this->error);
        }else{

            if($qc_id){
                $v = array($_POST['c_title'], $_POST['qcDescr'], $qc_id);
                $r = $db->prepare("update QzQcat SET qcName = ?, qcDescr = ? WHERE qc_id = ?");
            }else{
                $v = array($_POST['c_title'], $_POST['qcDescr']);
                $r = $db->prepare("insert into QzQcat (qcName, qcDescr) values (?, ?)");

            }
            $db->execute($r, $v);
            $this->redirect('/dashboard/bd_quiz/qcat');
        }

    }
    public function delete(){
        $db = Loader::db();
        $qIds = $_GET['qID'];
        if(count($qIds)){
            foreach($qIds as $delID){
                $db->Execute("delete from QzQcat where qc_id = ?", array($delID));
            }

        }

        $this->view();
    }
}
class QcatList extends DatabaseItemList {
    protected $itemsPerPage = 10;
    protected $autoSortColumns = array('qcName');
    function __construct() {
        $this->setQuery(
            'select qc_id, qcName, qcDescr FROM QzQcat');
    }

}