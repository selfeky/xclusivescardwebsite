<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardBdQuizReportController extends Controller {
    public $helpers = array('html','form');

    public function on_start() {
        Loader::library('item_list');
        $this->error = Loader::helper('validation/error');
    }
    public function view() {
        $db = Loader::db();
        $quiz_id = isset($_REQUEST['quiz_id'])?intval($_REQUEST['quiz_id']):0;
        $QList = new QReportList();
        if($quiz_id){
            $QList->filter('q.qzid',$quiz_id);
        }
        $QResults = $QList->getPage();


        $quiz_st  = $db->GetAssoc("SELECT qu.qzid,qu.qzName FROM QzQuiz as qu ORDER BY qu.qzName");

        $quiz[0] = t("** All Quiz");
        if(count($quiz_st)){
            foreach($quiz_st as $key=>$value){
                $quiz[$key] = $value;
            }
        }

        $this->set('quiz', $quiz);
        $this->set('quiz_id', $quiz_id);
        $this->set('qcat', $QResults);
        $this->set('qcatList', $QList);
    }


    public function delete(){
        $db = Loader::db();
        $qIds = $_GET['qID'];
        if(count($qIds)){
            foreach($qIds as $delID){
                $db->Execute("delete from QzQuizReport where id = ?", array($delID));
                $db->Execute("delete from QzQuizReportQuest where report_id = ?", array($delID));
            }

        }

        $this->view();
    }
}
class QReportList extends DatabaseItemList {
    protected $itemsPerPage = 10;
    protected $autoSortColumns = array('start_dt','qzName','usern','time_spend', 'passed', 'points');
    function __construct() {
        $this->setQuery(
            'select r.id, r.passed ,q.qzName, IF(u.uID,u.uName,"Guest") as usern,
              r.start_dt, r.time_spend, r.passed, r.points
              FROM QzQuizReport as r LEFT JOIN Users as u ON u.uID = r.user_id
            JOIN QzQuiz as q ON q.qzid = r.quiz_id');
        $this->sortBy('start_dt', 'desc');
    }

}