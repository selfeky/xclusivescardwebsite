<?php 
defined('C5_EXECUTE') or die(_("Access Denied.")); 

class DashboardBdQuizQuizessController extends Controller {
	public $helpers = array('html','form');

	public function on_start() {
		Loader::library('item_list');
		$this->error = Loader::helper('validation/error');
	}
	public function view() {

		$QList = new QuizList();
		$QResults = $QList->getPage();
		$this->set('quizes', $QResults);	
		$this->set('quizesList', $QList);
    }
	public function add(){
        $db = Loader::db();
        $moderid = isset($_GET['eId'])?intval($_GET['eId']):0;
        $qcats = $db->GetAll('SELECT q.*,(SELECT COUNT(*) FROM QzQuestCat as qc  WHERE qc.cat_id=q.qc_id) as cnt'
                            . ' FROM QzQcat as q ORDER BY q.qcName');
        $qcats_pool = $db->GetAll('SELECT q.number,c.*,'
                                . '(SELECT COUNT(*) FROM QzQuestCat as qc'
                                . ' LEFT JOIN QzQuestion as qu ON qu.q_id=qc.quest_id WHERE qc.cat_id=c.qc_id) as cnt'
                                . ' FROM QzQuizQfromcat as q, QzQcat as c WHERE q.cat_id=c.qc_id AND q.quiz_id = ?',
                                array($moderid));
        $questInQuiz = $db->getAll('SELECT q.* FROM QzQuestion as q JOIN QzQuestQuiz as qq ON '
                                . '(qq.quest_id=q.q_id AND qq.quiz_id = ? ) ORDER BY qq.ordering', array($moderid));
        $questArr = $db->getAll('SELECT q.*,d.name FROM QzQuestion as q JOIN QzQuestTypes as d ON d.id=q.q_type'
                                . ' ORDER BY q.qName');
        $quiz = $db->GetAll("Select * FROM QzQuiz WHERE qzid = ?", array($moderid));

        $args["Quiz"] = isset($quiz[0])?$quiz[0]:NULL;
        $args["qcats"] = $qcats;
        $args["qcats_pool"] = $qcats_pool;
        $args["questInQuiz"] = $questInQuiz;
        $args["questArr"] = $questArr;
        $args["mobj"] = $this;
        $this->set('args', $args);
	}
    function editcat(){
        header('Content-type: text/html; charset=utf-8');
        $qcat = intval($_GET["qcat"]);
        $curq = $_GET["curq"];
        $notin = array();
        if($curq){
            $notin = explode(",",$curq);
        }
        $db = Loader::db();
        $v = array();
        $query = 'SELECT q.*,d.name FROM QzQuestion as q';
        if($qcat && $qcat != -1){
            $query .= ' JOIN `QzQuestCat` as qc ON qc.quest_id=q.q_id AND qc.cat_id = ?';
            $v[] = $qcat;
        }elseif($qcat == -1){
            $query .= ' LEFT JOIN `QzQuestCat` as qc ON qc.quest_id=q.q_id';
        }
        $query .= ' JOIN `QzQuestTypes` as d ON d.id=q.q_type';
        if($qcat == -1){
            $query .= ' WHERE qc.quest_id is null';
        }
        $query .= ' GROUP BY q.q_id ORDER BY qName';

        $questArr = $db->getAll($query, $v);



        $ret = '<table class="table" cellpadding="0" cellspacing="0">
		<tr>
			<th>
			    <input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \'quest2Box[]\', this.checked)">
			</th>
			<th>'.t("Question Body").'</th>
			<th width="100">'.t("Type").'</th>
		</tr>';
        for($j=0;$j<count($questArr);$j++){
            $quest = $questArr[$j];
            if(in_array($quest["q_id"],$notin)){
                $ret .= '<script type="text/javascript">
                    $("#tr_tohide_'.$quest["q_id"].'").hide();
                    $("#tr_tohide_'.$quest["c_id"].' > td").find(".noborder").attr("disabled",true);
                    $("#tr_tohide_'.$quest["q_id"].'").find(":checkbox").attr("disabled","disabled");
                    </script>';
            }
            $ret .= '<tr id="tr_tohide_'.$quest["q_id"].'">
				<td class="center">
				    <input type="checkbox" name="quest2Box[]" value="'.$quest["q_id"].'" class="noborder">
				</td>
				<td>
					'.$this->stripQuestToQuiz_ajax($quest["qName"]).'
					<input type="hidden" id="qname_'.$quest["q_id"].'" name="qname_'.$quest["q_id"].'" value="'
                .strip_tags(addslashes($this->stripQuestToQuiz_ajax($quest["qName"]))).'" />
				</td>
				<td class="center">
					'.$quest["name"].'
				</td>
			</tr>';
        }
        $ret .= '</table>';
        echo  $ret;
       die();
    }

    public function saveq(){
        $db = Loader::db();
        $dh = Loader::helper('date');
        $qzid = isset($_POST['qzid'])?intval($_POST['qzid']):0;
        $qzDateAdded = $dh->getSystemDateTime();

        if (strlen(trim($_POST['c_title'])) < 1) {
            $this->error->add(t('Title required.'));

        }
        if ($this->error->has()) {
            $this->set('error',$this->error);
        }else{
            $qnum = $_POST['c_questnum']?(int)($_POST['c_qnum']):0;
            if($qzid){
                $v = array($_POST['c_title'], $_POST['c_description'], intval($_POST['c_attempts']),
                            intval($_POST['c_time_limit']), intval($_POST['c_random']), intval($_POST['c_guest']),
                            intval($_POST['c_return']), $_POST['passed_criteria'], $_POST['c_passing'], $qnum,
                            intval($_POST['c_ticktack']), $_POST['c_pass_message'], $_POST['c_unpass_message'], $qzid);
                $r = $db->prepare("Update QzQuiz SET qzName = ?, qzDescr = ?, qz_attempts = ?, qz_time_limit = ?,"
                                . " qz_random = ?, qz_guest = ?, qz_return = ?, qz_passed_criteria = ?,"
                                . " qz_passing = ?, qz_questnumber = ?, qz_ticktack = ?, qz_passed_mess = ?,"
                                . " qz_failed_mess = ? WHERE qzid = ?");
                $db->execute($r, $v);
                $qid = $qzid;
            }else{
                $v = array($_POST['c_title'], $_POST['c_description'], intval($_POST['c_attempts']),
                            intval($_POST['c_time_limit']),$qzDateAdded, intval($_POST['c_random']),
                            intval($_POST['c_guest']), intval($_POST['c_return']), $_POST['passed_criteria'],
                            $_POST['c_passing'], $qnum, intval($_POST['c_ticktack']), $_POST['c_pass_message'],
                            $_POST['c_unpass_message']);
                $r = $db->prepare("insert into QzQuiz (qzName, qzDescr, qz_attempts, qz_time_limit, qz_created_time,"
                            . " qz_random, qz_guest, qz_return, qz_passed_criteria, qz_passing, qz_questnumber, "
                            . " qz_ticktack, qz_passed_mess, qz_failed_mess)"
                            . " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $db->execute($r, $v);
                $qid = $db->Insert_ID();
            }


            $db->execute("DELETE FROM QzQuizQfromcat WHERE quiz_id = '".$qid."'");
            if (isset($_POST['pool_in_quiz'])) {
                $rt = 0;
                foreach ($_POST['pool_in_quiz'] as $qcat) {

                    $db->execute("INSERT INTO QzQuizQfromcat(quiz_id,cat_id,`number`) VALUES(?,?,?)",
                        array($qid,$qcat,intval($_POST['pool_in_quiz_count'][$rt])));
                    $rt++;
                }
            }

            $db->execute("DELETE FROM QzQuestQuiz WHERE quiz_id = '".$qid."'");
            if (isset($_POST['quesi_in_quiz'])) {
                $ordering = 0;
                foreach ($_POST['quesi_in_quiz'] as $qquest) {
                    $db->execute("INSERT INTO QzQuestQuiz(quiz_id,quest_id,ordering) VALUES(?,?,?)",
                        array($qid,$qquest,$ordering));
                    $ordering++;
                }
            }

            $this->redirect('/dashboard/bd_quiz/quizess');
        }

    }
    public function delete(){
        $db = Loader::db();
        $qIds = $_GET['qID'];
        if(count($qIds)){
            foreach($qIds as $delID){
                $db->Execute("delete from QzQuiz where qzid = ?", array($delID));
            }

        }

        $this->view();
    }

    public function stripQuestToQuiz_ajax($text_start){
        $text = strip_tags($text_start,'<img>');
        $text = str_replace("<img ",'<img width="50" height="50" ',$text);
        $pattern = "/width=\"[0-9]*\"/";
        $text = preg_replace($pattern, 'width="50"', $text);
        $pattern = "/height=\"[0-9]*\"/";
        $text = preg_replace($pattern, 'height="50"', $text);
        //}
        if(strlen(strip_tags($text_start)) > 300){
            $text_new=explode(".", $text);
            if(count($text_new) > 2){
                if(strlen($text_new[0]) > 200){
                    $text = $text_new[0].". ";
                }else{
                    $text = $text_new[0].". ".$text_new[1].". ";
                }
            }
        }
        return $text;
    }
}
class QuizList extends DatabaseItemList {
	protected $itemsPerPage = 10;
	protected $autoSortColumns = array('qzName', 'qz_created_time');
	function __construct() {
		$this->setQuery(
			   'select qzid, qzName, qzDescr, qz_created_time,SUM(r.passed) as passed,COUNT(r.passed) as cnt'
			   . ' FROM QzQuiz as q'
			   . ' LEFT JOIN QzQuizReport as r ON (r.quiz_id=q.qzid AND r.finished="1")
			   ');
        $this->groupBy("qzid");
	}

}
