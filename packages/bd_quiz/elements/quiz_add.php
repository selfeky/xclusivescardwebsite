<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
define('QUIZPATH_DIR',BASE_URL.'/packages/bd_quiz/single_pages/dashboard/bd_quiz/');
$form = loader::helper('form');
Loader::element('editor_config');
echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Quiz Add'), t('Quiz Add.')); ?>
<script type="text/javascript">

    $(document).ready(function(){
        $('#qtabs a').click(function(ev){
            var tab_to_show = $(this).attr('href');
            $('#qtabs li').
                    removeClass('ccm-nav-active').
                    find('a').
                    each(function(ix, elem){
                        var tab_to_hide = $(elem).attr('href');
                        $(tab_to_hide).hide();
                    });
            $(tab_to_show).show();
            $(this).parent('li').addClass('ccm-nav-active');
            return false;
        }).first().click();
        $('#qtabsq a').click(function(ev){
            var tab_to_show = $(this).attr('href');
            $('#qtabsq li').
                    removeClass('ccm-nav-active').
                    find('a').
                    each(function(ix, elem){
                        var tab_to_hide = $(elem).attr('href');
                        $(tab_to_hide).hide();
                    });
            $(tab_to_show).show();
            $(this).parent('li').addClass('ccm-nav-active');
            return false;
        }).first().click();
    });
    function JS_hideme(th,div){

        if(th.value == 1){
            document.getElementById(div).style.display = "block";
        }else{
            document.getElementById(div).style.display = "none";
        }
    }
    function chng_display(th,div){

        if(th.value == "2"){
            document.getElementById(div).style.display = "block";
        }else{
            document.getElementById(div).style.display = "none";
        }
    }
    function getByCats(){
        var allredyin = '';

        $("input[name='quesi_in_quiz\[\]']").each(function() {
            if(allredyin){
                allredyin += ",";
            }
            allredyin += this.value;
        });

        var scat = $('#qcats_quest').val();
        $.ajax({
            url: "<?php  echo $this->url("/dashboard/bd_quiz/quizess/editcat")?>" + "?qcat="+scat+"&curq="+allredyin,
            cache: false,
            success: function(j) {

                $("#div_questlist_id").html(j);
            }

        });
    }
    function checkDelBoxes(pForm, boxName, parent)
    {
        for (i = 0; i < pForm.elements.length; i++)
            if (pForm.elements[i].name == boxName)
                pForm.elements[i].checked = parent;
    }
    function getObj(name) {
        if (document.getElementById)  {  return document.getElementById(name);  }
        else if (document.all)  {  return document.all[name];  }
        else if (document.layers)  {  return document.layers[name];  }
    }
    function Add_new_quest(elem_field, tbl_id) {

        var ransw = eval( 'document.aQuizForm["quest2Box\[\]"]');
        if(ransw){

            var tbl_elem = getObj(tbl_id);
            var ransw2 = ransw.length;
            if(ransw2){
                for (var i=0; i < ransw2; i++) {
                    if(ransw[i].checked  && !ransw[i].disabled){
                        var curid = ransw[i].value;
                        var qname = "qname_"+curid;
                        var row = tbl_elem.insertRow(tbl_elem.rows.length);
                        var cell1 = document.createElement("td");
                        var cell2 = document.createElement("td");
                        var cell3 = document.createElement("td");


                        var input_hidden = document.createElement("input");
                        input_hidden.type = "hidden";
                        input_hidden.name = "quesi_in_quiz[]";
                        input_hidden.value = curid;
                        cell1.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Delete_tbl_row(this);Move_tbl_row('+curid+'); return false;" title="<?php  echo t('Delete');?>"><img src="<?php  echo QUIZPATH_DIR?>images/delete.gif" alt="<?php  echo t('Delete');?>"></a>';
                        cell1.appendChild(input_hidden);
                        cell2.innerHTML = getObj(qname).value;
                        var row_cur = ransw[i].parentNode.parentNode;

                        ransw[i].checked = false;
                        $(row_cur).hide();

                        $(ransw[i]).attr("disabled",true);
                        $("#mess_quest_no").css("display","none");
                        $("#tbl_quest").css("display","table");

                        row.appendChild(cell1);
                        row.appendChild(cell2);
                        row.appendChild(cell3);
                    }
                }
            }else{
                if(ransw.checked && !ransw.disabled){
                    var curid = ransw.value;
                    var qname = "qname_"+curid;
                    var row = tbl_elem.insertRow(tbl_elem.rows.length);
                    var cell1 = document.createElement("td");
                    var cell2 = document.createElement("td");
                    var cell3 = document.createElement("td");


                    var input_hidden = document.createElement("input");
                    input_hidden.type = "hidden";
                    input_hidden.name = "quesi_in_quiz[]";
                    input_hidden.value = curid;
                    cell1.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Delete_tbl_row(this);Move_tbl_row('+curid+'); return false;" title="<?php  echo t('Delete');?>"><img src="<?php  echo QUIZPATH_DIR;?>images/delete.gif" alt="<?php  echo t('Delete');?>"></a>';
                    cell1.appendChild(input_hidden);
                    cell2.innerHTML = getObj(qname).value;
                    var row_cur = ransw.parentNode.parentNode;

                    ransw.checked = false;
                    $(row_cur).hide();

                    $("#mess_quest_no").css("display","none");
                    $("#tbl_quest").css("display","table");
                    $(ransw).attr("disabled",true);
                    row.appendChild(cell1);
                    row.appendChild(cell2);
                    row.appendChild(cell3);
                }
            }
            ReAnalize_tbl_Rows(tbl_elem.rows.length - 2, tbl_id);
        }
    }
    function ReAnalize_tbl_Rows( start_index, tbl_id ) {
        start_index = 1;
        var tbl_elem = getObj(tbl_id);
        if (tbl_elem.rows[start_index]) {

            for (var i=start_index; i<tbl_elem.rows.length; i++) {



                if (i > 1) {
                    tbl_elem.rows[i].cells[2].innerHTML = '<a href="javascript: void(0);" onClick="javascript:Up_tbl_row(this); return false;" title="<?php  echo t('Move Up');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/up.gif" alt="<?php  echo t('Move Up');?>"></a>';
                } else { tbl_elem.rows[i].cells[2].innerHTML = ''; }
                if (i < (tbl_elem.rows.length - 1)) {
                    tbl_elem.rows[i].cells[2].innerHTML += '<a href="javascript: void(0);" onClick="javascript:Down_tbl_row(this); return false;" title="<?php  echo t('Move Down');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/down.gif" alt="<?php  echo t('Move Down');?>"></a>';
                } else { tbl_elem.rows[i].cells[2].innerHTML += ""; }

            }
        }
    }
    function Delete_tbl_row(element) {
        var del_index = element.parentNode.parentNode.sectionRowIndex;
        var tbl_id = element.parentNode.parentNode.parentNode.parentNode.id;
        element.parentNode.parentNode.parentNode.deleteRow(del_index);
    }
    function Move_tbl_row(id){
        $("#tr_tohide_"+id).show();
        $("#tr_tohide_"+id+" > td").find(".noborder").attr("disabled",false);

        ReAnalize_tbl_Rows(1, 'tbl_quest');
    }
    function Move_tbl_row_cats(id){
        $("#tr_cats_tohide_"+id).show();
        //	$("#div_id > input[name=whatever]")

        $("#tr_cats_tohide_"+id+" > td").find(".noborder").attr("disabled",false);
    }
    function Up_tbl_row(element) {
        if (element.parentNode.parentNode.sectionRowIndex > 1) {
            $('#tbl_quest').moveRow(element.parentNode.parentNode.sectionRowIndex,element.parentNode.parentNode.sectionRowIndex-1);

            ReAnalize_tbl_Rows(element.parentNode.parentNode.sectionRowIndex - 2, 'tbl_quest');
        }
    }

    function Down_tbl_row(element) {
        if (element.parentNode.parentNode.sectionRowIndex < element.parentNode.parentNode.parentNode.rows.length - 1) {
            $('#tbl_quest').moveRow2(element.parentNode.parentNode.sectionRowIndex,element.parentNode.parentNode.sectionRowIndex+1);
            ReAnalize_tbl_Rows(element.parentNode.parentNode.sectionRowIndex - 2, 'tbl_quest');
        }
    }
    $.fn.extend({
        moveRow: function(oldPosition, newPosition) {
            return this.each(function(){
                var row = $(this).find('tr').eq(oldPosition).remove();
                $(this).find('tr').eq(newPosition).before(row);
            });
        },
        moveRow2: function(oldPosition, newPosition) {
            return this.each(function(){
                var row = $(this).find('tr').eq(newPosition).remove();
                $(this).find('tr').eq(oldPosition).before(row);
            });
        }
    });
    function Add_new_cats(elem_field, tbl_id) {

        var ransw = eval( 'document.aQuizForm["cats2Box\[\]"]');
        if(ransw){

            var tbl_elem = getObj(tbl_id);
            var ransw2 = ransw.length;
            if(ransw2){
                for (var i=0; i < ransw2; i++) {

                    if(ransw[i].checked && !ransw[i].disabled){
                        var curid = ransw[i].value;
                        var qname = "catsname_"+curid;
                        var qnum = "inp_questnum_"+curid;
                        var catq = "inp_qcats_"+curid;
                        var row = tbl_elem.insertRow(tbl_elem.rows.length);
                        var cell1 = document.createElement("td");
                        var cell2 = document.createElement("td");
                        var cell3 = document.createElement("td");


                        var input_hidden = document.createElement("input");
                        input_hidden.type = "hidden";
                        input_hidden.name = "pool_in_quiz[]";
                        input_hidden.value = curid;
                        cell1.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Delete_tbl_row(this);Move_tbl_row_cats(\''+curid+'\'); return false;" title="<?php  echo t('Delete');?>"><img src="<?php  echo QUIZPATH_DIR;?>images/delete.gif" alt="<?php  echo t('Delete');?>"></a>';
                        cell1.appendChild(input_hidden);

                        cell2.innerHTML = getObj(qname).value+" ("+getObj(catq).value+")";
                        var row_cur = ransw[i].parentNode.parentNode;

                        var input_num = document.createElement("input");
                        input_num.type = "text";
                        input_num.name = "pool_in_quiz_count[]";
                        input_num.value = getObj(qnum).value;
                        input_num.onkeydown = function(event){JS_Numeric(event);};
                        cell3.appendChild(input_num);

                        ransw[i].checked = false;
                        $(row_cur).hide();

                        $(ransw[i]).attr("disabled",true);
                        $("#mess_no_qcats").css("display","none");

                        row.appendChild(cell1);
                        row.appendChild(cell2);
                        row.appendChild(cell3);
                    }
                }
            }else{
                if(ransw.checked && !ransw.disabled){
                    var curid = ransw.value;
                    var qname = "catsname_"+curid;
                    var qnum = "inp_questnum_"+curid;
                    var catq = "inp_qcats_"+curid;
                    var row = tbl_elem.insertRow(tbl_elem.rows.length);
                    var cell1 = document.createElement("td");
                    var cell2 = document.createElement("td");
                    var cell3 = document.createElement("td");


                    var input_hidden = document.createElement("input");
                    input_hidden.type = "hidden";
                    input_hidden.name = "pool_in_quiz[]";
                    input_hidden.value = curid;
                    cell1.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Delete_tbl_row(this);Move_tbl_row_cats(\''+curid+'\'); return false;" title="<?php  echo t('Delete');?>"><img src="<?php  echo QUIZPATH_DIR;?>images/delete.gif" alt="<?php  echo t('Delete');?>"></a>';
                    cell1.appendChild(input_hidden);
                    cell2.innerHTML = getObj(qname).value+" ("+getObj(catq).value+")";
                    var row_cur = ransw.parentNode.parentNode;

                    var input_num = document.createElement("input");
                    input_num.type = "text";
                    input_num.name = "pool_in_quiz_count[]";
                    input_num.value = getObj(qnum).value;
                    input_num.onkeydown = function(event){JS_Numeric(event);};
                    cell3.appendChild(input_num);

                    ransw.checked = false;
                    $(row_cur).hide();

                    $(ransw).attr("disabled",true);

                    row.appendChild(cell1);
                    row.appendChild(cell2);
                    row.appendChild(cell3);
                }
            }

        }
    }
    function JS_Numeric(evt){
        evt = window.event || evt;
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (( evt.ctrlKey || evt.altKey
                || (47<evt.keyCode && evt.keyCode<58 && evt.shiftKey==false)
                || (95<evt.keyCode && evt.keyCode<106)
                || (evt.keyCode==8) || (evt.keyCode==9)
                || (evt.keyCode>34 && evt.keyCode<40)
                || (evt.keyCode==46) ))
            return true;

        return false;
    }
    function JS_Percant(evt){
        evt = window.event || evt;
        if (( evt.ctrlKey || evt.altKey
                || (47<evt.keyCode && evt.keyCode<58 && evt.shiftKey==false)
                || (95<evt.keyCode && evt.keyCode<106)
                || (evt.keyCode==8) || (evt.keyCode==9)
                || (evt.keyCode>34 && evt.keyCode<40)
                || (evt.keyCode==46) ) && (document.getElementById("c_passing").value < 11 || (evt.keyCode==8) || (evt.keyCode==9)))
            return true;

        return false;

    }
</script>
<div class="ccm-pane-body">
    <form method="post" enctype="multipart/form-data" action="<?php  echo $this->action('saveq')?>" name="aQuizForm" id="aQuizForm">
        <ul id="qtabs" class="ccm-dialog-tabs">
            <li> <a href="#pq_tab1"><?php  echo t('Main')?></a></li>
            <li><a href="#pq_tab2"><?php  echo t('Questions')?></a></li>
        </ul>
        <div id="pq_curTab" style="padding:10px;">
            <div id="pq_tab1">
                <div class="clearfix">
                    <?php  echo $form->label('c_title', t('Title:'))?>
                    <div class="input">
                        <input type="hidden" name="q_id" value=""/>
                        <?php  echo $form->text('c_title', isset($Quiz["qzName"])?$Quiz["qzName"]:'')?><sup> *</sup>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_time_limit', t('Time Limit:'))?>
                    <div class="input">
                        <?php  $arr = array("onclick" => "JS_hideme(this,'ticktack_id');",
                        "style" => "display:inline;");?>
                        <label style="display: inline;">
                            <?php  echo $form->radio('c_ticktack',0,isset($Quiz["qz_ticktack"])?$Quiz["qz_ticktack"]:0,$arr);?>
                            <span><?php  echo t('Disabled');?></span>
                        </label>
                        <label style="display: inline;margin-left: 15px;">
                            <?php  echo $form->radio('c_ticktack',1,isset($Quiz["qz_ticktack"])?$Quiz["qz_ticktack"]:0,$arr);?>
                            <span><?php  echo t('Enabled');?></span>
                        </label>
                        <?php 
                        $tick_display = (isset($Quiz["qz_ticktack"]) && $Quiz["qz_ticktack"] == 1)?"display:block":"display:none";
                        ?>
                        <div id="ticktack_id" style='<?php  echo $tick_display;?>;clear:both;'>
                            <br />
                            <?php  echo $form->text('c_time_limit', isset($Quiz["qz_time_limit"])
                            ?$Quiz["qz_time_limit"]:'', array("class" => "numbersOnly"))?>
                            <small><?php  echo t("Value must be in seconds");?></small>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_attempts', t('Number of user attempts:'))?>
                    <div class="input">
                        <?php  echo $form->text('c_attempts',isset($Quiz["qz_attempts"])?$Quiz["qz_attempts"]:'',
                         array("class" => "numbersOnly"));?>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_description', t('Description:'))?>
                    <div class="input">
                        <?php  Loader::element('editor_controls'); ?>
                        <?php  echo $form->textarea('c_description', isset($Quiz["qzDescr"])?$Quiz["qzDescr"]:'',
                        array('rows' => 5, 'cols' => 30, 'class' => 'ccm-advanced-editor'))?>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_guest', t('Guest access:'));?>
                    <div class="input">
                        <label style="display: inline;">
                            <?php  echo $form->radio('c_guest',0,isset($Quiz["qz_guest"])?$Quiz["qz_guest"]:0,
                            array("style" => "display:inline;"));?>
                            <span><?php  echo t('Disabled');?></span>
                        </label>
                        <label style="display: inline; margin-left: 15px;">
                            <?php  echo $form->radio('c_guest',1,isset($Quiz["qz_guest"])?$Quiz["qz_guest"]:0,
                            array("style" => "display:inline;"));?>
                            <span><?php  echo t('Enabled');?></span>
                        </label>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_return', t('Ability to return to the previous question:'));?>
                    <div class="input">
                        <label style="display: inline;">
                            <?php  echo $form->radio('c_return',0,isset($Quiz["qz_return"])?$Quiz["qz_return"]:0, array("style" => "display:inline;"));?>
                            <span><?php  echo t('Disabled');?></span>
                        </label>
                        <label style="display: inline; margin-left: 15px;">
                            <?php  echo $form->radio('c_return',1,isset($Quiz["qz_return"])?$Quiz["qz_return"]:0, array("style" => "display:inline;"));?>
                            <span><?php  echo t('Enabled');?></span>
                        </label>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_pass_message', t('Passed Quiz text:'))?>
                    <div class="input">
                        <?php  Loader::element('editor_controls'); ?>
                        <?php  echo $form->textarea('c_pass_message',isset($Quiz["qz_passed_mess"])?$Quiz["qz_passed_mess"]:'',array("rows"=>10,"cols"=>100, 'class' => 'ccm-advanced-editor'));?>
                        <i style="font-size: 85%; line-height: 150%;">
                            {points} - user points <br />
                            {total_points} - total quiz points <br />
                            {time} - user time <br />
                            {played} - quiz played <br />
                            {avg_points} - average points <br />
                        </i>
                    </div>

                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_unpass_message', t('Failed Quiz text:'))?>
                    <div class="input">
                        <?php  Loader::element('editor_controls'); ?>
                        <?php  echo $form->textarea('c_unpass_message',isset($Quiz["qz_failed_mess"])?$Quiz["qz_failed_mess"]:'',array("rows"=>10,"cols"=>100, 'class' => 'ccm-advanced-editor'));?>
                    </div>
                </div>

                <div class="clearfix">
                    <?php  echo $form->label('passed_criteria', t('Passing Criteria:'))?>

                    <div class="input">
                        <?php  $optarr = array("0" => t("No Criteria"), "2" => t("By percent"));?>
                        <?php  echo $form->select("passed_criteria",$optarr,isset($Quiz["qz_passed_criteria"])?$Quiz["qz_passed_criteria"]:0,array("onchange" => "chng_display(this,'passperc_div');")); ?>

                    </div>
                </div>
                <?php 
                $pass_display = (isset($Quiz["qz_passed_criteria"]) && $Quiz["qz_passed_criteria"] == 2)?"display:block":"display:none";
                ?>
                <div class="clearfix"  id="passperc_div" style="<?php  echo $pass_display;?>">
                    <?php  echo $form->label('c_passing', t('Passing Score:'));?>

                    <div class="input">
                        <?php  echo $form->text("c_passing",isset($Quiz["qz_passing"])?$Quiz["qz_passing"]:0
                        , array("class" => "numbersOnly")); ?>
                    </div>
                </div>
            </div>
            <div id="pq_tab2" style="display:none;">
                <div class="clearfix">
                    <?php  echo $form->label('c_questnum', t('Set number of questions:'));?>
                    <div class="input">
                        <label style="display: inline;">
                            <?php  echo $form->radio('c_questnum',0,(isset($Quiz["qz_questnumber"]) &&
                            $Quiz["qz_questnumber"])?1:0, array("onclick" => "JS_hideme(this,'cqnum_div')",
                            "style" => "display:inline;"));?>
                            <span><?php  echo t('Disabled');?></span>
                        </label>
                        <label style="display: inline; margin-left: 15px;">
                            <?php  echo $form->radio('c_questnum',1,(isset($Quiz["qz_questnumber"]) &&
                            $Quiz["qz_questnumber"])?1:0, array("onclick" => "JS_hideme(this,'cqnum_div')",
                            "style" => "display:inline;"));?>
                            <span><?php  echo t('Enabled');?></span>
                        </label>
                    </div>
                    <?php 
                    $disp = (isset($Quiz["qz_questnumber"]) && $Quiz["qz_questnumber"])?"display:block":"display:none";
                    ?>
                    <div class="input" id="cqnum_div" style="<?php  echo $disp;?>;clear: both;">
                        <?php  echo $form->text("c_qnum",isset($Quiz["qz_questnumber"])?$Quiz["qz_questnumber"]:0); ?>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('c_random', t('Randomize Questions:'));?>
                    <div class="input">
                        <label style="display: inline;">
                            <?php  echo $form->radio('c_random',0,isset($Quiz["qz_random"])?$Quiz["qz_random"]:0, array("style" => "display:inline;"));?>
                            <span><?php  echo t('Disabled');?></span>
                        </label>
                        <label style="display: inline; margin-left: 15px;">
                            <?php  echo $form->radio('c_random',1,isset($Quiz["qz_random"])?$Quiz["qz_random"]:0, array("style" => "display:inline;"));?>
                            <span><?php  echo t('Enabled');?></span>
                        </label>
                    </div>
                </div>
                <ul id="qtabsq" class="ccm-dialog-tabs">
                    <li><a href="#pqtab21"><?php  echo t('Add question directly')?></a></li>
                    <li><a href="#pqtab22"><?php  echo t('Add categories of questions')?></a></li>
                </ul>
                <div id="pqtab21" style="clear:both; float:none;">
                    <?php 
                    $show_quest = true;
                    if(!$questInQuiz){
                        echo "<div id='mess_quest_no'>".t("There are no Questions added to the Quiz")."</div>";
                        $showquest = false;
                    }
                    echo '	<table cellpadding="0" cellspacing="0" class="table" id="tbl_quest" '.($questInQuiz?"":" style='display:none;'").'>
                    <tr>
                        <th>&nbsp</th>
                        <th>'.t("Question Body").'</th>
                        <th>'.t("Position").'</th>
                    </tr>';
                    $arr_already = array();
                    if($questInQuiz){
                    $ind = 0;

                    foreach($questInQuiz as $quQ){
                    $arr_already[] = $quQ["q_id"];
                    ?>
                    <tr>
                        <td>
                            <a href="javascript: void(0);" onclick="javascript:Delete_tbl_row(this);Move_tbl_row('<?php  echo $quQ["q_id"];?>'); return false;" title="<?php  echo t('Delete');?>">
                                <img src="<?php  echo QUIZPATH_DIR;?>images/delete.gif" alt="<?php  echo t('Delete');?>">
                            </a>
                            <input type="hidden" name="quesi_in_quiz[]" value="<?php  echo $quQ["q_id"];?>">
                        </td>
                        <td><?php  echo ($mobj->stripQuestToQuiz_ajax($quQ["qName"]));?></td>
                        <td>
                            <?php 
                            if(count($questInQuiz) > 1){
                                if($ind){
                                    echo '<a href="javascript: void(0);" onclick="javascript:Up_tbl_row(this); return false;" title="'.t("Move Up").'"><img border="0" src="'.QUIZPATH_DIR.'images/up.gif" alt="'.t("Move Up").'"></a>';
                                }
                                if($ind < count($questInQuiz)-1){
                                    echo '<a href="javascript: void(0);" onclick="javascript:Down_tbl_row(this); return false;" title="'.t("Move Down").'"><img border="0" src="'.QUIZPATH_DIR.'images/down.gif" alt="'.t("Move Up").'"></a>';
                                }
                            }
                            ?>
                        </td>
                    </tr>
                    <?php 
                    $ind++;
                    }
                    }
                    echo '		</table>
							<hr>
							<br />
							<label>'.t('Select Category').' </label>
							<div><select name="qcats_quest" id="qcats_quest" onchange="javascript:getByCats();">
								<option value="0">--</option>
								<option value="-1">'.t('No Category').'</option>';
                    if(count($qcats)){
                        foreach($qcats as $qcat){
                            echo '<option value="'.$qcat["qc_id"].'">'.$qcat["qcName"].'</option>';
                        }
                    }
                ?>
                </select></div><br />
                <div id="div_questlist_id">
                <?php 
                if(!$questArr && !$questInQuiz){
                echo "<div>".t("There are no Questions created in the Module. Please visit Questions section")."</div>";
                }else{
                echo	'<table class="table" cellpadding="0" cellspacing="0">
                    <tr>
                        <th><input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \'quest2Box[]\', this.checked)"></th>
                        <th>'.t("Question Body").'</th>
                        <th width="100">'.t("Type").'</th>
                    </tr>';
                    for($j=0;$j<count($questArr);$j++){
                    $quest = $questArr[$j];

                    echo '<tr id="tr_tohide_'.$quest["q_id"].'">
                    <td class="center"><input type="checkbox" name="quest2Box[]" value="'.$quest["q_id"].'" class="noborder"></td>
                    <td>
                        '.$mobj->stripQuestToQuiz_ajax($quest["qName"]).'
                        <input type="hidden" id="qname_'.$quest["q_id"].'" name="qname_'.$quest["q_id"].'" value="'
                        .htmlspecialchars($mobj->stripQuestToQuiz_ajax($quest["qName"])).'" />
                    </td>
                    <td class="center">
                        '.($quest["name"]).'
                    </td>
                    </tr>';
                    if(in_array($quest["q_id"],$arr_already)){
                    echo '<script type="text/javascript">
                            $("#tr_tohide_'.$quest["q_id"].'").hide();
                            $("#tr_tohide_'.$quest["q_id"].'").find(":checkbox").attr("disabled","disabled");
                        </script>';
                    }
                    }
                    echo	'</table>';
                }
                echo '	</div>	';
                if($questArr){
                echo '<input class="button" type="button" value="'.t("Add").'" onclick="Add_new_quest(\'quest2Box[]\',\'tbl_quest\');" />';
                }
            ?>
            </div>

                <div id="pqtab22" style="display:none;">
                    <?php 
                    if(!$qcats_pool){
                    echo "<div id='mess_no_qcats'>".t('There are no Categories added to the Quiz')."</div>";
                    }
                    echo '<table id="tbl_cats">';
                    $qcats_inarr = array();
                    if($qcats_pool){

                        for($j=0;$j<count($qcats_pool);$j++){
                            $qpool = $qcats_pool[$j];
                            $qcats_inarr[] = $qpool["qc_id"];
                            echo '
                            <tr>
                                <td>
                                    <a href="javascript: void(0);" onclick="javascript:Delete_tbl_row(this);
                                    Move_tbl_row_cats(\''.$qpool["qc_id"].'\'); return false;" title="'.t("Delete")
                                .'"><img src="'.QUIZPATH_DIR.'images/delete.gif" alt="'.t("Delete").'"></a><input type="hidden" name="pool_in_quiz[]" value="'.$qpool["qc_id"].'">
                                </td>
                                <td>'.$qpool["qcName"].' ('.$qpool["cnt"].')</td>
                                <td><input type="text" name="pool_in_quiz_count[]" value="'.$qpool["number"].'" onkeydown="return JS_Numeric(event);"></td>
                            </tr>
                            ';
                        }
                    }
                    echo	'</table>
                    <hr>
                    <br />';
                    echo '<div id="div_catslist_id">';

                    echo	'<table class="table" cellpadding="0" cellspacing="0">
                        <tr>
                            <th><input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \'cats2Box[]\', this.checked)"></th>
                            <th>'.t("Category").'</th>
                            <th>'.t("Number of questions").'</th>
                            <th>'.t("Set the number of questions from Category").'</th>
                        </tr>';
                        if(count($qcats)){
                        for($j=0;$j<count($qcats);$j++){
                        $cats = $qcats[$j];
                        echo '<tr id="tr_cats_tohide_'.$cats["qc_id"].'">
                        <td class="center"><input type="checkbox" name="cats2Box[]" value="'.$cats["qc_id"].'" class="noborder"></td>
                        <td>
                            '.$cats["qcName"].'
                            <input type="hidden" id="catsname_'.$cats["qc_id"].'" name="catsname_'.$cats["qc_id"].'" value="'.strip_tags(addslashes($cats["qcName"])).'" />
                        </td>
                        <td class="center">
                            '.$cats["cnt"].'
                            <input type="hidden" id="inp_qcats_'.$cats["qc_id"].'" name="inp_qcats_'.$cats["qc_id"].'" value="'.$cats["cnt"].'" />
                        </td>
                        <td>
                            <input type="text" id="inp_questnum_'.$cats["qc_id"].'" name="inp_questnum_'.$cats["qc_id"].'" value="" size="5" maxlength="3" onkeydown="return JS_Numeric(event);" />
                        </td>
                        </tr>';
                        if(in_array($cats["qc_id"],$qcats_inarr)){
                        echo '<script type="text/javascript">$("#tr_cats_tohide_'.$cats["qc_id"].'").hide();</script>';
                        }
                        }
                        }else{
                        echo t("There are no Categories created in the Module. Please visit Question Categories section");
                        }
                        echo	'</table>

                </div>	';
                    if(count($qcats)){
                    echo '<input class="button" type="button" value="'.t("Add").'" onclick="Add_new_cats(\'cats2Box[]\',\'tbl_cats\');" />';
                    }
                    ?>
                    <br />


                </div>

            </div>

        </div>
        <?php  echo Loader::helper('concrete/interface')->submit(t('Save'), 'save', 'right')?>
        <div class="small"><sup>*</sup><?php  echo t('Required fields')?></div>
        <input type="hidden" name="qzid" value="<?php  echo $Quiz["qzid"];?>" />
    </form>
</div>
<script type="text/javascript">
    $('.numbersOnly').keydown(function(e){
        var keyPressed;
        if (!e) var e = window.event;
        if (e.keyCode) keyPressed = e.keyCode;
        else if (e.which) keyPressed = e.which;
        var hasDecimalPoint = (($(this).val().split('.').length-1)>0);
        if ( keyPressed == 46 || keyPressed == 8 ||((keyPressed == 190||keyPressed == 110)&&(!hasDecimalPoint)) || keyPressed == 9 || keyPressed == 27 || keyPressed == 13 ||
            // Allow: Ctrl+A
                (keyPressed == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
                (keyPressed >= 35 && keyPressed <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (e.shiftKey || (keyPressed < 48 || keyPressed > 57) && (keyPressed < 96 || keyPressed > 105 )) {
                e.preventDefault();
            }
        }

    });
</script>