<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class BdQuizBlockController extends BlockController {
    protected $btTable = 'btBdQuiz';
    protected $btInterfaceWidth = "550";
    protected $btInterfaceHeight = "400";

    public function getBlockTypeDescription() {
        return t("Display a quiz.");
    }

    public function getBlockTypeName() {
        return t("QuizMaker");
    }
    function add(){
        $db = Loader::db();
        $quizess  = $db->GetAssoc("SELECT qu.qzid,qu.qzName FROM QzQuiz as qu ORDER BY qu.qzName");

        $this->set('quizess', $quizess);
    }
    function edit(){
        $db = Loader::db();
        $quizess  = $db->GetAssoc("SELECT qu.qzid,qu.qzName FROM QzQuiz as qu ORDER BY qu.qzName");

        $this->set('quizess', $quizess);
        $this->set('qid', $this->quiz_id);
    }
    function on_page_view(){
        $bt = BlockType::getByHandle($this->btHandle);
        $uh = Loader::helper('concrete/urls');
        $this->addHeaderItem('<link rel="stylesheet" type="text/css" href="'.($uh->getBlockTypeAssetsURL($bt, 'quiz.css')).'" />');
    }

    function view(){
        $db = Loader::db();
        $quiz  = $db->GetAll("SELECT qu.* FROM QzQuiz as qu WHERE qzid = ?",array($this->quiz_id));
        if(isset($quiz[0])){
            $this->set('quiz',$quiz[0]);
        }
        $tool_name = "action_quiz";
        $tool_helper = Loader::helper('concrete/urls');
        $bt = BlockType::getByHandle($this->btHandle);

        $this->set ($tool_name, $tool_helper->getBlockTypeToolsURL($bt).'/'.$tool_name);
        $this->set('quiz_id', $this->quiz_id);
        $this->set('bID', $this->bID);
    }


}
