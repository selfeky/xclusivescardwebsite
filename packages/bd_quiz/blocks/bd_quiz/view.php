<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
?>
<script language="JavaScript" type="text/javascript">
<!--//--><![CDATA[//><!--
function pq_getObj(name) {
    if (document.getElementById) { return document.getElementById(name); }
    else if (document.all) { return document.all[name]; }
    else if (document.layers) { return document.layers[name]; }
}

function ShowMessage(msg_obj,stat,mes,loading) {
    var mes_span = pq_getObj(msg_obj+'<?php  echo $bID;?>');
    if (stat == 1) {
        if(loading==1){
            mes = '<div class="pqmess_div">'+mes+'</div>';
        }
        mes_span.innerHTML = mes;
        mes_span.style.visibility = "visible";

    } else {
        mes_span.innerHTML = '&nbsp;';
        mes_span.style.visibility = "hidden";
    }
}

var show_quiztitle = '<?php  echo $quiz['qzName'];?>';
var ticktack = <?php  echo $quiz["qz_ticktack"]?$quiz["qz_ticktack"]:0;?>;
var barstep = 0;
var msg_not_reg = '<?php  echo t('You need to be registered user to have access to this quiz')?>';
var stu_quiz_id = 0;
var mes_complete_this_part = '<?php  echo t('Please answer the question');?>';
var mes_loading = '<div class="msg_loading">' +
                    '<img src="<?php  echo BASE_URL;?>/packages/bd_quiz/blocks/bd_quiz/images/loading.gif" />' +
                '</div>';
var mes_failed = '<?php  echo t('Failed');?>';
var mes_please_wait = '<?php  echo t('Wait....');?>';
var mes_time_is_up = '<?php  echo t('Time is Up');?>';
var user_unique_id = '';
var cur_quest_type = '';
var saved_prev_quest_data = '';
var saved_prev_res_data = '';
var saved_prev_quest_id = 0;
var saved_prev_quest_type = 0;
var saved_prev_quest_score = 0;
var saved_prev_quest_num = 0;
var cur_quest_id = 0;
var cur_quest_score = 0;
var cur_quest_num = 0;
var quiz_count_quests = 0;
var response;
var prev_correct = 0;
var allow_attempt = 0;
var timer_sec = 0;
var stop_timer = 0;
var max_quiz_time = <?php  echo ($quiz["qz_time_limit"])?($quiz["qz_time_limit"]):20; ?>;
var quiz_blocked = 0;

function jq_MakeRequest(url, do_clear) {
    if (do_clear == 1) jq_UpdateTaskDiv('clear');

    var http_request = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        try { http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try { http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }
    if (!http_request) {
        return false;
    }
    if (do_clear == 1) {
        pq_getObj('jq_quiz_container<?php  echo $bID;?>').innerHTML = '';
    }
    ShowMessage('jq_quiz_container', 1, mes_loading,0);
    quiz_blocked == 1;
    http_request.onreadystatechange = function() { jq_AnalizeRequest(http_request); };

    var url_prefix2 = '&user_unique_id=' + user_unique_id;
    lp_url_prefix2 = "<?php  echo $action_quiz;?>" + url_prefix2;
    var post_target = '<?php  echo $action_quiz;?>';
    http_request.open("POST", post_target.replace(/\&amp\;/gi, "&"), true);
    http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http_request.setRequestHeader("charset", "utf-8");

    http_request.send(lp_url_prefix2.replace(/\&amp\;/gi, "&") + url);
}
function jq_AnalizeRequest(http_request) {
    if (http_request.readyState == 4) {
        if ((http_request.status == 200)) {
            if(http_request.responseXML.documentElement == null){
                try {
                    http_request.responseXML.loadXML(http_request.responseText);
                } catch (e) {
                }
            }

            response  = http_request.responseXML.documentElement;
            var task = response.getElementsByTagName('task')[0].firstChild.data;
            ShowMessage('error_messagebox',0,'',0);
            switch (task) {
                case 'start':
                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 1000);
                    user_unique_id = response.getElementsByTagName('user_unique_id')[0].firstChild.data;
                    stu_quiz_id = response.getElementsByTagName('stu_quiz_id')[0].firstChild.data;
                    cur_quest_type = response.getElementsByTagName('quest_type')[0].firstChild.data;
                    saved_prev_quest_type = cur_quest_type;
                    cur_quest_id = response.getElementsByTagName('quest_id')[0].firstChild.data;
                    saved_prev_quest_id = cur_quest_id;
                    cur_quest_score = response.getElementsByTagName('quest_score')[0].firstChild.data;
                    saved_prev_quest_score = cur_quest_score;
                    quiz_count_quests = response.getElementsByTagName('quiz_count_quests')[0].firstChild.data;
                    cur_quest_num = response.getElementsByTagName('quiz_quest_num')[0].firstChild.data;
                    saved_prev_quest_num = cur_quest_num;
                    pq_getObj('jq_quiz_container<?php  echo $bID;?>').innerHTML = '';

                    if(!show_quiztitle){
                        pq_getObj("pq_header_id<?php  echo $bID;?>").innerHTML = '';
                    }
                    pq_getObj("pq_header_id<?php  echo $bID;?>").className = "pq_header_quest";
                    pq_getObj("pq_slidebar<?php  echo $bID;?>").style.display = 'block';
                    barstep = parseInt(100/quiz_count_quests);

                    pq_getObj("pq_slidebar_inner<?php  echo $bID;?>").style.marginRight = 100 - barstep + '%';
                    var div_inside1=document.createElement("div");
                    div_inside1.id = 'quest_div';

                    div_inside1.innerHTML = response.getElementsByTagName('quest_data')[0].firstChild.data + response.getElementsByTagName('quest_data_user')[0].firstChild.data;
                    saved_prev_quest_data = response.getElementsByTagName('quest_data')[0].firstChild.data + response.getElementsByTagName('quest_data_user')[0].firstChild.data;
                    pq_getObj('jq_quiz_container<?php  echo $bID;?>').appendChild(div_inside1);

                    jq_UpdateTaskDiv('next');
                    if(ticktack){
                        jq_Start_TickTack();
                    }

                    break;


                case 'next':

                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 1000);
                    prev_correct = response.getElementsByTagName('quiz_prev_correct')[0].firstChild.data;

                    var quiz_cont = pq_getObj('jq_quiz_container<?php  echo $bID;?>');
                    var children = quiz_cont.childNodes;
                    for (var i = 0; i < children.length; i++) { quiz_cont.removeChild(quiz_cont.childNodes[i]); };
                    quiz_cont.innerHTML = '<form name=\'quest_form\'></form>';

                    var qfrf = response.getElementsByTagName('quest_feedback_repl_func')[0].firstChild.data;
                    eval(qfrf);

                    break;

                case 'time_is_up':
                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 1000);
                    var quiz_cont = pq_getObj('jq_quiz_container<?php  echo $bID;?>');
                    var children = quiz_cont.childNodes;
                    for (var i = 0; i < children.length; i++) { quiz_cont.removeChild(quiz_cont.childNodes[i]); };
                    var qmb = response.getElementsByTagName('quiz_message_box')[0].firstChild.data;
                    quiz_cont.innerHTML = '<form name=\'quest_form\'></form>';
                    quiz_cont.innerHTML = '<form name=\'quest_form\'></form>'+qmb;

                    pq_getObj("pq_back_button<?php  echo $bID;?>").style.display = "none";
                    pq_getObj("pq_slidebar<?php  echo $bID;?>").style.display = "none";

                    jq_UpdateTaskDiv('continue_finish');
                    break;

                case 'finish':
                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 1000);

                    //marY
                    pq_getObj("pq_slidebar<?php  echo $bID;?>").style.display = 'none';
                    pq_getObj("pq_back_button<?php  echo $bID;?>").style.display = "none";

                    prev_correct = response.getElementsByTagName('quiz_prev_correct')[0].firstChild.data;

                    var quiz_cont = pq_getObj('jq_quiz_container<?php  echo $bID;?>');
                    var children = quiz_cont.childNodes;
                    for (var i = 0; i < children.length; i++) { quiz_cont.removeChild(quiz_cont.childNodes[i]); };
                    quiz_cont.innerHTML = '<form name=\'quest_form\'></form>';

                    var qfrf = response.getElementsByTagName('quest_feedback_repl_func')[0].firstChild.data;
                    eval(qfrf);

                    break;

                case 'results':
                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 100);
                    var quiz_cont = pq_getObj('jq_quiz_container<?php  echo $bID;?>');
                    var children = quiz_cont.childNodes;
                    for (var i = 0; i < children.length; i++) { quiz_cont.removeChild(quiz_cont.childNodes[i]); };
                    quiz_cont.innerHTML = '<form name=\'quest_form\'></form>';
                    stop_timer = 1;
                    jq_UpdateTaskDiv('finish');
                    var finish_msg = response.getElementsByTagName('finish_msg')[0].firstChild.data;
                    var unique_report = response.getElementsByTagName('unique_report')[0].firstChild.data;


                    var quiz_cont = pq_getObj('jq_quiz_container<?php  echo $bID;?>');
                    quiz_cont.innerHTML = '<form name=\'quest_form\'></form>'+finish_msg;

                    saved_prev_res_data = finish_msg;
                    break;

                case 'failed':
                    ShowMessage('error_messagebox', 1, mes_failed,1);
                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 1000);
                    break;

                case 'showmess':

                    var qmb = response.getElementsByTagName('quiz_message_box')[0].firstChild.data;
                    ShowMessage('error_messagebox', 1, qmb,1);
                    pq_getObj('jq_quiz_container<?php  echo $bID;?>').innerHTML='';
                    quiz_blocked = 1;
                    setTimeout("jq_releaseBlock()", 1000);
                    break;

                default:
                    break;
            }
        } else {
            ShowMessage('error_messagebox', 1, 'Failed',1);
        }
    }
}
function jq_releaseBlock() {
    quiz_blocked = 0;
}
function jq_Start_TickTack() {
    timer_sec = max_quiz_time;
    var st_min = parseInt(max_quiz_time/60);

    var st_sec = parseInt(max_quiz_time) - parseInt(st_min*60);
    st_min = st_min + '';
    if (st_min.length == 1){ st_min = '0'+st_min};
    st_sec = st_sec + '';
    if (st_sec.length == 1){ st_sec = '0'+st_sec};
    pq_getObj('jq_time_tick_container<?php  echo $bID;?>').innerHTML = st_min+':'+st_sec;
    pq_getObj('jq_time_tick_container<?php  echo $bID;?>').style.visibility = "visible";
    setTimeout("jq_Continue_TickTack()", 1000);
}
function jq_Continue_TickTack() {
    if (stop_timer == 1) {
        pq_getObj('jq_time_tick_container<?php  echo $bID;?>').style.visibility = "hidden";
    } else {
        timer_sec --;
        if (timer_sec < 1) {
            pq_getObj('jq_time_tick_container<?php  echo $bID;?>').innerHTML = mes_time_is_up;
            jq_MakeRequest('&ajax_task=time_up&quiz=<?php  echo $quiz["qzid"]?>',1);
        } else {
            var timer_min = parseInt(timer_sec/60);
            var plus_sec = timer_sec - (timer_min*60);
            if (timer_min < 0) { timer_min = timer_min*(-1); }
            if (plus_sec < 0) { plus_sec = plus_sec*(-1); }
            var time_str = timer_min + '';
            if (time_str.length == 1) time_str = '0'+time_str;
            time_str2 = plus_sec + '';
            if (time_str2.length == 1) time_str2 = '0'+time_str2;
            pq_getObj('jq_time_tick_container<?php  echo $bID;?>').innerHTML = time_str + ':' + time_str2;
            setTimeout("jq_Continue_TickTack()", 1000);
        }
    }
}
function jq_StartQuizOn() {

    if (!quiz_blocked) {
        ShowMessage('jq_quiz_container', 1, mes_loading,0);
        timerID = setTimeout("jq_StartQuiz()", 300);
    } else {
        ShowMessage('error_messagebox', 1, mes_please_wait,1);
    }
}
function jq_StartQuiz() {
    jq_MakeRequest('&ajax_task=start&quiz=<?php  echo $quiz["qzid"]?>',1);
}

function jq_Check_selectRadio(rad_name, form_name) {
    var tItem = eval('document.'+form_name);
    if (tItem) {
        var selItem = eval('document.'+form_name+'.'+rad_name);
        if (selItem) {
            if (selItem.length) { var i;
                for (i = 0; i<selItem.length; i++) {
                    if (selItem[i].checked) {
                        //if (selItem[i].value > 0) { return selItem[i].value; } } }
                        return selItem[i].value;
                    }
                }
            } else if (selItem.checked) { return selItem.value; } }
        return false; }
    return false;
}
function jq_Check_selectCheckbox(check_name, form_name) {
    selItem = eval('document.'+form_name+'.'+check_name);
    var rrr = '';
    if (selItem) {
        if (selItem.length) { var i;
            for (i = 0; i<selItem.length; i++) {
                if (selItem[i].checked) {
                    if (selItem[i].value > 0) { rrr = rrr + selItem[i].value + ', '; }
                }}
            rrr = rrr.substring(0, rrr.length - 2);
        } else if (selItem.checked) { rrr = rrr + selItem.value; }}
    return rrr;
}
function jq_Check_valueItem(item_name, form_name) {
    selItem = eval('document.'+form_name+'.'+item_name);
    var rrr = '';
    if (selItem) {
        if (selItem.length) { var i;
            for (i = 0; i<selItem.length; i++) {
                if (selItem[i].value == '{0}') return '';
                rrr = rrr + selItem[i].value + '```';
            }
            rrr = rrr.substring(0, rrr.length - 3);
        } else { rrr = rrr + selItem.value;	}}
    return rrr;
}
function jq_QuizNextOn() {
    switch (cur_quest_type) {
        case '1': //Multi choice
            if (!jq_Check_selectRadio('quest_choice', 'quest_form')) {
                ShowMessage('error_messagebox', 1, mes_complete_this_part,1);
                return false;}
            break;
        case '2': //Multi Response
            var res = jq_Check_selectCheckbox('quest_choice', 'quest_form');
            if (res == '') {
                ShowMessage('error_messagebox', 1, mes_complete_this_part,1);
                return false;}
            break;
        case '3': //true-false
            if (!jq_Check_selectRadio('quest_choice', 'quest_form')) {
                ShowMessage('error_messagebox', 1, mes_complete_this_part,1);
                return false;}
            break;

        case '4': //survey question
            var answer = document.quest_form.free_text.value;
            if (TRIM_str(answer) == '') {
                ShowMessage('error_messagebox', 1, mes_complete_this_part,1);
                return false;}
            break;
    }
    if (!quiz_blocked) {

        quiz_blocked = 1;
        timerID = setTimeout("jq_QuizNext()", 300);
    } else { ShowMessage('error_messagebox', 1, mes_please_wait,1); setTimeout("jq_releaseBlock()", 1000); }
}
function jq_QuizContinue() {
    cur_quest_type = response.getElementsByTagName('quest_type')[0].firstChild.data;
    saved_prev_quest_type = cur_quest_type;
    cur_quest_id = response.getElementsByTagName('quest_id')[0].firstChild.data;
    saved_prev_quest_id = cur_quest_id;
    cur_quest_score = response.getElementsByTagName('quest_score')[0].firstChild.data;
    saved_prev_quest_score = cur_quest_score;
    cur_quest_num = response.getElementsByTagName('quiz_quest_num')[0].firstChild.data;
    saved_prev_quest_num = cur_quest_num;
    var quiz_cont = pq_getObj('jq_quiz_container<?php  echo $bID;?>');
    quiz_cont.innerHTML = '';

    if('<?php  echo $quiz["qz_return"]?>' == '1' && cur_quest_num != 0){
        pq_getObj("pq_back_button<?php  echo $bID;?>").style.display = "block";
        pq_getObj("pq_back_button<?php  echo $bID;?>").setAttribute ("onclick", "jq_QuizBack()");//jq_QuizBack
    }else{
        pq_getObj("pq_back_button<?php  echo $bID;?>").style.display = "none";
    }
    pq_getObj("pq_slidebar_inner<?php  echo $bID;?>").style.marginRight = 100-(parseInt(cur_quest_num)+1)*barstep + '%';

    var div_inside1=document.createElement("div");
    div_inside1.id = 'quest_div';
    div_inside1.innerHTML = response.getElementsByTagName('quest_data')[0].firstChild.data +response.getElementsByTagName('quest_data_user')[0].firstChild.data;
    saved_prev_quest_data = response.getElementsByTagName('quest_data')[0].firstChild.data + response.getElementsByTagName('quest_data_user')[0].firstChild.data;
    quiz_cont.appendChild(div_inside1);
    jq_UpdateTaskDiv('next');
}
function jq_QuizContinueFinish() {
    jq_MakeRequest('&ajax_task=finish_stop&quiz=<?php  echo $quiz["qzid"]?>'+'&stu_quiz_id='+stu_quiz_id, 1);
}
function jq_QuizBack() {

    jq_MakeRequest('&ajax_task=next&quiz=<?php  echo $quiz["qzid"]?>'+'&stu_quiz_id='+stu_quiz_id+'&quest_id='+cur_quest_id+'&backquest=1', 1);
}
function URLencode(sStr) {
    return escape(sStr).replace(/\+/g, '%2B').replace(/\"/g,'%22').replace(/\'/g, '%27').replace(/\//g,'%2F');
}
function TRIM_str(sStr) {
    return (sStr.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""));
}
function jq_QuizNext() {

    var jq_task = 'next';

    switch (cur_quest_type) {
        case '1':
        case '3':
            var answer = jq_Check_selectRadio('quest_choice', 'quest_form');
            if (answer) {
                jq_MakeRequest('&ajax_task=' + jq_task + '&quiz=<?php  echo $quiz["qzid"]?>'+'&stu_quiz_id='+stu_quiz_id+'&quest_id='+cur_quest_id+'&answer='+answer, 1);
            } else { ShowMessage('error_messagebox', 1, mes_complete_this_part,1); setTimeout("jq_releaseBlock()", 1000); return false; }
            break;
        case '2':
            var answer = jq_Check_selectCheckbox('quest_choice', 'quest_form');
            if (answer != '') {
                jq_MakeRequest('&ajax_task=' + jq_task + '&quiz=<?php  echo $quiz["qzid"]?>'+'&stu_quiz_id='+stu_quiz_id+'&quest_id='+cur_quest_id+'&answer='+answer, 1);
            } else { ShowMessage('error_messagebox', 1, mes_complete_this_part,1); setTimeout("jq_releaseBlock()", 1000); return false; }
            break;

        case '4':
            var answer = encodeURIComponent(TRIM_str(document.quest_form.free_text.value));
            if (answer != '') {
                jq_MakeRequest('&ajax_task=' + jq_task + '&quiz=<?php  echo $quiz["qzid"]?>'+'&stu_quiz_id='+stu_quiz_id+'&quest_id='+cur_quest_id+'&answer='+answer, 1);
            } else { ShowMessage('error_messagebox', 1, mes_complete_this_part,1); setTimeout("jq_releaseBlock()", 1000); return false; }
            break;

        default:
            ShowMessage('error_messagebox', 1, 'Failed',1);
            setTimeout("jq_releaseBlock()", 1000);
            break;

    }
}
function jq_UpdateTaskDiv(task) {
    switch (task) {
        case 'start':
            pq_getObj('jq_quiz_task_container<?php  echo $bID;?>').innerHTML = jq_StartButton('jq_StartQuizOn()', '<?php  echo t("Start");?>');
            break;
        case 'next':
            pq_getObj('jq_quiz_task_container<?php  echo $bID;?>').innerHTML = jq_NextButton('jq_QuizNextOn()', '<?php  echo t("Next");?>');
            break;
        case 'finish':

        case 'clear':
            pq_getObj('jq_quiz_task_container<?php  echo $bID;?>').innerHTML = "";
            break;
        case 'continue':
            pq_getObj('jq_quiz_task_container<?php  echo $bID;?>').innerHTML = jq_ContinueButton('jq_QuizContinue()', '');
            break;
        case 'continue_finish':
            pq_getObj('jq_quiz_task_container<?php  echo $bID;?>').innerHTML = jq_ContinueButton('jq_QuizContinueFinish()', '');
            break;


    }

    if (task == 'finish') {
        var obj_plc = pq_getObj('jq_panel_link_container<?php  echo $bID;?>');
        if (obj_plc) obj_plc.style.visibility = 'hidden';
    }
}
function jq_NextButton(task, text) {
    return "<div class=\"jq_next_link_container\" id=\"jq_next_link_container<?php  echo $bID;?>\" onClick=\""+task+"\"><div class=\"back_button\" id=\"jq_quiz_task_link_container<?php  echo $bID;?>\"><a href=\"javascript: void(0)\" title='"+text+"'></a></div><br /></div>";
}

function jq_ContinueButton(task, text) {
    return "<div class=\"jq_continue_link_container\" id=\"jq_continue_link_container<?php  echo $bID;?>\" onClick=\""+task+"\"><div class=\"back_button\" id=\"jq_quiz_task_link_container<?php  echo $bID;?>\"><a href=\"javascript: void(0)\" title='"+text+"'></a></div><br /></div>";
}
function jq_StartButton(task, text) {
    return "<div class=\"jq_start_link_container\" id=\"jq_start_link_container<?php  echo $bID;?>\" onClick=\""+task+"\"><div class=\"back_button\" id=\"jq_quiz_task_link_container<?php  echo $bID;?>\"><a href=\"javascript: void(0)\" title='"+text+"'></a></div><br /></div>";
}
function jq_BackButton(task, text) {
    return "<div class=\"jq_back_link_container\" id=\"jq_back_link_container<?php  echo $bID;?>\" onClick=\""+task+"\"><div class=\"back_button\" id=\"jq_quiz_task_link_container<?php  echo $bID;?>\"><a href=\"javascript: void(0)\" title='"+text+"'></a></div><br /></div>";
}


if (window.addEventListener)
    window.addEventListener("load", not_preview_mode, false);
else if (window.attachEvent)
    window.attachEvent("onload", not_preview_mode);
else
    window.onload=not_preview_mode;
function not_preview_mode(){
    jq_UpdateTaskDiv('start');

}
//--><!]]>
</script>
<div class="pq_main_container">
    <div>
        <div id="pq_header_id<?php  echo $bID;?>" class="pq_header"><?php  echo $quiz["qzName"];?></div>

        <div class="jq_time_tick_container" id="jq_time_tick_container<?php  echo $bID;?>"></div>
    </div>


    <div id="jq_quiz_container<?php  echo $bID;?>" class="pq_cont">
        <?php  echo $quiz["qzDescr"]?>
    </div>
    <div style="clear:both;"></div>
    <div class="pq_mess error_messagebox" id="error_messagebox<?php  echo $bID;?>"></div>
    <div class="pq_footer">
        <div class="pq_back_button" id="pq_back_button<?php  echo $bID;?>">
            <a href="javascript: void(0)" title="<?php  t('Previous');?>"></a>
        </div>
        <div id="jq_quiz_task_container<?php  echo $bID;?>"></div>
        <div class="pq_slidebar" id="pq_slidebar<?php  echo $bID;?>">
            <div class="pq_slidebar_inner" id="pq_slidebar_inner<?php  echo $bID;?>">
            </div>
        </div>
    </div>
</div>