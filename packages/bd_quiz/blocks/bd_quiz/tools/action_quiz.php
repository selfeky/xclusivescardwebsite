<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$jq_task = $_REQUEST['ajax_task'];
@ob_start();
$jq_ret_str = '';
switch ($jq_task) {
    case 'start':			$jq_ret_str = JQ_StartQuiz();		break;
    case 'next':			$jq_ret_str = JQ_NextQuestion();	    break;
    case 'finish_stop':		$jq_ret_str = JQ_FinishQuiz();		break;
    case 'time_up':			$jq_ret_str = PQ_TimeUp();			break;

    default:	break;
}
$debug_str = @ob_get_contents();

@ob_end_clean();

if ($jq_ret_str != "") {
    header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
    header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header ('Cache-Control: no-cache, must-revalidate');
    header ('Pragma: no-cache');
    header ('Content-Type: text/xml');
    echo '<?php xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    echo '<response>' . "\n";
    echo $jq_ret_str;
    echo "\t" . '<debug><![CDATA['.$debug_str.']]></debug>' . "\n";
    echo '</response>' . "\n";
} else {
    header ('Expires: Fri, 14 Mar 1980 20:53:00 GMT');
    header ('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header ('Cache-Control: no-cache, must-revalidate');
    header ('Pragma: no-cache');
    header ('Content-Type: text/xml');
    echo '<?php xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    echo '<response>' . "\n";
    echo "\t" . '<task>failed</task>' . "\n";
    echo "\t" . '<info>boom</info>' . "\n";
    echo "\t" . '<dqerrors><![CDATA['.$debug_str.']]></dqerrors>' . "\n";
    echo '</response>' . "\n";
}
die();

function JQ_StartQuiz() {
    $u = new User();
    $uID = intval( $u->getUserID() );
    $db = Loader::db();

    $ret_str = '';
    $quiz_id = intval($_REQUEST['quiz']);
    $unique_report = md5(uniqid(rand(), true));

    $quiz = $db->getAll('SELECT * FROM QzQuiz WHERE qzid = ?',array($quiz_id));

    if (count($quiz)) {
        $quiz = $quiz[0];
    } else {
        return $ret_str;
    }

    if ( !$u->isLoggedIn() && !$quiz["qz_guest"]) {

        return PQ_Mess(t('To pass this quiz you need to be authorized'));
    }

    $user_id = $u->isLoggedIn()?$uID:0;

    if ($quiz_id) {
        $unique_id = md5(uniqid(rand(), true));

        if(isset($_COOKIE['pq_quiz'.$quiz_id]) && $_COOKIE['pq_quiz'.$quiz_id]){
            $unique_id = $_COOKIE['pq_quiz'.$quiz_id];
        }else{
            setcookie('pq_quiz'.$quiz_id,$unique_id,time()+3600);
        }
        $query = "SELECT COUNT(*) FROM QzQuizReport"
            ." WHERE quiz_id = ? AND (uniqueid = ? "
            .($user_id?" OR user_id = ?":"").")";
        $v = array($quiz_id, $unique_id);
        if($user_id){
            $v = array($quiz_id, $unique_id, $user_id);
        }
        $attempts_count = $db->getOne($query, $v);
        $query = "SELECT COUNT(*) FROM QzQuizReport WHERE passed='1' AND quiz_id = ?"
            ." AND (uniqueid = ? "
            .($user_id?" OR user_id=?":"").")";
        $passed_count = $db->getOne($query, $v);

        if($attempts_count >= $quiz["qz_attempts"] && $quiz["qz_attempts"]){
            return PQ_Mess(t('You run out of attempts to pass this Quiz'));
        }
        if($passed_count){
            if($quiz["qz_passed_criteria"] == 2){

            }else{
                return PQ_Mess(t('Thank you, you have already answered the questions. We appreciate your opinion'));
            }
        }

        $v = array($user_id, $quiz_id, $unique_id, time(), $unique_report);
        $r = $db->prepare("INSERT INTO QzQuizReport(user_id,quiz_id,uniqueid,start_date,unique_report)"
            . " VALUES(?,?,?,?,?)");
        $db->execute($r, $v);
        $report_id = $db->Insert_ID();

        $q_data = $db->getAll("SELECT DISTINCT(q.q_id),q.* FROM QzQuestion as q ,QzQuestQuiz as z "
            . "WHERE z.quiz_id = ? AND q.q_id = z.quest_id ORDER BY z.ordering", array($quiz_id));

        $quest_ids = array();
        if($q_data){
            for($i=0;$i<count($q_data);$i++){
                $quest_ids[] = $q_data[$i]["q_id"];
            }
        }

        $q_fromcat = $db->getAll("SELECT * FROM QzQuizQfromcat as q "
            ." JOIN QzQcat as qc ON q.cat_id = qc.qc_id"
            ." WHERE q.quiz_id = ?",array($quiz_id));
        if($q_fromcat){
            for($i=0;$i<count($q_fromcat);$i++){
                $dopq = $q_fromcat[$i];

                $query = "SELECT q.* FROM QzQuestion as q ,QzQuestCat as c "
                    . " WHERE c.cat_id = ? AND q.q_id=c.quest_id "
                    .(count($quest_ids)?" AND q.q_id NOT IN (".implode(',',$quest_ids).")":"")
                    . "GROUP BY q.q_id ORDER BY rand() LIMIT ?";

                $q_data_cat = $db->getAll($query, array($dopq["cat_id"], intval($dopq["number"])));
                if($q_data_cat && count($q_data_cat)){
                    $q_data = array_merge($q_data,$q_data_cat);
                    for($j=0;$j<count($q_data_cat);$j++){
                        if($q_data_cat[$j]["q_id"]){
                            $quest_ids[] = $q_data_cat[$j]["q_id"];
                        }
                    }
                }
            }
        }

        $kol_quests = count($q_data);
        $quest_num = 0;

        if ($kol_quests > 0) {
            $numbers = range (0,($kol_quests - 1));
            if ($quiz["qz_random"]) {
                srand ((float)microtime()*1000000);
                shuffle ($numbers);
            }
            if($quiz["qz_questnumber"] && $quiz["qz_questnumber"] < $kol_quests){
                $kol_quests = $quiz["qz_questnumber"];
            }
            if(count($numbers)){
                $quest_num = $numbers[0];
            }
            $i = 0;

            while (list (, $number) = each ($numbers)) {
                if($i < $kol_quests){
                    $db->execute("INSERT INTO QzQuizChain(report_id,question_id,ordering) VALUES(?, ?, ?)",
                        array($report_id, $q_data[$number]["q_id"], $i));
                    $i++;
                }
            }
        }else{
            return PQ_Mess(t('This Quiz is empty. Please contact the site administrator'));
        }

        $ret_str .= "\t" . '<task>start</task>' . "\n";
        $ret_str .= "\t" . '<stu_quiz_id>'.$report_id.'</stu_quiz_id>' . "\n";
        $ret_str .= "\t" . '<user_unique_id>'.$unique_id.'</user_unique_id>' . "\n";
        $ret_str .= "\t" . '<quiz_count_quests>'.$kol_quests.'</quiz_count_quests>' . "\n";
        $ret_str .= "\t" . '<quiz_quest_num>1</quiz_quest_num>' . "\n";
        $ret_str .= JQ_GetQuestData($q_data[$quest_num],$report_id);

    } else {
        $ret_str = '';
    }

    return $ret_str;
}

function JQ_FinishQuiz() {
    $db = Loader::db();
    $u = new User();
    $ret_str = '';

    $quiz_id = intval( $_REQUEST['quiz'] );
    $quiz = $db->getAll('SELECT * FROM QzQuiz WHERE qzid = ?',array($quiz_id));
    if (count($quiz)) {
        $quiz = $quiz[0];
    } else {
        return $ret_str;
    }

    if ( !$u->isLoggedIn() && !$quiz["qz_guest"]) {
        return PQ_Mess(t('To pass this quiz you need to be authorized'));
    }

    if ($quiz_id) {
        $report_id = intval( $_REQUEST['stu_quiz_id'] );

        if ($report_id) {
            $q_beg_time = $db->getOne('SELECT start_date FROM QzQuizReport WHERE id = ?', array($report_id));
            $q_time_total = intval(time()) - intval($q_beg_time);
            $user_points = $db->getOne("SELECT SUM(points) FROM QzQuizReportQuest "
                . " WHERE report_id = ?",array($report_id));
            $overal_points = $db->getOne("SELECT SUM(q.q_point) FROM QzQuizChain as c, QzQuestion as q "
                . " WHERE q.q_id = c.question_id AND c.report_id = ?", array($report_id));

            $perc = 0;
            $successfull = 1;
            if($overal_points && $quiz["qz_passing"] && $quiz["qz_passed_criteria"]){
                $perc = round(($user_points/$overal_points)*100);
                if($perc < $quiz["qz_passing"]){
                    $successfull = 0;
                }
            }

            $finmsg = $successfull?$quiz["qz_passed_mess"]:$quiz["qz_failed_mess"];
            $v = array($user_points, $perc, $q_time_total, $successfull, $finmsg, $report_id);
            $db->execute("UPDATE QzQuizReport SET finished='1',points=?,percent=?,time_spend=?,"
                . " passed=?,result_text=? WHERE id = ?", $v);

            $user_points = $db->getOne("SELECT SUM(points) FROM QzQuizReportQuest"
                . " WHERE report_id = ?", array($report_id));

            $overal_points = $db->getOne("SELECT SUM(q.q_point) FROM QzQuizChain as c, QzQuestion as q"
                . " WHERE q.q_id = c.question_id AND c.report_id = ?", array($report_id));

            $unique_report = $db->getOne('SELECT unique_report FROM QzQuizReport WHERE id = ?',array($report_id));
            $quiz_played = $db->getOne("SELECT COUNT(unique_report) FROM QzQuizReport"
                ." WHERE quiz_id = ? AND finished='1'", array($quiz_id));

            $avg_points = $db->getOne('SELECT AVG(q.points) FROM QzQuizReport as q JOIN QzQuizReportQuest as qr'
                                 .' ON q.id = qr.report_id WHERE q.quiz_id = ? AND q.finished="1"', array($quiz_id));
            // user points
            $finmsg = str_replace('{points}',$user_points,$finmsg);
            // overall points
            $finmsg = str_replace('{total_points}',$overal_points,$finmsg);
            // user time
            $finmsg = str_replace('{time}',$q_time_total,$finmsg);
            // avarage point
            $finmsg = str_replace('{avg_points}',sprintf("%.2f",$avg_points),$finmsg);
            // quiz played count
            $finmsg = str_replace('{played}',$quiz_played,$finmsg);

            $ret_str .= "\t" . '<task>results</task>' . "\n";
            $ret_str .= "\t" . '<unique_report><![CDATA['.$unique_report.']]></unique_report>' . "\n";
            $ret_str .= "\t" . '<finish_msg><![CDATA['.$finmsg.']]></finish_msg>' . "\n";

        }
    }
    return $ret_str;
}

function PQ_TimeUp(){
    $msg_html = t("Your time is up");
    $ret_str = "\t" . '<task>time_is_up</task>' . "\n";
    $ret_str .= "\t" . '<quiz_message_box><![CDATA['.$msg_html.']]></quiz_message_box>' . "\n";

    return $ret_str;
}

function PQ_Mess($msg_html) {

    $ret_str = "\t" . '<task>showmess</task>' . "\n";
    $ret_str .= "\t" . '<quiz_message_box><![CDATA['.$msg_html.']]></quiz_message_box>' . "\n";

    return $ret_str;
}

function JQ_NextQuestion() {
    $db = Loader::db();
    $u = new User();
    $ret_str = '';
    $quiz_id = intval($_REQUEST['quiz']);
    $quiz = $db->getAll('SELECT * FROM QzQuiz WHERE qzid = ?',array($quiz_id));

    if (count($quiz)) {
        $quiz = $quiz[0];
    } else {
        return $ret_str;
    }

    if ( !$u->isLoggedIn() && !$quiz["qz_guest"]) {
        return PQ_Mess(t('To pass this quiz you need to be authorized'));
    }

    $report_id = intval( $_REQUEST['stu_quiz_id'] );
    $quest_id = intval( $_REQUEST['quest_id'] );
    $answer = strval( $_REQUEST['answer'] );
    $user_unique_id = strval( $_REQUEST['user_unique_id'] );
    $backquest = intval( $_REQUEST['backquest'] );

    if (($quiz_id) && ($report_id) && ($quest_id)) {

        if ($quiz["qz_time_limit"] && $quiz["qz_ticktack"]) {

            $quiz_time1 = time();
            $qtime = $db->getOne("SELECT start_date FROM QzQuizReport WHERE id = ?",array($report_id));
            $user_time = $quiz_time1 - $qtime;
            if ($user_time > ($quiz["qz_time_limit"])) {
                return PQ_TimeUp();
            }
        }

        $question = $db->getAll('SELECT q.* FROM QzQuestion as q WHERE q.q_id = ?',array($quest_id));

        if (count($question)) {
            $question = $question[0];
        } else {
            return $ret_str;
        }
        $is_correct = 0;
        if(!$backquest){
            switch($question["q_type"]){
                case '1':
                    $is_correct = $db->getOne('SELECT c_right FROM QzQuestChoice WHERE c_id = ?'
                        . ' AND c_question_id = ?',array($answer, $quest_id));
                    if($question["require_answer"] == 0){
                        $is_correct = 1;
                    }
                    break;

                case '2':
                    $correct_answers = $db->getAll('SELECT c_id FROM QzQuestChoice WHERE c_right="1"'
                        . ' AND c_question_id = ? ',array($quest_id));
                    $ans_array = explode(',',$answer);
                    if(count($correct_answers) == count($ans_array)){
                        $is_correct = 1;
                        foreach($correct_answers as $answ){
                            if(!in_array($answ["c_id"],$ans_array)){
                                $is_correct = 0;
                            }
                        }
                    }
                    if($question["require_answer"] == 0){
                        $is_correct = 1;
                    }
                    break;

                case '3':
                    if($question['qchoice'] == $answer){
                        $is_correct = 1;
                    }
                    break;

                case '4':
                    $is_correct = 1;
                    $answer = trim(urldecode($answer));
                    break;

                default:
                    break;
            }

            $points = 0;
            if($is_correct && $question["q_point"]){
                $points = $question["q_point"];
            }
            $v = array($report_id, $quest_id, $answer, $points, $is_correct, $answer, $points);
            $query = 'INSERT INTO QzQuizReportQuest (report_id,question_id,answer,points,`c_right`)'
                . ' VALUES(?, ?, ?, ?, ?)'
                . ' ON DUPLICATE KEY UPDATE answer = ?, points = ?';
            if($question["q_type"] == 4){
                $v = array($report_id, $quest_id, $answer, $points, $answer, $points);
                $query = 'INSERT INTO QzQuizReportQuest (report_id,question_id,answer_text,points)'
                    .' VALUES(?, ?, ?, ?)'
                    .' ON DUPLICATE KEY UPDATE answer = ?, points = ?';
            }
            $db->execute($query, $v);

            $quest_num = $db->getOne('SELECT ordering FROM QzQuizChain WHERE report_id = ? AND question_id = ?',
                array($report_id, $quest_id));

            $next_question_id = $db->getOne('SELECT question_id FROM QzQuizChain WHERE report_id = ?'
                .' AND ordering = ?',array($report_id, ($quest_num+1)));
        }else{
            $quest_num = $db->getOne('SELECT ordering FROM QzQuizChain WHERE report_id = ? AND question_id = ?',
                array($report_id, $quest_id));

            $next_question_id = $db->getOne('SELECT question_id FROM QzQuizChain WHERE report_id = ?'
                .' AND ordering = ?',array($report_id, ($quest_num-1)));
        }
        if($next_question_id){
            $q_data = $db->getAll('SELECT q.* FROM QzQuestion as q WHERE q.q_id = ?', array($next_question_id));
            $ret_str .= "\t" . '<task>next</task>' . "\n";
            $ret_str .= "\t" . '<quest_feedback_repl_func>'
                . '<![CDATA[jq_QuizContinue();]]>'
                . '</quest_feedback_repl_func>' . "\n";
            $ret_str .= "\t" . '<quiz_quest_num>'
                .($backquest?($quest_num-1):($quest_num+1))
                . '</quiz_quest_num>' . "\n";
            $ret_str .= JQ_GetQuestData($q_data[0], $report_id);
        }else{
            $ret_str .= "\t" . '<task>finish</task>' . "\n";
            $ret_str .= "\t" . '<quest_feedback_repl_func>'
                . '<![CDATA[jq_QuizContinueFinish();]]>'
                . '</quest_feedback_repl_func>' . "\n";
        }

        $ret_str .= "\t" . '<quiz_prev_correct>'.$is_correct.'</quiz_prev_correct>' . "\n";
        $ret_str .= "\t" . '<stu_quiz_id>'.$report_id.'</stu_quiz_id>' . "\n";
        $ret_str .= "\t" . '<user_unique_id>'.$user_unique_id.'</user_unique_id>' . "\n";

    }
    return $ret_str;
}

function JQ_GetQuestData($q_data,$report_id) {
    $db = Loader::db();

    $ret_add = '<div class="qdata_div">'.$q_data["qName"].'</div>';
    $ret_str = "\t" . '<quest_data><![CDATA['.$ret_add.']]></quest_data>' . "\n";
    $ret_str .= "\t" . '<quest_type>'.$q_data["q_type"].'</quest_type>' . "\n";
    $ret_str .= "\t" . '<quest_id>'.$q_data["q_id"].'</quest_id>' . "\n";
    $ret_str .= "\t" . '<quest_score><![CDATA['.$q_data["q_point"].']]></quest_score>' . "\n";
    //--if randomize quest
    $qrandom = $db->getOne("SELECT q_random from QzQuestion WHERE q_id = ?",array($q_data["q_id"]));

    //answered?
    $v = array($report_id, $q_data["q_id"]);
    $query = 'SELECT answer FROM QzQuizReportQuest WHERE report_id=? AND question_id=?';
    $answ_isset = $db->getOne('SELECT COUNT(*) FROM QzQuizReportQuest WHERE report_id=?'
        .' AND question_id=?', $v);
    if($q_data["q_type"] == 4){
        $query = 'SELECT answer_text FROM QzQuizReportQuest WHERE report_id=? AND question_id=?';
    }
    $answered = $db->getOne($query, $v);
    switch ($q_data["q_type"]) {
        case 1:

            $query = "SELECT c_id as value, c_choice as text, '0' as c_right FROM QzQuestChoice "
                    ." WHERE c_question_id = ?";
            if ($qrandom){
                $query .=  "\n ORDER BY rand()";
            }else{
                $query .=  "\n ORDER BY ordering";
            }
            $choice_data = $db->getAll($query, array($q_data["q_id"]));
            $z = 1;
            $qhtml = "<table align='left'>" . "\n";
            foreach ($choice_data as $qone) {
                $qhtml .= "<tr><td><input id='quest_choice_".$z."' name='quest_choice' value='".(isset
                ($qone["value"])
                    ?$qone["value"]:"")."' type='radio' ".((isset($answered) && ($qone["value"] == $answered))
                    ?"checked":"")." ></td><td align='left' class='quest_pos'><label for='quest_choice_".$z."'>".
                    stripslashes($qone["text"])."</label></td>" . "\n";

                $qhtml .= "</tr>" . "\n";
                $z++;
            }

            $qhtml .= "</table>" . "\n";

            $ret_str .= "\t" . '<quest_data_user><![CDATA[<div class="div_answr"><form name=\'quest_form\'>'
                . $qhtml . '</form></div>]]></quest_data_user>' . "\n";
            break;

        case 2:
            $query = "SELECT c_id as value, c_choice as text, '0' as c_right FROM QzQuestChoice WHERE "
               ." c_question_id = ?";

            if ($qrandom){
                $query .=  "\n ORDER BY rand()";
            }else{
                $query .=  "\n ORDER BY ordering";
            }
            $choice_data = $db->getAll($query, array($q_data["q_id"]));

            $qhtml = "<table align='left'>" . "\n";
            if(isset($answered)){
                $answered = explode(',',$answered);
            }
            $z = 1;
            foreach ($choice_data as $qone) {
                $qhtml .= "<tr><td>
                    <input id='quest_choice_".$z."' name='quest_choice'
                        value='".(isset($qone["value"])?$qone["value"]:"")."'
                        type='checkbox' ".((isset($answered) && in_array($qone["value"],$answered))?"checked":"")." />
                     </td>
                     <td align='left' class='quest_pos'>
                        <label for='quest_choice_".$z."'>".stripslashes($qone["text"])."</label>
                     </td>" . "\n";

                $qhtml .= "</tr>" . "\n";
                $z++;
            }

            $qhtml .= "</table>" . "\n";

            $ret_str .= "\t" . '<quest_data_user><![CDATA[<div class="div_answr"><form name=\'quest_form\'>'
                . $qhtml . '</form></div>]]></quest_data_user>' . "\n";
            break;

        case 3:
            $qhtml = "<table align='left'>" . "\n";

            $qhtml .= "<tr><td><input id='quest_choice_1' name='quest_choice' value='1' type='radio'
                ".((isset($answered) && $answered)?"checked":"")."></td>
                <td align='left' class='quest_pos'><label for='quest_choice_1'>".t("True")."</label></td>" . "\n";

            $qhtml .= "</tr>" . "\n";

            $qhtml .= "<tr><td><input id='quest_choice_2' name='quest_choice' value='0' type='radio'
                ".(($answ_isset && $answered == 0)?"checked":"")."></td>
                <td align='left' class='quest_pos'><label for='quest_choice_2'>".t("False")."</label></td>" . "\n";

            $qhtml .= "</tr>" . "\n";


            $qhtml .= "</table>" . "\n";

            $ret_str .= "\t" . '<quest_data_user><![CDATA[<div class="div_answr"><form name=\'quest_form\'>'
                . $qhtml . '</form></div>]]></quest_data_user>' . "\n";
            break;


        case 4:
            $qhtml = '<textarea cols="50" rows="10" name="free_text" id="free_text">'
                .((isset($answered) && $answered)?$answered:"") . '</textarea>';
            $ret_str .= "\t" . '<quest_data_user><![CDATA[<div class="div_answr"><form name=\'quest_form\'>'
                . $qhtml . '</form></div>]]></quest_data_user>' . "\n";
            break;

        default:
            break;
    }

    return $ret_str;
}