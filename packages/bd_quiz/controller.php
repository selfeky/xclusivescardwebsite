<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
class BdQuizPackage extends Package {
	protected $pkgHandle = 'bd_quiz';
	protected $appVersionRequired = '5.5.0';
	protected $pkgVersion = '1.2';
	public function getPackageDescription() {
		return t("QuizMaker");
	}
	public function getPackageName() {
		return t("QuizMaker");
	}
	public function install() {
		$pkg = parent::install();
		$this->installDashboard($pkg);
        $this->setupDashboardIcons();
        BlockType::installBlockTypeFromPackage('bd_quiz', $pkg);
        $db = Loader::db();
        $qtypes = $db->getOne("SELECT COUNT(*) FROM QzQuestTypes");
        if(!$qtypes){
            $db->execute("insert into QzQuestTypes (`id`, `name`) values (1,'Single answer')");
            $db->execute("insert into QzQuestTypes (`id`, `name`) values (2,'Multiple answer')");
            $db->execute("insert into QzQuestTypes (`id`, `name`) values (3,'True/False')");
            $db->execute("insert into QzQuestTypes (`id`, `name`) values (4,'Free Text answer')");
        }
	}
	
	function installDashboard($pkg) {
	   Loader::model('single_page');
	   $dashPages = array(
		  array('pkg'=>$pkg,'path'=>'/dashboard/bd_quiz','name'=>t("QuizMaker"),'description'=>$this->pkgDescription),
		  array('pkg'=>$pkg,'path'=>'/dashboard/bd_quiz/quizess','name'=>t("Quizzes"),
              'description'=>$this->pkgDescription),
		  array('pkg'=>$pkg,'path'=>'/dashboard/bd_quiz/qcat','name'=>t("Question Categories"),
              'description'=>$this->pkgDescription),
		  array('pkg'=>$pkg,'path'=>'/dashboard/bd_quiz/question','name'=>t("Questions"),
              'description'=>$this->pkgDescription),
		  array('pkg'=>$pkg,'path'=>'/dashboard/bd_quiz/report','name'=>t("Report"),
              'description'=>$this->pkgDescription)
	   );
	   foreach ($dashPages as $dashPage) {
		  $dp = Page::getByPath($dashPage['path']);
		  if($dp->cID==0) {
			$sp = SinglePage::add($dashPage['path'],$dashPage['pkg']);
			$sp->update(array('cName'=>t($dashPage['name']), 'cDescription'=>t($dashPage['description'])));
		  }
	   }
   }
    private function setupDashboardIcons() {
        $cak = CollectionAttributeKey::getByHandle('icon_dashboard');
        if (is_object($cak)) {
            $iconArray = array(
                // Change to list your single pages and their icons
                '/dashboard/bd_quiz/quizess' => 'icon-check',
                '/dashboard/bd_quiz/qcat' => 'icon-list',
                '/dashboard/bd_quiz/question' => 'icon-question-sign',
                '/dashboard/bd_quiz/report' => 'icon-signal',
            );
            foreach($iconArray as $path => $icon) {
                $sp = Page::getByPath($path);
                if (is_object($sp) && (!$sp->isError())) {
                    $sp->setAttribute('icon_dashboard', $icon);
                }
            }
        }
    }
	
}