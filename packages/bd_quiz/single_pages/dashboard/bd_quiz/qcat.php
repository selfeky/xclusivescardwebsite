<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));    
$form = loader::helper('form');
if ($this->controller->getTask() == 'qcat_add' || $this->controller->getTask() == 'save') {
    echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Question Category Add'), t('Question Category Add.')); ?>

    <div class="ccm-pane-body">

        <form method="post" enctype="multipart/form-data" action="<?php  echo $this->action('save')?>" id="aQCatForm">

            <div id="pq_curTab" style="padding:10px;">
                <div class="clearfix">
                    <?php  echo $form->label('c_title', t('Name:'))?>
                    <div class="input">
                        <?php  echo $form->text('c_title', isset($eQcat["qcName"])?$eQcat["qcName"]:'')?><sup> *</sup>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('qcDescr', t('Category description:'))?>
                    <div class="input">
                        <input type="hidden" name="qc_id" value="<?php  echo isset($eQcat["qc_id"])?$eQcat["qc_id"]:''?>"/>
                        <?php  echo $form->textarea('qcDescr', isset($eQcat["qcDescr"])?$eQcat["qcDescr"]:'', array('rows' => 5, 'cols' => 30))?>
                    </div>
                </div>

                <?php  echo Loader::helper('concrete/interface')->submit(t('Save'), 'save', 'right')?>
                <div class="small"><sup>*</sup><?php  echo t('Required fields')?></div>
            </div>
        </form>
    </div>
<?php 
}else{

echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Question Category'),
    t('Question Category.'), false, false); ?>
<script type="text/javascript">
    ccm_setupQuiz =  function (){
        $("#ccm-quiz-list-cb-all").click(function() {
            if ($(this).prop('checked') == true) {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', true);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', false);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            if ($("td.ccm-user-list-cb input[type=checkbox]:checked").length > 0) {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });

        $("#ccm-quiz-list-multiple-operations").change(function() {
            var action = $(this).val();
            var fIDstring = ccm_alGetSelectedQuizIDs();
            switch(action) {
                case "delete":
                    if(confirm("<?php  echo t('Are you sure?');?>"))    {
                        window.location.href = '<?php  echo $this->url("/dashboard/bd_quiz/qcat/delete?")?>' +
                                fIDstring;
                    }

                    break;
            }

            $(this).get(0).selectedIndex = 0;
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            ccm_alRescanMultiQuizMenu();
        });
    }
    ccm_alRescanMultiQuizMenu = function() {
        if ($("td.ccm-quiz-list-cb input[type=checkbox]:checked").length > 0) {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
        } else {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
        }
    }
    ccm_alGetSelectedQuizIDs = function() {
        var fidstr = '';
        $("td.ccm-quiz-list-cb input[type=checkbox]:checked").each(function() {
            fidstr += 'qID[]=' + $(this).val() + '&';
        });
        return fidstr;
    }
    $(function() {
        ccm_setupQuiz();
    });
</script>
<div class="ccm-pane-body">
<div id="ccm-list-wrapper">
    <div style="margin-bottom: 10px">
        <?php   $form = Loader::helper('form'); ?>
        <a href="<?php  echo View::url('/dashboard/bd_quiz/qcat/qcat_add')?>" style="float: right" class="btn primary">
            <?php  echo  t("Add Question Category")?>
        </a>
        <select id="ccm-quiz-list-multiple-operations" class="span3" disabled>
            <option value="">** <?php  echo t('With Selected')?></option>
            <option value="delete"><?php  echo t('Delete')?></option>
        </select>
    </div>
</div>
<div>

		<?php   
			$nh = Loader::helper('navigation');
			if ($qcatList->getTotal() > 0) {

				?>

				<table border="0" class="ccm-results-list" cellspacing="0" cellpadding="0">
					<tr>
                        <th width="1"><input id="ccm-quiz-list-cb-all" type="checkbox" /></th>
                        <th class="<?php   echo $qcatList->getSearchResultsClass('qcName')?>"><a href="<?php   echo $qcatList->getSortByURL('qcName', 'asc')?>"><?php   echo t('Name')?></a></th>

					</tr>
					<?php 
					foreach($qcat as $cobj) { ?>
                        <tr>
                            <td class="ccm-quiz-list-cb" style="vertical-align: middle !important">
                                <input type="checkbox" value="<?php  echo $cobj["qc_id"];?>" />
                            </td>
                            <td>
                                <a href="<?php  echo View::url('/dashboard/bd_quiz/qcat/qcat_add?eId='.$cobj["qc_id"])
                                ;?>">
                                    <?php  echo strip_tags($cobj["qcName"]);?>
                                </a>
                            </td>
                        </tr>
					<?php 
					}
					?>

				</table>
				<br/>
				<?php 
                $qcatList->displaySummary();
                $qcatList->displayPaging();
			} else {
				print t('No Question entries found.');
			}
	?>
	</div>
</div>
	<div class='ccm-pane-footer'></div>
<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);
}?>