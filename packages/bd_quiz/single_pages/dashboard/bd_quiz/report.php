<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$form = loader::helper('form');
if ($this->controller->getTask() == 'show') {
    $title = $report[0]["usern"]." ".t("report for ")." ".$report[0]["qzName"]." ".t("quiz");
    echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper($title,$title); ?>

<div class="ccm-pane-body">


        <div id="pq_curTab" style="padding:10px;">
            <table class="ccm-results-list" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <th><?php  echo t("Question");?></th>
                    <th><?php  echo t("Answers");?></th>
                    <th><?php  echo t("Correct answers");?></th>
                </tr>
                <?php 
                for($j=0;$j<count($report_answer);$j++){
                    echo "<tr>";
                    echo "<td>".$report_answer[$j]["qName"]."</td>";
                    echo "<td>";
                    if($report_answer[$j]["q_type"] == 4 || $report_answer[$j]["q_type"] == 3){
                        echo $report_answer[$j]["uanswer"];
                    }else{
                        for($z=0;$z<count($report_answer[$j]["uanswer"]);$z++){
                            echo $report_answer[$j]["uanswer"][$z]["c_choice"]."<br />";
                        }
                    }
                    echo "</td>";
                    echo "<td>";
                    if($report_answer[$j]["q_type"] == 3){
                        echo $report_answer[$j]["qchoice"]?t("True"):t("False");
                    }else{
                        for($z=0;$z<count($report_answer[$j]["ranswer"]);$z++){
                            echo $report_answer[$j]["ranswer"][$z]["c_choice"]."<br />";
                        }
                    }
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
            </table>

        </div>

</div>
<?php 
}else{

    echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Report'), t('Report.'), false,
        false); ?>
<script type="text/javascript">
    ccm_setupQuiz =  function (){
        $("#ccm-quiz-list-cb-all").click(function() {
            if ($(this).prop('checked') == true) {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', true);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', false);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            if ($("td.ccm-user-list-cb input[type=checkbox]:checked").length > 0) {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });

        $("#ccm-quiz-list-multiple-operations").change(function() {
            var action = $(this).val();
            var fIDstring = ccm_alGetSelectedQuizIDs();
            switch(action) {
                case "delete":
                    if(confirm("<?php  echo t('Are you sure?');?>"))    {
                        window.location.href = '<?php  echo $this->url("/dashboard/bd_quiz/report/delete?")?>' +
                                fIDstring;
                    }

                    break;
            }

            $(this).get(0).selectedIndex = 0;
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            ccm_alRescanMultiQuizMenu();
        });
    }
    ccm_alRescanMultiQuizMenu = function() {
        if ($("td.ccm-quiz-list-cb input[type=checkbox]:checked").length > 0) {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
        } else {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
        }
    }
    ccm_alGetSelectedQuizIDs = function() {
        var fidstr = '';
        $("td.ccm-quiz-list-cb input[type=checkbox]:checked").each(function() {
            fidstr += 'qID[]=' + $(this).val() + '&';
        });
        return fidstr;
    }
    $(function() {
        ccm_setupQuiz();
    });
</script>
<div class="ccm-pane-body">
<div id="ccm-list-wrapper">
    <div style="margin-bottom: 10px">
        <form method="post" action="<?php  echo $this->action('')?>" name="qfForm" id="qfForm">
            <?php   $form = Loader::helper('form'); ?>
            <select id="ccm-quiz-list-multiple-operations" class="span3" disabled>
                <option value="">** <?php  echo t('With Selected')?></option>
                <option value="delete"><?php  echo t('Delete')?></option>
            </select>
            <?php 
                $js = array('onchange' => "this.form.submit();");
                echo $form->select("quiz_id",$quiz,$quiz_id,$js);
            ?>
        </form>
    </div>
</div>
<div>

    <?php 
    $nh = Loader::helper('navigation');
    if ($qcatList->getTotal() > 0) {

        ?>

        <table border="0" class="ccm-results-list" cellspacing="0" cellpadding="0">
            <tr>
                <th width="1"><input id="ccm-quiz-list-cb-all" type="checkbox" /></th>
                <th class="<?php   echo $qcatList->getSearchResultsClass('usern')?>">
                    <a href="<?php   echo $qcatList->getSortByURL('usern', 'asc', false, array("quiz_id" => $quiz_id))?>">
                        <?php   echo t('User')?>
                    </a>
                </th>
                <th class="<?php   echo $qcatList->getSearchResultsClass('qzName')?>">
                    <a href="<?php   echo $qcatList->getSortByURL('qzName', 'asc', false, array("quiz_id" => $quiz_id))?>">
                        <?php   echo t('Quiz')?>
                    </a>
                </th>
                <th class="<?php   echo $qcatList->getSearchResultsClass('start_dt')?>">
                    <a href="<?php   echo $qcatList->getSortByURL('start_dt', 'asc', false, array("quiz_id" => $quiz_id))?>">
                        <?php   echo t('Date')?>
                    </a>
                </th>
                <th class="<?php   echo $qcatList->getSearchResultsClass('time_spend')?>">
                    <a href="<?php   echo $qcatList->getSortByURL('time_spend', 'asc', false, array("quiz_id" => $quiz_id))?>">
                        <?php   echo t('Time spend')?>
                    </a>
                </th>
                <th class="<?php   echo $qcatList->getSearchResultsClass('points')?>">
                    <a href="<?php   echo $qcatList->getSortByURL('points', 'asc', false, array("quiz_id" => $quiz_id))?>">
                        <?php   echo t('Points')?>
                    </a>
                </th>
                <th class="<?php   echo $qcatList->getSearchResultsClass('passed')?>">
                    <a href="<?php   echo $qcatList->getSortByURL('passed', 'asc', false, array("quiz_id" => $quiz_id))?>">
                        <?php   echo t('Passed')?>
                    </a>
                </th>
            </tr>
            <?php 
            foreach($qcat as $cobj) { ?>
                <tr>
                    <td class="ccm-quiz-list-cb" style="vertical-align: middle !important"><input type="checkbox" value="<?php  echo $cobj["id"];?>" /></td>
                    <td>
                        <?php  echo strip_tags($cobj["usern"]);?>
                    </td>
                    <td><?php  echo substr(strip_tags($cobj["qzName"]),0,70);?></td>
                    <td><?php  echo ($cobj["start_dt"]);?></td>
                    <td><?php  echo ($cobj["time_spend"]);?></td>
                    <td><?php  echo ($cobj["points"]);?></td>
                    <td><?php  echo ($cobj["passed"]?t("Yes"):t("No"));?></td>
                </tr>
                <?php 
            }
            ?>

        </table>
        <br/>
        <?php 
        $qcatList->displaySummary();
        $qcatList->displayPaging(false,false,array("quiz_id" => $quiz_id));
    } else {
        print t('No Report entries found.');
    }
    ?>
</div>
</div>
<div class='ccm-pane-footer'></div>
<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);
}?>