<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));    
$form = loader::helper('form');
Loader::element('editor_config');
define('QUIZPATH_DIR',BASE_URL.'/packages/bd_quiz/single_pages/dashboard/bd_quiz/');
if ($this->controller->getTask() == 'add' || $this->controller->getTask() == 'save') {
    echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Question Add'), t('Question Add.'),
        false, false
    ); ?>
    <script type="text/javascript">

        function eqFormSubmit(button){
            var form = document.aQuestForm;
            var srcListName = 'qcats_in';
            var srcList = eval( 'form.' + srcListName );

            var srcLen = srcList.length;

            for (var i=0; i < srcLen; i++) {
                srcList.options[i].selected = true;
            }

            var srcListName2 = 'quizess_in';
            var srcList2 = eval( 'form.' + srcListName2 );

            var srcLen2 = srcList2.length;

            for (var i=0; i < srcLen2; i++) {
                srcList2.options[i].selected = true;
            }


            var is_rght = 1;

            if(form.c_type.value == 1 || form.c_type.value == 2){


                var ransw = eval( 'document.aQuestForm["jq_checked\[\]"]');
                if(ransw){
                    if(form.require_answer4.checked){
                        var is_rght = 0;
                        var ransw2 = ransw.length;
                        if(ransw2){
                            for (var i=0; i < ransw2; i++) {
                                if(ransw[i].checked){
                                    is_rght = 1;
                                }
                            }
                        }else{
                            if(ransw.checked){
                                is_rght = 1;
                            }
                        }
                    }
                }else{
                    alert('<?php  echo t('Please add the answer(s) to the question.');?>');
                    return
                }

            }
            if(is_rght){
                if(form.q_point.value == ''){
                    if((form.c_type.value == 1 || form.c_type.value == 2) && form.require_answer4.checked == 0){
                        form.submit();
                    }else{
                        alert('<?php  echo t('Please specify points.');?>');
                    }

                }else{
                    form.submit();
                }
            }else{
                alert('<?php  echo t('Please specify the correct answer(s).');?>');
            }
            return;
        }
        function JS_addSelectedToList( frmName, srcListName, tgtListName ) {
            var form = eval( 'document.' + frmName );
            var srcList = eval( 'form.' + srcListName );
            var tgtList = eval( 'form.' + tgtListName );

            var srcLen = srcList.length;
            var tgtLen = tgtList.length;
            var tgt = "x";

            for (var i=tgtLen-1; i > -1; i--) {
                tgt += "," + tgtList.options[i].value + ","
            }

            for (var i=0; i < srcLen; i++) {

                if (srcList.options[i].selected && tgt.indexOf( "," + srcList.options[i].value + "," ) == -1) {
                    opt = new Option( srcList.options[i].text, srcList.options[i].value );
                    tgtList.options[tgtList.length] = opt;
                }
            }

            JS_delFFF(srcList);

        }

        function JS_delFFF(srcList){
            var srcLen = srcList.length;

            for (var i=srcLen-1; i > -1; i--) {
                if (srcList.options[i].selected) {
                    srcList.options[i] = null;
                }
            }
        }

        function JS_delSelectedFromList( frmName, srcListName, tgtListName ) {
            var form = eval( 'document.' + frmName );
            var srcList = eval( 'form.' + srcListName );

            var srcLen = srcList.length;
            JS_addSelectedToList(frmName,srcListName,tgtListName);
            for (var i=srcLen-1; i > -1; i--) {
                if (srcList.options[i].selected) {
                    srcList.options[i] = null;
                }
            }

        }
    </script>
    <div class="ccm-pane-body">

        <form method="post" enctype="multipart/form-data" action="<?php  echo $this->action('save')?>" id="aQuestForm" name="aQuestForm">

            <div id="pq_curTab" style="padding:10px;">
                <div class="clearfix">
                    <?php  echo $form->label('qName', t('Question body:'))?>
                    <div class="input">
                        <input type="hidden" name="q_id" value="<?php  echo isset($eQuest["q_id"])?$eQuest["q_id"]:'';?>"/>
                        <?php  Loader::element('editor_controls'); ?>
                        <?php  echo $form->textarea('qName', isset($eQuest["qName"])?$eQuest["qName"]:'', array('rows' => 5, 'cols' => 30, 'class' => 'ccm-advanced-editor'))?><sup> *</sup>
                    </div>
                </div>
                <div class="clearfix">
                    <?php  echo $form->label('q_point', t('Points:'))?>
                    <div class="input">
                        <?php  echo $form->text('q_point', isset($eQuest["q_point"])?$eQuest["q_point"]:'')?>
                    </div>
                </div>
                <?php 
                switch($qtype){
                    case '1': QChoiceForm($eQuest,$choices,1);  break;
                    case '2': QChoiceForm($eQuest,$choices,2);  break;
                    case '3': QTrueForm($eQuest);  break;
                }
                ?>
                <label><?php  echo t("Add Question to Categories:");?></label>
                <table  border="0">
                    <tr>
                        <td width="150" style="text-align:right">
                            <select name="qcats[]" id="qcats" multiple size="10" style="min-width: 100px;">
                                <?php 
                                if(count($qcats)){
                                    foreach($qcats as $qcat){
                                        echo '<option value="'.$qcat["qc_id"].'">'.$qcat["qcName"].'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td valign="middle" width="60" align="center">
                            <input type="button" class="button" style="cursor:pointer;" value=">>" onClick="javascript:JS_addSelectedToList('aQuestForm','qcats','qcats_in');" /><br />
                            <input type="button" class="button" style="cursor:pointer;" value="<<" onClick="javascript:JS_delSelectedFromList('aQuestForm','qcats_in','qcats');" />
                        </td>
                        <td >
                            <select name="qcats_in[]" id="qcats_in" multiple size="10" style="min-width: 100px;">
                                <?php 
                                if(count($qcats_in)){
                                    foreach($qcats_in as $qcatin){
                                        echo '<option value="'.$qcatin["qc_id"].'">'.$qcatin["qcName"].'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <label><?php  echo t("Add Question to Quizzes:");?></label>
                <table  border="0">
                    <tr>
                        <td width="150" style="text-align:right">
                            <select name="quizess[]" id="quizess" multiple size="10" style="min-width: 100px;">
                                <?php 
                                if(count($quizess)){
                                    foreach($quizess as $quiz){
                                        echo '<option value="'.$quiz["qzid"].'">'.$quiz["qzName"].'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                        <td valign="middle" width="60" align="center">
                            <input type="button" class="button" style="cursor:pointer;" value=">>" onClick="javascript:JS_addSelectedToList('aQuestForm','quizess','quizess_in');" /><br />
                            <input type="button" class="button" style="cursor:pointer;" value="<<" onClick="javascript:JS_delSelectedFromList('aQuestForm','quizess_in','quizess');" />
                        </td>
                        <td >
                            <select name="quizess_in[]" id="quizess_in" multiple size="10" style="min-width: 100px;">
                                <?php 
                                if(count($quizess_in)){
                                    foreach($quizess_in as $quizessin){
                                        echo '<option value="'.$quizessin["qzid"].'">'.$quizessin["qzName"].'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <?php  echo $form->button('save', t('Save'), array('style' => 'float:right', 'onclick' => 'javascript:eqFormSubmit();'))?>
                <div class="small"><sup>*</sup><?php  echo t('Required fields')?></div>
            </div>
            <input type="hidden" name="c_type" value="<?php  echo $qtype;?>" />
            <input type="hidden" name="q_id" value="<?php  echo isset($eQuest["q_id"])?$eQuest["q_id"]:0;?>" />
        </form>
    </div>
<?php 
}else{

echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Question'), t('Question.'), false,
    false); ?>
<script type="text/javascript">
    ccm_setupQuiz =  function (){
        $("#ccm-quiz-list-cb-all").click(function() {
            if ($(this).prop('checked') == true) {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', true);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', false);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            if ($("td.ccm-user-list-cb input[type=checkbox]:checked").length > 0) {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });

        $("#ccm-quiz-list-multiple-operations").change(function() {
            var action = $(this).val();
            var fIDstring = ccm_alGetSelectedQuizIDs();
            switch(action) {
                case "delete":

                    if(confirm("<?php  echo t('Are you sure?');?>"))    {
                        window.location.href = '<?php  echo $this->url("/dashboard/bd_quiz/question/delete?")?>' +
                                fIDstring;
                    }

                    break;
            }

            $(this).get(0).selectedIndex = 0;
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            ccm_alRescanMultiQuizMenu();
        });
    }
    ccm_alRescanMultiQuizMenu = function() {
        if ($("td.ccm-quiz-list-cb input[type=checkbox]:checked").length > 0) {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
        } else {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
        }
    }
    ccm_alGetSelectedQuizIDs = function() {
        var fidstr = '';
        $("td.ccm-quiz-list-cb input[type=checkbox]:checked").each(function() {
            fidstr += 'qID[]=' + $(this).val() + '&';
        });
        return fidstr;
    }
    $(function() {
        ccm_setupQuiz();
    });
</script>
<div class="ccm-pane-body">
<div id="ccm-list-wrapper">
    <form method="post" enctype="multipart/form-data" action="<?php  echo $this->action('add')?>" id="addQuestForm" name="addQuestForm">
        <div style="margin-bottom: 10px">
            <?php   $form = Loader::helper('form'); ?>
            <a href="javascript:document.addQuestForm.submit();" style="float: right" class="btn primary"><?php  echo t("Add Question")?></a>
            <div style="float: right;"><?php  echo $form->select('qtype',$qtypes);?>&nbsp;</div>
            <select id="ccm-quiz-list-multiple-operations" class="span3" disabled>
                <option value="">** <?php  echo t('With Selected')?></option>
                <option value="delete"><?php  echo t('Delete')?></option>
            </select>
        </div>
    </form>
</div>
<div>

		<?php   
			$nh = Loader::helper('navigation');
			if ($questList->getTotal() > 0) {

				?>

				<table border="0" class="ccm-results-list" cellspacing="0" cellpadding="0">
					<tr>
                        <th width="1"><input id="ccm-quiz-list-cb-all" type="checkbox" /></th>
                        <th class="<?php   echo $questList->getSearchResultsClass('qName')?>"><a href="<?php   echo $questList->getSortByURL('qName', 'asc')?>"><?php   echo t('Body')?></a></th>
						<th class="<?php   echo $questList->getSearchResultsClass('q_point')?>"><a href="<?php   echo $questList->getSortByURL('q_point', 'asc')?>"><?php   echo t('Points')?></a></th>

					</tr>
					<?php 
					foreach($quest as $cobj) { ?>
                        <tr>
                            <td class="ccm-quiz-list-cb" style="vertical-align: middle !important"><input type="checkbox" value="<?php  echo $cobj["q_id"];?>" /></td>
                            <td><a href="<?php  echo View::url('/dashboard/bd_quiz/question/add?eId='.$cobj["q_id"])
                                ?>"><?php  echo substr(strip_tags($cobj["qName"]),0,80);?></a></td>
                            <td><?php  echo $cobj["q_point"];?></td>
                        </tr>
					<?php 
					}
					?>

				</table>
				<br/>
				<?php 
                $questList->displaySummary();
                $questList->displayPaging();
			} else {
				print t('No Question entries found.');
			}
	?>
	</div>
   </div>
	<div class='ccm-pane-footer'></div>
<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);
}?>

<?php 
function QChoiceForm($obj,$choices,$qtype){
    JSFunc($qtype);
    $form = loader::helper('form');

$styvis = $obj['require_answer']?'':'style="visibility:hidden;"';
?>
<div class="clearfix">
    <?php  echo $form->label('q_random', t('Randomize answers?:'));?>
    <div class="input">
        <label style="display: inline;">
            <?php  echo $form->radio('q_random',0,isset($obj["q_random"])?$obj["q_random"]:0, array("style" => "display:inline;"));?>
            <span><?php  echo t('False');?></span>
        </label>
        <label style="display: inline; margin-left: 15px;">
            <?php  echo $form->radio('q_random',1,isset($obj["q_random"])?$obj["q_random"]:0, array("style" => "display:inline;"));?>
            <span><?php  echo t('True');?></span>
        </label>
    </div>
</div>
<div class="clearfix">
    <?php  echo $form->label('require_answer', t('Is there correct answer to the question?:'));?>
    <div class="input">
        <label style="display: inline;">
            <?php  echo $form->radio('require_answer',0,isset($obj["require_answer"])?$obj["require_answer"]:0,
            array("onchange" => "javascript:$('.qd_hideanswers').css('visibility','hidden');",
                "style" => "display:inline;"));?>
            <span><?php  echo t('False');?></span>
        </label>
        <label style="display: inline; margin-left: 15px;">
            <?php  echo $form->radio('require_answer',1,isset($obj["require_answer"])?$obj["require_answer"]:0,
            array("onchange" => "javascript:$('.qd_hideanswers').css('visibility','visible');",
                "style" => "display:inline"));?>
            <span><?php  echo t('True');?></span>
        </label>
    </div>
</div>
<?php 
echo '
<label>'.t('Answers:').' </label>
			<table class="table" id="qfld_tbl" cellspacing="0" cellpadding="0">
					<tr>
                        <th width="20px" align="center">#</th>
                        <th width="20px" align="center"><div class="qd_hideanswers" '.$styvis.'>'.t('Correct').'</div></th>
                        <th class="title" width="200px">'.t('Answers').'</th>
                        <th width="20px" align="center" class="title">'.t('Delete').'</th>
                        <th width="20px" align="center" class="title"></th>
                        <th width="20px" align="center" class="title"></th>
                    </tr>';


    if(isset($choices) && count($choices)){
        $k = 0; $ii = 1; $ind_last = count($choices);
        foreach ($choices as $frow) { ?>
            <tr>
                <td align="center" ><?php  echo $ii?></td>
                <td align="center">
                    <div class="qd_hideanswers" <?php  echo $styvis;?>>
                        <?php  if($qtype == 1){?>
                        <input <?php  echo ($frow["c_right"]?'checked':'')?> type="radio" name="jq_checked[]" value="<?php  echo $ii?>" />
                        <?php  }elseif($qtype == 2){?>
                        <input <?php  echo ($frow["c_right"]?'checked':'')?> type="checkbox" name="jq_checked[]" value="<?php  echo $ii?>" onClick="jq_UnselectCheckbox2(event);" />
                        <?php  } ?>
                    </div>
                </td>
                <td align="left">
                    <input type="text" name="jq_hid_fields[]" value="<?php  echo str_replace('"',"&quot;",stripslashes($frow["c_choice"]))?>" maxlength="300" />
                    <input type="hidden" name="jq_hid_fields_ids[]" value="<?php  echo $frow["c_id"]?>" />
                </td>
                <td><a href="javascript: void(0);" onClick="javascript:Delete_tbl_row(this); return false;" title="<?php  echo t('Delete');?>"><img src="<?php  echo QUIZPATH_DIR;?>images/delete.gif" alt="<?php  echo t('Delete');?>"></a></td>
                <td><?php  if ($ii > 1) { ?><a href="javascript: void(0);" onClick="javascript:Up_tbl_row(this); return false;" title="<?php  echo t('Move Up');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/up.gif" alt="<?php  echo t('Move Up');?>"></a><?php  } ?></td>
                <td><?php  if ($ii < $ind_last) { ?><a href="javascript: void(0);" onClick="javascript:Down_tbl_row(this); return false;" title="<?php  echo t('Move Down');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/down.gif" alt="<?php  echo t('Move Down');?>"></a><?php  } ?></td>

            </tr>
        <?php 
        $k = 1 - $k; $ii ++;
        }
    } ?>
			</table>

			<br>
			<label></label>
			<table class="table">

				<tr>
				<td width="76px" align="left"  ><?php  echo t('Answer');?>:</td>

				<td width="220px"  align="left" >

					<input id="new_field" class="text_area" style="" type="text" name="new_field" maxlength="300" />


				</td>

				<td  width="85px" align="left" >
					<input class="button" type="button" name="add_new_field" value="<?php  echo t('Add Answer');?>" onClick="javascript:Add_new_tbl_field('new_field', 'qfld_tbl', 'jq_hid_fields[]');" />
				</td>
				</tr>
			</table>

		<?php 

}

function QTrueForm($obj){
    $form = loader::helper('form');
?>
    <div class="clearfix">
        <?php  echo $form->label('qchoice', t('Correct answer:'));?>
        <div class="input">
            <label style="display: inline;">
                <?php  echo $form->radio('qchoice',0,isset($obj['qchoice'])?$obj['qchoice']:0, array("style" => "display:inline;"));?>
                <span><?php  echo t('False');?></span>
            </label>
            <label style="display: inline; margin-left: 15px;">
                <?php  echo $form->radio('qchoice',1,isset($obj['qchoice'])?$obj['qchoice']:0, array("style" => "display:inline;"));?>
                <span><?php  echo t('True');?></span>
            </label>
        </div>
    </div>
<?php 
}

function JSFunc($q_om_type){
?>
	<script language="javascript" type="text/javascript">
		var quest_type = <?php  echo $q_om_type; ?>;

		function getObj(name) {
		  if (document.getElementById)  {  return document.getElementById(name);  }
		  else if (document.all)  {  return document.all[name];  }
		  else if (document.layers)  {  return document.layers[name];  }
		}





		function ReAnalize_tbl_Rows( start_index, tbl_id ) {
			start_index = 1;
			var tbl_elem = getObj(tbl_id);
			if (tbl_elem.rows[start_index]) {
				var count = start_index; var row_k = 1 - start_index%2;//0;
				for (var i=start_index; i<tbl_elem.rows.length; i++) {
					tbl_elem.rows[i].cells[0].innerHTML = count;

					//Redeclare_element_inputs(tbl_elem.rows[i].cells[2]);
					Redeclare_element_inputs2(tbl_elem.rows[i].cells[1], i);
					if (i > 1) {
						tbl_elem.rows[i].cells[4].innerHTML = '<a href="javascript: void(0);" onClick="javascript:Up_tbl_row(this); return false;" title="<?php  echo t('Move Up');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/up.gif" alt="<?php  echo t('Move Up');?>"></a>';
					} else { tbl_elem.rows[i].cells[4].innerHTML = ''; }
					if (i < (tbl_elem.rows.length - 1)) {
						tbl_elem.rows[i].cells[5].innerHTML = '<a href="javascript: void(0);" onClick="javascript:Down_tbl_row(this); return false;" title="<?php  echo t('Move Down');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/down.gif" alt="<?php  echo t('Move Down');?>"></a>';;
					} else { tbl_elem.rows[i].cells[5].innerHTML = ''; }
					tbl_elem.rows[i].className = 'row'+row_k;
					count++;
					row_k = 1 - row_k;
				}
			}
		}

		function Redeclare_element_inputs(object) {
			if (object.hasChildNodes()) {
				var children = object.childNodes;
				var i = 0;
				while (i < children.length) {
					if (children[i].nodeName.toLowerCase() == 'input') {
						var inp_name = children[i].name;
						var inp_value = children[i].value;
						var inp_type = children[i].type;
						if (inp_type.toLowerCase() == 'text') {
							var inp_size = children[i].size;
						}
						if (inp_type.toLowerCase() == 'checkbox' || inp_type.toLowerCase() == 'radio') {
							var inp_check = children[i].checked;
						}
						object.removeChild(object.childNodes[i]);/*i --;*/
						var input_hidden = document.createElement("input");
						input_hidden.type = inp_type;
						if (inp_type.toLowerCase() == 'text') {
							input_hidden.size = inp_size;
						}
						if (inp_type.toLowerCase() == 'checkbox' || inp_type.toLowerCase() == 'radio') {
							input_hidden.checked = inp_check;
							input_hidden.onchange=input_hidden.onclick = new Function('jq_UnselectCheckbox(this)');
						}

						input_hidden.setAttribute('name',inp_name);
						input_hidden.value = inp_value;
						object.appendChild(input_hidden);
					}
					i ++;
				}
			}
		}


		function Redeclare_element_inputs4(object,object2) {
			if (object.hasChildNodes()) {
				var children = object.childNodes;
				for (var i = 0; i < children.length; i++) {
					if (children[i].nodeName.toLowerCase() == 'textarea') {
						var inp_name = object.innerHTML;

						object.removeChild(object.childNodes[i]);

						object2.innerHTML = inp_name;
					}
				}
			}
		}

		function Redeclare_element_inputs3(object,object2) {
			if (object.hasChildNodes()) {
				var children = object.childNodes;
				for (var i = 0; i < children.length; i++) {
					if (children[i].nodeName.toLowerCase() == 'input') {
						var inp_name = children[i].name;
						var inp_value = children[i].value;
						var inp_type = children[i].type;
						if (inp_type.toLowerCase() == 'checkbox' || inp_type.toLowerCase() == 'radio') {
							var inp_check = children[i].checked;
						}
						var input_hidden = document.createElement("input");
						input_hidden.type = inp_type;
						input_hidden.setAttribute('name',inp_name);
						input_hidden.value = inp_value;
						if (inp_type.toLowerCase() == 'checkbox' || inp_type.toLowerCase() == 'radio') {
							input_hidden.checked = inp_check;
							input_hidden.onchange=input_hidden.onclick = new Function('jq_UnselectCheckbox(this)');
						}
						object2.appendChild(input_hidden);
					}
				}
			}
		}

		function Redeclare_element_inputs2(object, gg) {
			if (object.hasChildNodes()) {
				var children = object.childNodes;
				for (var i = 0; i < children.length; i++) {
					if (children[i].nodeName.toLowerCase() == 'input') {

						var inp_type = children[i].type;
						if (inp_type.toLowerCase() == 'checkbox' || inp_type.toLowerCase() == 'radio') {
							object.childNodes[i].value = gg;

						}
					}
				}
			}
		}

		function Delete_tbl_row(element) {
			var del_index = element.parentNode.parentNode.sectionRowIndex;
			var tbl_id = element.parentNode.parentNode.parentNode.parentNode.id;
			element.parentNode.parentNode.parentNode.deleteRow(del_index);
			ReAnalize_tbl_Rows(del_index - 1, tbl_id);
		}

		function Up_tbl_row(element) {
			if (element.parentNode.parentNode.sectionRowIndex > 1) {
				var sec_indx = element.parentNode.parentNode.sectionRowIndex;
				var table = element.parentNode.parentNode.parentNode;
				var tbl_id = table.parentNode.id;

				var cell1 = document.createElement("td");
				cell1.align = 'center';
				var row = table.insertRow(sec_indx - 1);
				row.appendChild(cell1);
				row.appendChild(element.parentNode.parentNode.cells[1]);
				row.appendChild(element.parentNode.parentNode.cells[1]);
				row.appendChild(element.parentNode.parentNode.cells[1]);



				var cell5 = document.createElement("td");
				cell5.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Up_tbl_row(this); return false;" title="<?php  echo t('Move Up');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/up.gif" alt="<?php  echo t('Move Up');?>"></a>';
				row.appendChild(cell5);
				var cell6 = document.createElement("td");

				cell6.innerHTML = '&nbsp;';

				row.appendChild(cell6);

				element.parentNode.parentNode.parentNode.deleteRow(element.parentNode.parentNode.sectionRowIndex);

				ReAnalize_tbl_Rows(sec_indx - 2, tbl_id);
			}
		}

		function Down_tbl_row(element) {
			if (element.parentNode.parentNode.sectionRowIndex < element.parentNode.parentNode.parentNode.rows.length - 1) {
				var sec_indx = element.parentNode.parentNode.sectionRowIndex;
				var table = element.parentNode.parentNode.parentNode;
				var tbl_id = table.parentNode.id;

				var cell1 = document.createElement("td");
				cell1.align = 'center';
				var row = table.insertRow(sec_indx + 2);
				row.appendChild(cell1);
				row.appendChild(element.parentNode.parentNode.cells[1]);
				row.appendChild(element.parentNode.parentNode.cells[1]);
				row.appendChild(element.parentNode.parentNode.cells[1]);


				var cell5 = document.createElement("td");
				cell5.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Up_tbl_row(this); return false;" title="<?php  echo t('Move Up');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/up.gif" alt="<?php  echo t('Move Up');?>"></a>';

				row.appendChild(cell5);
				var cell6 = document.createElement("td");

				cell6.innerHTML = '&nbsp;';

				row.appendChild(cell6);

				element.parentNode.parentNode.parentNode.deleteRow(element.parentNode.parentNode.sectionRowIndex);


				ReAnalize_tbl_Rows(sec_indx, tbl_id);
			}
		}

		function Add_new_tbl_field(elem_field, tbl_id, field_name) {
			var new_element_txt = getObj(elem_field).value;

			getObj(elem_field).value = '';
			if (new_element_txt == '') {
				alert("Please enter text to the field.");return;
			}
			var tbl_elem = getObj(tbl_id);
			var row = tbl_elem.insertRow(tbl_elem.rows.length);
			var cell1 = document.createElement("td");
			var cell2 = document.createElement("td");
			var cell3 = document.createElement("td");
			var cell4 = document.createElement("td");
			var cell5 = document.createElement("td");
			var cell6 = document.createElement("td");

			var input_hidden = document.createElement("input");
			input_hidden.type = "text";
			input_hidden.name = field_name;
			input_hidden.value = new_element_txt;
			input_hidden.setAttribute('maxlength', 300);
			var input_hidden_id = document.createElement("input");
			input_hidden_id.type = "hidden";
			input_hidden_id.setAttribute('name','jq_hid_fields_ids[]');
			input_hidden_id.value = "0";
			var input_check = createNamedElement("input","jq_checked[]")
			if(quest_type == 1){
				input_check.type = "radio";
			}else{
				input_check.type = "checkbox";
			}

			input_check.setAttribute('name','jq_checked[]');
			input_check.checked = false;
			input_check.setAttribute('class','qd_hideanswers');
			if(quest_type == 1 || quest_type == 2){
				if(getObj("require_answer3").checked){
					input_check.style.visibility = 'hidden';
				}
			}
			input_check.onchange=input_check.onclick = new Function('jq_UnselectCheckbox(this)');
			cell1.align = 'center';
			cell1.innerHTML = 0;
			cell2.align = 'center';
			cell2.appendChild(input_check);
			cell3.appendChild(input_hidden);
			cell3.appendChild(input_hidden_id);
			cell4.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Delete_tbl_row(this); return false;" title="<?php  echo t('Delete');?>"><img src="<?php  echo QUIZPATH_DIR;?>images/delete.gif" alt="<?php  echo t('Delete');?>"></a>';
			cell5.innerHTML = '<a href="javascript: void(0);" onClick="javascript:Up_tbl_row(this); return false;" title="<?php  echo t('Move Up');?>"><img border="0" src="<?php  echo QUIZPATH_DIR;?>images/up.gif" alt="<?php  echo t('Move Up');?>"></a>';
			cell6.innerHTML = '';

			row.appendChild(cell1);
			row.appendChild(cell2);
			row.appendChild(cell3);
			row.appendChild(cell4);
			row.appendChild(cell5);
			row.appendChild(cell6);
			ReAnalize_tbl_Rows(tbl_elem.rows.length - 2, tbl_id);
		}
		function createNamedElement(type, name) {
		   var element = null;
		   try {
		      element = document.createElement('<'+type+' name="'+name+'">');
		   } catch (e) {
		   }
		   if (!element || element.nodeName != type.toUpperCase()) {
		      element = document.createElement(type);
		      element.name = name;
		   }
		   return element;
		}

		function jq_UnselectCheckbox(che) {
			<?php  if ($q_om_type == 1) { ?>
			f_name = che.form.name;
			ch_name = che.name;
			var a = che.checked;
			start_index = 1;
			var tbl_elem = getObj('qfld_tbl');
			if (tbl_elem.rows[start_index]) {
				var count = start_index;
				for (var j=start_index; j<tbl_elem.rows.length; j++) {
					if (tbl_elem.rows[j].cells[1].hasChildNodes()) {
						for (var i = 0; i < tbl_elem.rows[j].cells[1].childNodes.length; i++) {
							if (tbl_elem.rows[j].cells[1].childNodes[i].nodeName.toLowerCase() == 'input') {
								if (tbl_elem.rows[j].cells[1].childNodes[i].type.toLowerCase() == 'checkbox') {
									tbl_elem.rows[j].cells[1].childNodes[i].checked = false;
									tbl_elem.rows[j].cells[1].childNodes[i].setAttribute('checked', false);
								}
							}
						}
					}
					count++;
				}
			}
			if (a)
			che.checked = true;
		<?php  } else { ?>
			return;
		<?php  } ?>
		}
		function jq_UnselectCheckbox2(e) {
			if (!e) { e = window.event;}
			var cat2=e.target?e.target:e.srcElement;
			jq_UnselectCheckbox(cat2);

		}

	</script>
	<?php 
}