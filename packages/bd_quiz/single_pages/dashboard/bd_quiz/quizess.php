<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
$form = loader::helper('form');
if ($this->controller->getTask() == 'add' || $this->controller->getTask() == 'saveq') {
	loader::packageElement("quiz_add","bd_quiz",$args);
}else{

echo  Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Quiz'), t('Quiz.'), false, false); ?>
<script type="text/javascript">
    ccm_setupQuiz =  function (){
        $("#ccm-quiz-list-cb-all").click(function() {
            if ($(this).prop('checked') == true) {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', true);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $('td.ccm-quiz-list-cb input[type=checkbox]').attr('checked', false);
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            if ($("td.ccm-user-list-cb input[type=checkbox]:checked").length > 0) {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
            } else {
                $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
            }
        });

        $("#ccm-quiz-list-multiple-operations").change(function() {
            var action = $(this).val();
            var fIDstring = ccm_alGetSelectedQuizIDs();
            switch(action) {
                case "delete":
                    if(confirm("<?php  echo t('Are you sure?');?>"))    {
                        window.location.href = '<?php  echo $this->url("/dashboard/bd_quiz/quizess/delete?")?>' +
                                fIDstring;
                    }

                    break;
            }

            $(this).get(0).selectedIndex = 0;
        });
        $("td.ccm-quiz-list-cb input[type=checkbox]").click(function(e) {
            ccm_alRescanMultiQuizMenu();
        });
    }
    ccm_alRescanMultiQuizMenu = function() {
        if ($("td.ccm-quiz-list-cb input[type=checkbox]:checked").length > 0) {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', false);
        } else {
            $("#ccm-quiz-list-multiple-operations").attr('disabled', true);
        }
    }
    ccm_alGetSelectedQuizIDs = function() {
        var fidstr = '';
        $("td.ccm-quiz-list-cb input[type=checkbox]:checked").each(function() {
            fidstr += 'qID[]=' + $(this).val() + '&';
        });
        return fidstr;
    }
    $(function() {
        ccm_setupQuiz();
    });
</script>
<div class="ccm-pane-body">
<div id="ccm-list-wrapper">
    <div style="margin-bottom: 10px">
        <?php   $form = Loader::helper('form'); ?>
        <a href="<?php  echo View::url('/dashboard/bd_quiz/quizess/add')?>" style="float: right" class="btn primary"><?php  echo t
        ("Add Quiz")?></a>
        <select id="ccm-quiz-list-multiple-operations" class="span3" disabled>
            <option value="">** <?php  echo t('With Selected')?></option>
            <option value="delete"><?php  echo t('Delete')?></option>
        </select>
    </div>
</div>
<div>

		<?php   
			$nh = Loader::helper('navigation');

			if ($quizesList->getTotal() > 0) {

				?>

				<table border="0" class="ccm-results-list" cellspacing="0" cellpadding="0">
					<tr>
                        <th width="1"><input id="ccm-quiz-list-cb-all" type="checkbox" /></th>
                        <th class="<?php   echo $quizesList->getSearchResultsClass('qzName')?>"><a href="<?php   echo $quizesList->getSortByURL('qzName', 'asc')?>"><?php   echo t('Name')?></a></th>
						<th class="<?php   echo $quizesList->getSearchResultsClass('qz_created_time')?>"><a href="<?php   echo $quizesList->getSortByURL('qz_created_time', 'asc')?>"><?php   echo t('Date Added')?></a></th>
                        <th ><?php   echo t('Success')?></th>

                    </tr>
					<?php   
					foreach($quizes as $cobj) { ?>
                        <tr>
                            <td class="ccm-quiz-list-cb" style="vertical-align: middle !important"><input type="checkbox" value="<?php  echo $cobj["qzid"];?>" /></td>
                            <td><a href="<?php  echo View::url('/dashboard/bd_quiz/quizess/add?eId='.$cobj["qzid"])
                                ?>"><?php  echo strip_tags($cobj["qzName"]);?></a></td>
                            <td><?php  echo $cobj["qz_created_time"];?></td>
                            <td><?php  echo intval($cobj["passed"]);?>/<?php  echo $cobj["cnt"];?></td>
                        </tr>
					<?php 
					}
					?>

				</table>
				<br/>
				<?php 
                $quizesList->displaySummary();
				$quizesList->displayPaging();
			} else {
				print t('No Quiz entries found.');
			}
	?>
	</div>
</div>
	<div class='ccm-pane-footer'></div>
<?php   echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);
}?>