<?php if($controller->language == 'en') {?>
		<h2>Outlets</h2>
		    <ul class="nav nav-pills">
			  <li class="<?=$clsFood?>"><a href="?category=506addfe9b6286e82a000015">Restaurants & Cafés</a></li>
			  <li class="<?=$clsFashion?>"><a href="?category=506ade239b6286e72a00001c">Fashion</a></li>
			  <li class="<?=$clsOther?>"><a href="?category=506ade359b62869b70000017">Specials</a></li>
			</ul>
	<?php }else{ ?>
		<h2>منافذ</h2>
	    <ul class="nav nav-pills">
		  <li class="<?=$clsFood?>"><a href="?category=506addfe9b6286e82a000015">مطاعم و كافيهات</a></li>
		  <li class="<?=$clsFashion?>"><a href="?category=506ade239b6286e72a00001c">الموضه</a></li>
		  <li class="<?=$clsOther?>"><a href="?category=506ade359b62869b70000017">أخرى</a></li>
		</ul>
	<?php } ?>
<div class="sep"></div>

<div class="hpBlocksContainer clearfix">

	<?php foreach($chains as $chain): ?>
	
	<?php
        	
        		$path = $this->getThemePath().'/dummy/truLogohpa.jpg';
        		$path2 = $this->getThemePath().'/dummy/truhpa.jpg';
        		$name = '';
        		
        		if(isset($chain->chainDisplayName))
        			$name = $chain->chainDisplayName;
        			
        		//echo "<pre>";
        		//var_dump($chain);exit();
        		//var_dump($clsFood);exit();
            	if(isset($chain->images))
            	{
            		foreach($chain->images as $img)
            		{
            			if(isset($img))
            			{
            				if(isset($img->path))
            				{
            					if(isset($img->label))
            					{
            						switch($img->label)
            						{
            							case "logocircle":
                						case "logo":
								        	$path = $img->path;
											//$path = str_replace('vol', 'var', $path);
								        	$path = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path.'&width=72&height=72&gravity=center&oauth_token='.$auth_token;
								        break;
								    
										case "ad":
								        case "iphonecoverphoto":
								        	$path2 = $img->path;
											//$path2 = str_replace('vol', 'var', $path2);					                					
								        	$path2 = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path2.'&gravity=center&width=180&height=120&oauth_token='.$auth_token;
								        break;
								    
            						}
            					}
            						
            				}
            			}
            			
            			
            					

            		}
            	}
            
        	?>

<a href="/<?php echo $controller->language;?>/outlets-data?chainId=<?=$chain->_id;?>&collection=<?=$chain->collections[0];?>" target="_blank">
    <div class="hpBlocks">
    	<img src="<?=$path2?>" alt="<?=$name?>" width="180" height="120" />
        <div style="height:20px;"></div>
        <div class="hpBlockSmallLog">
            <img src="<?=$path;?>" alt="<?=$name?>" width="72" height="72" />
        </div>
        <div class="hpBlockText">
        	<?php
        		$cls = 'refund';
        		if($chain->privilege == 'Instant Discount')
        			$cls = 'instantDiscount';
    			if($chain->upTo == "true"){
        	?>
        	<h7 style="color: #999999; font-weight: bold;">
	        	<?php if($controller->language == 'en')
		        		echo 'upto';
	        		elseif($controller->language == 'ar')
	        			echo 'يصل الى';
	        	?>
        	</h7>
        	<?php } ?>
        	<h4 class="<?=$cls?>"><?=$chain->privilegeValue;?></h4>
        	<h6><?=$chain->privilege;?></h6>
        	<?php //echo $chain->name;?>
        </div>
    </div>
 </a>   
    <?php endforeach;?>
    
    
</div>

<div class="pagination clearfix">
	  <ul>
	  	<li><a href="?category=<?=$category?>&offset=<?=$prevPage;?>">Prev</a></li>
	  	<?php for($i=1;$i<=$totalPages;$i++): ?>
	  		<?php
	  			$cls = '';
	  			if($currentPage == $i) 
		  			$cls = 'class="active"';
	  		?>
		    <li <?=$cls?>><a href="?category=<?=$category?>&offset=<?=($i-1)*$limit?>"><?=$i?></a></li>
	    <?php endFor;?>
	    <li><a href="?category=<?=$category?>&offset=<?=$nextPage?>">Next</a></li>
	  </ul>
	</div>
