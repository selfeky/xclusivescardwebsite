<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

	require_once('libraries/pest/PestJSON.php');

	class ApiOutletsBlockController extends BlockController {
		
		var $pobj;
		
		protected $btDescription = "A simple testing block for developers.";
		protected $btName = "Api Outlets";
		protected $btTable = 'btApiOutlets';
		protected $btInterfaceWidth = "350";
		protected $btInterfaceHeight = "300";
		
		//protected $auth_token = "bb81e7bfda7df5899af4c5316bac9942";
		protected $auth_token = "8ff0096821120a0f2975dc3f0b8ff9ff";
		protected $limit = 9;
		protected $offset = 0;
		protected $count = 0;
		protected $currentPage = 1;
		
		protected $prevPage = 0;
		protected $nextPage = 0;
		public $language;
		
		//protected $foodCategoryId = '5017d2db9b62860159000187';
		//protected $fashionCategoryId = '5017d2eb9b62866b50000169';
		//protected $otherCategoryId = '50290cd09b6286bb41000023';
		
		protected $foodCategoryId = '506addfe9b6286e82a000015';
		protected $fashionCategoryId = '506ade239b6286e72a00001c';
		protected $otherCategoryId = '506ade359b62869b70000017';
		
		protected $category = '506addfe9b6286e82a000015';
		
		private function getLanguage(){
			//this will get language of the current page
			$lh = Loader::helper('section', 'multilingual');
			
			if($lh->getLanguage() == "ar_EG")
		    {
		       $this->language = 'ar';
		    }
		    else{
			    $this->language = 'en';
		    }
		}
		public function view() {
			$this->getLanguage();
			
			if(isset($_GET['offset']))
				$this->offset = $_GET['offset'];
				
			if(isset($_GET['category']))
				$this->category = $_GET['category'];
			
			$pest = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
			
			//get total count for pagination
			$resp = $pest->get('/chains/count?categories='.$this->category.'&oauth_token='.$this->auth_token);
			$resp = json_decode($resp);
			$this->count = $resp->recordsCount;
			$this->set('count', $this->count);
			
			//get chains
			$resp = $pest->get('/chains?categories='.$this->category.'&oauth_token='.$this->auth_token.'&limit='.$this->limit.'&offset='.$this->offset.'&sort=weight&dir=desc&language='.$this->language);
			//$chains = array();
			$chains = json_decode($resp);
			
			//var_dump('/chains?categories='.$this->category.'&oauth_token='.$this->auth_token.'&limit='.$this->limit.'&offset='.$this->offset);exit();
			$this->set('chains', $chains);
			
			$this->calcPages();
			
			$this->set('limit', $this->limit);
			
			$this->set('auth_token',$this->auth_token);
			
			//set category and active cls
			$this->set('category', $this->category);
			switch($this->category)
			{
				case $this->foodCategoryId:
					$this->set('clsFood', 'active');
				break;
				
				case $this->fashionCategoryId:
					$this->set('clsFashion', 'active');
				break;
				
				case $this->otherCategoryId:
					$this->set('clsOther', 'active');
				break;

			}
			
			
		}
		
		public function calcPages() {
		
			$this->currentPage = ($this->offset / $this->limit) + 1;
			$this->set('currentPage', $this->currentPage);
			
			$this->totalPages = ceil($this->count / $this->limit);
			$this->set('totalPages', $this->totalPages);
			
			
			$this->prevPage = ($this->currentPage - 2) * $this->limit;
			
			if($this->prevPage < 0)
				$this->prevPage = 0;
				
			$this->set('prevPage', $this->prevPage);
			
			
			
			$this->nextPage = $this->currentPage * $this->limit;
			
			if($this->currentPage == $this->totalPages)
				$this->nextPage = $this->nextPage - $this->limit;
			
			$this->set('nextPage', $this->nextPage);
			
		}
		
		
	}
	
?>