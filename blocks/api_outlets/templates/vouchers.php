<!-- alternating-table-color-rows -->
<style type="text/css">
.myTable { width:100%; border-collapse:collapse;  }
.myTable td { padding:8px; border:#999 1px solid; }
 
.alternateRowTable tr:nth-child(even) { /*(even) or (2n 0)*/
	background: #EBECEE;
}
.alternateRowTable tr:nth-child(odd) { /*(odd) or (2n 1)*/
	background: #ffffff;
}
</style>

<div class="tabbable">
<ul class="nav nav-pills">
  <li class="active"><a href="#pane1" data-toggle="tab">Food</a></li>
  <li><a href="#pane2" data-toggle="tab">Fashion</a></li>
  <li><a href="#pane3" data-toggle="tab">Specials</a></li>
</ul>
   
<div class="tab-content">
<!--Food Table-->

<div class='tab-pane active' id='pane1' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:12px;'>
	<!-- <h3>Food</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="foodTableContent" width="100%" name="foodTableContent">
		<tr>
			<th>Logo</th>
			<th>Outlet</th>
			<th>Instant Discount</th>
			<th>Description</th>
		</tr>
		<?php
			$foodCounter = 2;
			foreach ($foodVouchers as $key => $voucher) {
        	
        		$path = $this->getThemePath().'/dummy/truLogohp.jpg';
        		$path2 = $this->getThemePath().'/dummy/truhp.jpg';
        		
            	if(isset($voucher->images))
            	{
            		foreach($voucher->images as $img)
            		{
            			// var_dump($img);exit();
            			if(isset($img))
            			{
            				if(isset($img->path))
            				{
            					if(isset($img->label))
            					{
            						switch($img->label)
            						{
                						case "logocircle":
								        	$path = $img->path;
								        	$path = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path.'&width=72&height=72&gravity=center&oauth_token='.$auth_token;
								        break;
								    
								        case "voucher":
								        	$path2 = $img->path;					                					
								        	$path2 = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path2.'&gravity=center&width=180&height=120&oauth_token='.$auth_token;
								        break;
								    
            						}
            					}
            						
            				}
            			}
            			
            			
            					

            		}
            	}
            

				echo "<tr>";
				echo "<td style='text-align:center;'>";
				echo "<div class='hpBlockSmallLog'>";
				echo "<img src='<?=$path;?>' alt='truLogohp' width='72' height='72' />";
	    		echo "</div>";
				echo "</td>";
				echo "<td style='text-align:center;'>";
				echo $voucher->name;
				echo "</td>";
				echo "<td style='text-align:center;width:100px;'>";
				echo "<input type='number' disabled='disabled' value=$value->calculatorBasePrice style='width:60px;' name='singleValue' class='singleValue' onkeyup='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' onchange='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' />";
				echo "</td>";	
				echo "<td style='text-align:center;'>";
				echo "<input type='number' disabled='disabled' value=$value->visitsNumber style='width:60px;' name='noOfVisitsValue' class='singleValue' onkeyup='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' onchange='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")'/>";
				echo "</td>";	
				echo "<td style='text-align:center;'>";
				echo $voucher->privilege;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->privilegeValue;
				echo "</td>";
				echo "<td style='width:100px;'>";
				echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='refund'/>";
				echo "</td>";
				echo "<td style='width:100px;'>";
				echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='discount'/>";
				echo "</td>";
				echo "</tr>";   
				
				$foodCounter ++;
			}
	   	?>
	</table>
</div>

<!--Fashion Table-->
<div class='tab-pane' id='pane2' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>
	<!-- <h3>Fashion</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="fashionTableContent" width="100%" name="fashionTableContent">
		<tr>
			<th>Logo</th>
			<th>Outlet</th>
			<th>Instant Discount</th>
			<th>Description</th>
		</tr>
		<?php
			$fashionCounter = 2;
			foreach ($fashionVouchers as $key => $value) {
				// remove the records with privilege value isn't unique
				if(strlen($value->privilegeValue) <= 4)
				{
					echo "<tr>";
					echo "<td style='text-align:center;'>";
					echo "<div class='hpBlockSmallLog'>";
					echo "<img src='<?=$path;?>' alt='truLogohp' width='72' height='72' />";
            		echo "</div>";
					echo "</td>";
					echo "<td style='text-align:center;'>";
					echo $value->name;
					echo "</td>";
					echo "<td style='text-align:center;width:100px;'>";
					echo "<input type='number' disabled='disabled' value=$value->calculatorBasePrice style='width:60px;' name='singleValue' class='singleValue' onkeyup='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")' onchange='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo "<input type='number' disabled='disabled' value=$value->visitsNumber  style='width:60px;' name='noOfVisitsValue' class='singleValue' onkeyup='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")' onchange='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo $value->privilege;
					echo "</td>";
					echo "<td style='text-align:center;' class='privilegeValue'>";
					echo $value->privilegeValue;
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='refund'/>";
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='discount'/>";
					echo "</td>";
					echo "</tr>";   
					
					$fashionCounter ++;
				}
			}
	   	?>
	</table>
</div>

<!--Special Table-->     
<div class='tab-pane' id='pane3' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>
	<!-- <h3>Specials</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="specialTableContent" width="100%" name="specialTableContent">
		<tr>
			<th>Logo</th>
			<th>Outlet</th>
			<th>Instant Discount</th>
			<th>Description</th>
		</tr>
		<?php
			$specialCounter = 2;
			foreach ($specialsVouchers as $key => $value) {
				// remove the records with privilege value isn't unique
				if(strlen($value->privilegeValue) <= 4)
				{
					echo "<tr>";
					echo "<td style='text-align:center;'>";
					echo "<div class='hpBlockSmallLog'>";
					echo "<img src='<?=$path;?>' alt='truLogohp' width='72' height='72' />";
            		echo "</div>";
					echo "</td>";
					echo "<td style='text-align:center;'>";
					echo $value->name;
					echo "</td>";
					echo "<td style='text-align:center;width:100px;'>";
					echo "<input type='number' disabled='disabled' value=$value->calculatorBasePrice style='width:60px;' name='singleValue' class='singleValue' onkeyup='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")' onchange='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo "<input type='number' disabled='disabled' value=$value->visitsNumber style='width:60px;' name='noOfVisitsValue' class='singleValue' onkeyup='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")' onchange='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo $value->privilege;
					echo "</td>";
					echo "<td style='text-align:center;' class='privilegeValue'>";
					echo $value->privilegeValue;
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='refund'/>";
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='discount'/>";
					echo "</td>";
					echo "</tr>";   
					
					$specialCounter ++;
				}
			}
	   	?>
	</table>    
</div>   <!--Special Table-->     
</div><!-- /.tab-content -->
</div><!-- /.tabbable -->

<div class="pagination clearfix">
	  <ul>
	  	<li><a href="?offset=<?=$prevPage;?>">Prev</a></li>
	  	<?php for($i=1;$i<=$totalPages;$i++): ?>
	  		<?php
	  			$cls = '';
	  			if($currentPage == $i) 
		  			$cls = 'class="active"';
	  		?>
		    <li <?=$cls?>><a href="?offset=<?=($i-1)*$limit?>"><?=$i?></a></li>
	    <?php endFor;?>
	    <li><a href="?offset=<?=$nextPage?>">Next</a></li>
	  </ul>
</div>
