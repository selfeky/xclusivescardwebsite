<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

	require_once('libraries/pest/PestJSON.php');

	class ApiValuecalculatorBlockController extends BlockController {
		
		var $pobj;
		
		protected $btDescription = "A simple block";
		protected $btName = "Api Value Calculator ";
		protected $btTable = 'btApiOutlets';
		protected $btInterfaceWidth = "350";
		protected $btInterfaceHeight = "300";
		
		//protected $auth_token = "bb81e7bfda7df5899af4c5316bac9942";
		protected $auth_token = "8ff0096821120a0f2975dc3f0b8ff9ff";
		protected $limit = 9;
		protected $offset = 0;
		protected $count = 0;
		protected $currentPage = 1;
		
		protected $prevPage = 0;
		protected $nextPage = 0;
		
		//protected $foodCategoryId = '5017d2db9b62860159000187';
		//protected $fashionCategoryId = '5017d2eb9b62866b50000169';
		//protected $otherCategoryId = '50290cd09b6286bb41000023';
		
		protected $foodCategoryId = '506addfe9b6286e82a000015';
		protected $fashionCategoryId = '506ade239b6286e72a00001c';
		protected $specialsCategoryId = '506ade359b62869b70000017';
		public $language;
		
		private function getLanguage(){
			//this will get language of the current page
			$lh = Loader::helper('section', 'multilingual');
			
			if($lh->getLanguage() == "ar_EG")
		    {
		       $this->language = 'ar';
		    }
		    else{
			    $this->language = 'en';
		    }
		}
		public function view() {
			$this->getLanguage();
			$pest = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
			
			//get food Data
			//$resp = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&categories='.$this->foodCategoryId.'&definition=name,privilegeDescription,privilegeValue');
			$resp = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&categories='.$this->foodCategoryId.'&sort=weight&dir=desc&language='.$this->language);
			$resp = json_decode($resp);
			$this->set('foodChains', $resp);
			
			//get food Data
			//$resp = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&categories='.$this->fashionCategoryId.'&definition=name,privilegeDescription,privilegeValue');
			$resp = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&categories='.$this->fashionCategoryId.'&sort=weight&dir=desc&language='.$this->language);
			$resp = json_decode($resp);
			$this->set('fashionChains', $resp);
			
			//get food Data
			//$resp = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&categories='.$this->specialsCategoryId.'&definition=name,privilegeDescription,privilegeValue');
			$resp = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&categories='.$this->specialsCategoryId.'&sort=weight&dir=desc&language='.$this->language);
			$resp = json_decode($resp);
			$this->set('specialsChains', $resp);
		}
		
	}
	
?>
