<style type="text/css">
	.viewAppButton {
		-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
		-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
		box-shadow:inset 0px 1px 0px 0px #ffffff;
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
/*		background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );*/
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
		background-color:#ededed;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
		border-radius:6px;
		border:1px solid #dcdcdc;
		display:inline-block;
		color:#777777;
		font-family:arial;
		font-size:14px;
		font-weight:bold;
		padding:3px 24px;
		text-decoration:none;
		text-shadow:1px 1px 0px #ffffff;
		margin-top:3px;
	}.viewAppButton:hover {
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
		background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
		background-color:#dfdfdf;
	}.viewAppButton:active {
		position:relative;
		top:1px;
	}
</style>

<!-- alternating-table-color-rows -->
<style type="text/css">
.myTable { width:100%; border-collapse:collapse;  }
.myTable td { padding:8px; border:#999 1px solid; }
 
.alternateRowTable tr:nth-child(even) { /*(even) or (2n 0)*/
	background: #EBECEE;
}
.alternateRowTable tr:nth-child(odd) { /*(odd) or (2n 1)*/
	background: #ffffff;
}
</style>

<script>
function calculateTotalSavings(rowNumber,tableName)
{
	if(($('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(1)').find('input[name="tick"]')).is(':checked'))
	{
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(3)').find('input[name="singleValue"]').removeAttr("disabled");
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(4)').find('input[name="noOfVisitsValue"]').removeAttr("disabled");
		
		var customValue = parseFloat($('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(3)').find('input[name="singleValue"]').val());
		var numberOfVisits = parseFloat($('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(4)').find('input[name="noOfVisitsValue"]').val());
		var privilege =  $('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(5)').text();
		var privilegeValue =  parseFloat($('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(6)').text());
		var rowTotal = parseFloat(customValue*numberOfVisits*privilegeValue/100);
/* 		alert(privilege); */
		if((privilege == "Refund" || privilege == "قسائم") && parseFloat(rowTotal))
		{
			$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(7)').find('input[name="refund"]').val(rowTotal);
		}
		else if((privilege == "Instant Discount" || privilege == "خصم فوري") && parseFloat(rowTotal))
		{
			$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(8)').find('input[name="discount"]').val(rowTotal);
		}
	}
	else
	{
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(7)').find('input[name="refund"]').val(0);
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(8)').find('input[name="discount"]').val(0);
		
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(3)').find('input[name="singleValue"]').attr('disabled','disabled');
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber+') td:nth-child(4)').find('input[name="noOfVisitsValue"]').attr('disabled','disabled');
		
		$('table[name="'+tableName+'"] tr:nth-child('+rowNumber).attr('color','red');
	}
	
	calculateTotals(tableName);
}

function calculateTotals(tableName)
{
	var totalSpentFood = 0;
	var totalDiscountFood = 0;
	
	var totalSpentSpecials = 0;
	var totalDiscountSpecials = 0;
	
	var totalSpentFashion = 0;
	var totalDiscountFashion = 0;
	
	var rowCount = (document.getElementById("foodTableContent").rows.length);
	for(i=2;i<=rowCount;i++)
	{
		if(($('table[name="foodTableContent"] tr:nth-child('+i+') td:nth-child(1)').find('input[name="tick"]')).is(':checked'))
		{
			var pricePerRow = parseFloat($('table[name="foodTableContent"] tr:nth-child('+i+') td:nth-child(3)').find('input[name="singleValue"]').val());
			var numberOfVisitsPerRow = parseFloat($('table[name="foodTableContent"] tr:nth-child('+i+') td:nth-child(4)').find('input[name="noOfVisitsValue"]').val());
			var refund = $('table[name="foodTableContent"] tr:nth-child('+i+') td:nth-child(7)').find('input[name="refund"]').val();
			var discount = $('table[name="foodTableContent"] tr:nth-child('+i+') td:nth-child(8)').find('input[name="discount"]').val();
			totalSpentFood += parseFloat(pricePerRow)*parseFloat(numberOfVisitsPerRow);
			totalDiscountFood += parseFloat(refund)+parseFloat(discount);
		}
		
	}
	
	var rowCount = (document.getElementById("fashionTableContent").rows.length);
	for(i=2;i<=rowCount;i++)
	{
		if(($('table[name="fashionTableContent"] tr:nth-child('+i+') td:nth-child(1)').find('input[name="tick"]')).is(':checked'))
		{
			var pricePerRow = parseFloat($('table[name="fashionTableContent"] tr:nth-child('+i+') td:nth-child(3)').find('input[name="singleValue"]').val());
			var numberOfVisitsPerRow = parseFloat($('table[name="fashionTableContent"] tr:nth-child('+i+') td:nth-child(4)').find('input[name="noOfVisitsValue"]').val());
			var refund = $('table[name="fashionTableContent"] tr:nth-child('+i+') td:nth-child(7)').find('input[name="refund"]').val();
			var discount = $('table[name="fashionTableContent"] tr:nth-child('+i+') td:nth-child(8)').find('input[name="discount"]').val();
			totalSpentFashion += parseFloat(pricePerRow)*parseFloat(numberOfVisitsPerRow);
			totalDiscountFashion += parseFloat(refund)+parseFloat(discount);
		}
	}
	
	var rowCount = (document.getElementById("specialTableContent").rows.length);
	for(i=2;i<=rowCount;i++)
	{
		if(($('table[name="specialTableContent"] tr:nth-child('+i+') td:nth-child(1)').find('input[name="tick"]')).is(':checked'))
		{
			var pricePerRow = parseFloat($('table[name="specialTableContent"] tr:nth-child('+i+') td:nth-child(3)').find('input[name="singleValue"]').val());
			var numberOfVisitsPerRow = parseFloat($('table[name="specialTableContent"] tr:nth-child('+i+') td:nth-child(4)').find('input[name="noOfVisitsValue"]').val());
			var refund = $('table[name="specialTableContent"] tr:nth-child('+i+') td:nth-child(7)').find('input[name="refund"]').val();
			var discount = $('table[name="specialTableContent"] tr:nth-child('+i+') td:nth-child(8)').find('input[name="discount"]').val();
			totalSpentSpecials += parseFloat(pricePerRow)*parseFloat(numberOfVisitsPerRow);
			totalDiscountSpecials += parseFloat(refund)+parseFloat(discount);
		}
	}
	
	
	$('table[name="total"] tr[name="totalSpent"] td').find('input[name="totalSpentFood"]').val(totalSpentFood);
	$('table[name="total"] tr[name="totalSavings"] td').find('input[name="totalSavingsFood"]').val(totalDiscountFood);
	
	$('table[name="total"] tr[name="totalSpent"] td').find('input[name="totalSpentFashion"]').val(totalSpentFashion);
	$('table[name="total"] tr[name="totalSavings"] td').find('input[name="totalSavingsFashion"]').val(totalDiscountFashion);
	
	$('table[name="total"] tr[name="totalSpent"] td').find('input[name="totalSpentSpecials"]').val(totalSpentSpecials);
	$('table[name="total"] tr[name="totalSavings"] td').find('input[name="totalSavingsSpecials"]').val(totalDiscountSpecials);
	
	$('table[name="total"] tr[name="totalSpent"] td').find('input[name="totalSpent"]').val(totalSpentFood + totalSpentFashion + totalSpentSpecials);
	$('table[name="total"] tr[name="totalSavings"] td').find('input[name="totalSavings"]').val(totalDiscountFood + totalDiscountFashion + totalDiscountSpecials);
	// $("#totalSpent").val(totalSpent);
	// $("#totalSavings").val(totalDiscount);
	
}

function slideToggle(id)
{
	$('#'+id).slideToggle('slow');
}
</script>

<h2><?php echo $controller->language == 'en' ? "Value Calculator" : "قيمة حاسبة"; ?></h2>
<div style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:12px;'>
	<table border="2" cellpadding="2" cellspacing="2" width="100%" name="total">
  
	<tr name="headers">
   		<td style='text-align:center;' colspan="6"></td>
   		<td width=100px;>
   			<div style='text-align:center' ><?php echo $controller->language == 'en' ? "Restaurants & Cafés" : "مطاعم و كافيهات"; ?></div>
   		</td>
   		<td width=100px;>
   			<div style='text-align:center' ><?php echo $controller->language == 'en' ? "Fashion" : "الموضه"; ?></div>
   		</td>
   		<td width=100px;>
   			<div style='text-align:center' ><?php echo $controller->language == 'en' ? "Specials" : "أخرى"; ?></div>
   		</td>
   		<td width=100px;>
   			<div style='text-align:center' ><?php echo $controller->language == 'en' ? "Totals" : "المجموع"; ?></div>
   		</td>
   	</tr>
	<tr name="totalSpent">
   		<td style='text-align:center;' colspan="6"><?php echo $controller->language == 'en' ? "Total Spent (EGP): " : "(مجموع ما أنفق (جنيه: "; ?></td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSpentFood"/>
   		</td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSpentFashion"/>
   		</td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSpentSpecials"/>
   		</td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSpent"/>
   		</td>
   	</tr>
   	<tr name="totalSavings">
   		<td style='text-align:center;' colspan="6"><?php echo $controller->language == 'en' ? "Total Saved (EGP): " : "(مجموع ما وفر (جنيه: "; ?></td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSavingsFood"/>
   			<!-- <input class="viewAppButton" type="button" id="clearValues" name="clearValues" value="Clear All" onclick="clearValues()"/> -->
   		</td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSavingsFashion"/>
   		</td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSavingsSpecials"/>
   		</td>
   		<td width=100px;>
   			<input type="text" disabled="disabled" value="0" style='margin:6px 6px 6px 6px;width:75px; text-align:center;' name="totalSavings"/>
   		</td>
   	</tr>
</table>
</div>
<br/>
<br/>

<div class="tabbable">
<ul class="nav nav-pills">
  <li class="active"><a href="#pane1" data-toggle="tab"><?php echo $controller->language == 'en' ? "Restaurants & Cafés" : "مطاعم و كافيهات"; ?></a></li>
  <li><a href="#pane2" data-toggle="tab"><?php echo $controller->language == 'en' ? "Fashion" : "الموضه"; ?></a></li>
  <li><a href="#pane3" data-toggle="tab"><?php echo $controller->language == 'en' ? "Specials" : "أخرى"; ?></a></li>
</ul>
   
<div class="tab-content">
<!--Food Table-->

<div class='tab-pane active' id='pane1' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:12px;'>
	<!-- <h3>Food</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="foodTableContent" width="100%" name="foodTableContent">
		<tr>
			<th><?php echo $controller->language == 'en' ? "Choose" : "اختار"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Name" : "اسم"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Price" : "السعر"; ?></th>
			<th><?php echo $controller->language == 'en' ? "# of visits" : "عدد الزيارات"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Privilege" : "امتياز"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Privilege Value" : "قيمة الامتياز"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Refund" : "إعادة مال"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Discount" : "خصم"; ?></th>
		</tr>
		<?php
			$foodCounter = 2;
			foreach ($foodChains as $key => $value) {
				// remove the records with privilege value isn't unique
				if(strlen($value->privilegeValue) <= 4)
				{
					echo "<tr>";
					echo "<td style='text-align:center;'>";
					echo "<input type='checkbox' name='tick' onclick='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' />";
					echo "</td>";
					echo "<td style='text-align:center;'>";
					echo $value->chainDisplayName;
					echo "</td>";
					echo "<td style='text-align:center;width:100px;'>";
					echo "<input type='number' disabled='disabled' value=$value->calculatorBasePrice style='width:60px;' name='singleValue' class='singleValue' onkeyup='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' onchange='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' />";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo "<input type='number' disabled='disabled' value=$value->visitsNumber style='width:60px;' name='noOfVisitsValue' class='singleValue' onkeyup='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")' onchange='calculateTotalSavings(".$foodCounter.",\"foodTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo $value->privilege;
					echo "</td>";
					echo "<td style='text-align:center;' class='privilegeValue'>";
					echo $value->privilegeValue;
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='refund'/>";
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='discount'/>";
					echo "</td>";
					echo "</tr>";   
					
					$foodCounter ++;
				}
			}
	   	?>
	</table>
</div>

<!--Fashion Table-->
<div class='tab-pane' id='pane2' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>
	<!-- <h3>Fashion</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="fashionTableContent" width="100%" name="fashionTableContent">
		<tr>
			<th><?php echo $controller->language == 'en' ? "Choose" : "اختار"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Name" : "اسم"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Price" : "السعر"; ?></th>
			<th><?php echo $controller->language == 'en' ? "# of visits" : "عدد الزيارات"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Privilege" : "امتياز"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Privilege Value" : "قيمة الامتياز"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Refund" : "إعادة مال"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Discount" : "خصم"; ?></th>
		</tr>
		<?php
			$fashionCounter = 2;
			foreach ($fashionChains as $key => $value) {
				// remove the records with privilege value isn't unique
				if(strlen($value->privilegeValue) <= 4)
				{
					echo "<tr>";
					echo "<td style='text-align:center;'>";
					echo "<input type='checkbox' name='tick' onclick='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")' />";
					echo "</td>";
					echo "<td style='text-align:center;'>";
					echo $value->name;
					echo "</td>";
					echo "<td style='text-align:center;width:100px;'>";
					echo "<input type='number' disabled='disabled' value=$value->calculatorBasePrice style='width:60px;' name='singleValue' class='singleValue' onkeyup='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")' onchange='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo "<input type='number' disabled='disabled' value=$value->visitsNumber  style='width:60px;' name='noOfVisitsValue' class='singleValue' onkeyup='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")' onchange='calculateTotalSavings(".$fashionCounter.",\"fashionTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo $value->privilege;
					echo "</td>";
					echo "<td style='text-align:center;' class='privilegeValue'>";
					echo $value->privilegeValue;
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='refund'/>";
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='discount'/>";
					echo "</td>";
					echo "</tr>";   
					
					$fashionCounter ++;
				}
			}
	   	?>
	</table>
</div>

<!--Special Table-->     
<div class='tab-pane' id='pane3' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>
	<!-- <h3>Specials</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="specialTableContent" width="100%" name="specialTableContent">
		<tr>
			<th><?php echo $controller->language == 'en' ? "Choose" : "اختار"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Name" : "اسم"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Price" : "السعر"; ?></th>
			<th><?php echo $controller->language == 'en' ? "# of visits" : "عدد الزيارات"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Privilege" : "امتياز"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Privilege Value" : "قيمة الامتياز"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Refund" : "إعادة مال"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Discount" : "خصم"; ?></th>
		</tr>
		<?php
			$specialCounter = 2;
			foreach ($specialsChains as $key => $value) {
				// remove the records with privilege value isn't unique
				if(strlen($value->privilegeValue) <= 4)
				{
					echo "<tr>";
					echo "<td style='text-align:center;'>";
					echo "<input type='checkbox' name='tick' onclick='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")' />";
					echo "</td>";
					echo "<td style='text-align:center;'>";
					echo $value->name;
					echo "</td>";
					echo "<td style='text-align:center;width:100px;'>";
					echo "<input type='number' disabled='disabled' value=$value->calculatorBasePrice style='width:60px;' name='singleValue' class='singleValue' onkeyup='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")' onchange='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo "<input type='number' disabled='disabled' value=$value->visitsNumber style='width:60px;' name='noOfVisitsValue' class='singleValue' onkeyup='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")' onchange='calculateTotalSavings(".$specialCounter.",\"specialTableContent\")'/>";
					echo "</td>";	
					echo "<td style='text-align:center;'>";
					echo $value->privilege;
					echo "</td>";
					echo "<td style='text-align:center;' class='privilegeValue'>";
					echo $value->privilegeValue;
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='refund'/>";
					echo "</td>";
					echo "<td style='width:100px;'>";
					echo "<input type='text' disabled='disabled' value='0' style='margin-left:6px;width:60px;' name='discount'/>";
					echo "</td>";
					echo "</tr>";   
					
					$specialCounter ++;
				}
			}
	   	?>
	</table>    
</div>   <!--Special Table-->     
</div><!-- /.tab-content -->
</div><!-- /.tabbable -->