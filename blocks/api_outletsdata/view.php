<script type="text/javascript">
	function slideToggle(id)
	{
		$('#content'+id).slideToggle('slow');
	}

    function initialize(locationsJson) {
		var map = new google.maps.Map(document.getElementById("map"), {
			zoom: 7,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			mapTypeControlOptions:
			{
			    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
			},
			navigationControl: true,
			navigationControlOptions:
			{
				style: google.maps.NavigationControlStyle.SMALL
			}
		});
		for (var key in locationsJson) {
			if (locationsJson.hasOwnProperty(key)) {
				var latLng = new google.maps.LatLng(locationsJson[key].lat,locationsJson[key].lon);
		  		var marker = new google.maps.Marker({
				   	position: latLng,
				   	map: map,
				   	animation: google.maps.Animation.DROP
				 });
			}
		}
		map.setCenter(latLng);
			
    }

    </script>

    <?php 
    //var_dump($auth_token);exit();
	$cities = array();
	$districts = array();
	$locations = array();
	foreach ($data as $key => $value) {
			
		if(!is_array($cities[$value->address->city->name]))
			$cities[$value->address->city->name] = array();
		
		array_push($cities[$value->address->city->name],$value->address->district->name);
		array_push($districts,$value->address->district->name);
		$lon = $value->location->loc->lon;
		$lat = $value->location->loc->lat;
		array_push($locations , array("lon"=>$lon,"lat"=>$lat));
	}
	$locationsJson = json_encode($locations);
	echo "<script>
			jQuery(document).ready(function(){
				initialize({$locationsJson});
			});
		</script>";
	?>
	

	<?php
	//$cities = array_unique($cities);
	$districts = array_unique($districts);
	$name = $chainData->name;
	foreach($chainData->images as $img)
	{
		if($img->label == "websitecoverphoto" )
		{
			$path = $img->path;
			$path = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path.'&width=650&height=244&gravity=center&oauth_token='.$auth_token;
			break;
		}
	}

	?>
	<div class="sep"></div>
	
	<div class="row">
		<div class="span9" style="padding-top: 10px;">
			<!-- <div class="span3"> -->
				<img src="<?=$path;?>" alt="<?=$name?>" width="650" height="268" />
			<!-- </div> -->

			<div>
				<div class="outletProfile-PrivilegeDescription" style="line-height:28px;">
					<font color="#014B8D"><b><?php echo $chainData->privilegeDescription;?></b></font>
				</div>

				<?php $imageName = str_replace("%","%25",$chainData->privilegeValue); ?>
				<div>
				<?php if($chainData->upTo == 'true') {?>
						<img src="/themes/xc_theme/img/<?php echo $controller->language == 'en' ? "en/" : "ar/"; ?>upto.png">
						<img src="/themes/xc_theme/img/<?php echo $controller->language == 'en' ? "en/" : "ar/"; echo $imageName; ?>.png" style="padding-top: 30px; padding-right: 10px;">
				<?php }elseif($chainData->privilege == 'Instant Discount' || $chainData->privilege == 'خصم فوري'){ ?>
					<img src="/themes/xc_theme/img/<?php echo $controller->language == 'en' ? "en/" : "ar/"; echo $imageName; ?>.png" style="padding-top: 30px;padding-left: 30px;padding-right: 20px;">
				<?php }elseif($chainData->privilege != 'Instant Discount'){ ?>
					<img src="/themes/xc_theme/img/<?php echo $controller->language == 'en' ? "en/Refund_" : "ar/Refund_"; echo $imageName; ?>.png" style="padding-top: 30px;padding-left: 30px;padding-right: 20px;">
				<?php } ?>
						<img src="/themes/xc_theme/img/separator.png" style="height: 95px; padding-top: 20px;">
				</div>
			</div>
		</div>
	</div>

<div style="clear:both;height:24px;"></div>

<div class="row">
	<div class="span9">
		<div class="tab-pane" id="tabs-basic">
			<div class="tabbable">
				<ul class="nav nav-tabs">
					<?php
						$cityCounter = 0;
						foreach ($cities as $key => $val) {
						$cityCounter++;
						if ($cityCounter == 1) {
					?>
						<li class="active"><a href=<?php echo "#tabs1-pane".$cityCounter;?> data-toggle="tab"><?php echo $key;?></a></li>
						<?php }else{?>
						<li class=""><a href=<?php echo "#tabs1-pane".$cityCounter;?> data-toggle="tab"><?php echo $key;?></a></li>
					<?php
						}}
					?>			
				</ul>
				<div class="tab-content">
					<?php
					$districtCounter = 0;
					foreach ($cities as $keyDistrict => $valueDistrict) {
						$districtCounter++;
						if ($districtCounter == 1) {
						?>
						<div class="tab-pane active" id=<?php echo "tabs1-pane".$districtCounter;?>>
						<?php }else{ ?>
						<div class="tab-pane" id=<?php echo "tabs1-pane".$districtCounter;?>>
						<?php
							}
						// echo "<div onclick='slideToggle(".$districtCounter.")' style='background:#e47721;width:30%;border-radius:10px;padding:10px;margin-bottom:-10px;maring-left:-10px;'>";
							// echo "<a href='#' onclick='return false;' style='color:white;text-decoration:none'>";
							// echo "<span style='font-size:20px;'>".$valueDistrict."</span>";
							// echo "</a>";
						// echo "</div>";
						// echo "<div id='content".$districtCounter."' ";
						// if($districtCounter != 1)
						// {
							// echo "style='display:none;background:#e47721;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>";
						// }	
						// else {
							// echo "style='background:#e47721;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>";
						// }
						foreach ($data as $key => $value) {
							if($keyDistrict == $value->address->city->name)
							{
								echo "<div style='width:90%;background:#e6e6e6;padding:10px;margin-top:10px;border-radius:5px;'>";
								echo "<b>Address: </b>". $value->address->street;
								echo "<br/>";
								echo "<b>City: </b>". $value->address->city->name;
								echo "<br/>";
								echo "<b>District: </b>". $value->address->district->name;
								echo "<br/>";
								echo "<b>Phones: </b>". implode(",",$value->phones?$value->phones:array("N/A"));
								echo "</div>";
							}
						}
					echo "</div>";
					// echo "</div>";
					// echo "<br/>";
				}
				?>
					<div id="map" style="width: 650px;float:left; height: 400px; border: 0px; margin-top: 10px;">
					
					</div>
				</div>
				</div>		
			</div>
	</div> 
		
	</div>
</div>



<!-- <div class="sep"></div> -->
  
<div class="row">
		              
	<!-- <div class="span9"> -->
<!-- <?php
$districtCounter = 0;
foreach ($districts as $keyDistrict => $valueDistrict) {
	$districtCounter++;
	?>
	<div class="tab-pane" id=<?php echo "#tabs1-pane".$districtCounter;?>>
	<?php
	echo "<div onclick='slideToggle(".$districtCounter.")' style='background:#e47721;width:30%;border-radius:10px;padding:10px;margin-bottom:-10px;maring-left:-10px;'>";
		echo "<a href='#' onclick='return false;' style='color:white;text-decoration:none'>";
		echo "<span style='font-size:20px;'>".$valueDistrict."</span>";
		echo "</a>";
	echo "</div>";
	echo "<div id='content".$districtCounter."' ";
	if($districtCounter != 1)
	{
		echo "style='display:none;background:#e47721;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>";
	}	
	else {
		echo "style='background:#e47721;width:90%;padding:10px;border-radius:12px;border-top-left-radius:0px;'>";
	}
		foreach ($data as $key => $value) {
			if($valueDistrict == $value->address->district->name)
			{
				echo "<div style='width:90%;background:#e6e6e6;padding:10px;margin-top:10px;border-radius:5px;'>";
				echo "<b>Address: </b>". $value->address->street;
				echo "<br/>";
				echo "<b>City: </b>". $value->address->city->name;
				echo "<br/>";
				echo "<b>District: </b>". $value->address->district->name;
				echo "<br/>";
				echo "<b>Phones: </b>". implode(",",$value->phones?$value->phones:array("N/A"));
				echo "</div>";
			}
		}
	echo "</div>";
	echo "</div>";
	echo "<br/>";
}
?>
	</div> -->
<!-- </div> -->

<div style="clear:both;height:24px;"></div>
	<!-- <div class="row">
		<div class="span9">
			<div id="map" style="width: 660px;float:right; height: 400px; border: 0px; padding: 0px;">
				
			</div>
		</div>
	</div> -->
		               