<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

	require_once('libraries/pest/PestJSON.php');

	class ApiOutletsdataBlockController extends BlockController {
		
		var $pobj;
		
		protected $btDescription = "A simple block";
		protected $btName = "Api Outlets Data ";
		protected $btTable = 'btApiOutlets';
		protected $btInterfaceWidth = "350";
		protected $btInterfaceHeight = "300";
		
		//protected $auth_token = "bb81e7bfda7df5899af4c5316bac9942";
		protected $auth_token = "8ff0096821120a0f2975dc3f0b8ff9ff";
		
		//protected $foodCategoryId = '5017d2db9b62860159000187';
		//protected $fashionCategoryId = '5017d2eb9b62866b50000169';
		//protected $otherCategoryId = '50290cd09b6286bb41000023';
		
		protected $foodCategoryId = '506addfe9b6286e82a000015';
		protected $fashionCategoryId = '506ade239b6286e72a00001c';
		protected $otherCategoryId = '506ade359b62869b70000017';
		
		protected $category = '506addfe9b6286e82a000015';
		public $language;
		
		
		private function getLanguage(){
			//this will get language of the current page
			$lh = Loader::helper('section', 'multilingual');
			
			if($lh->getLanguage() == "ar_EG")
		    {
		       $this->language = 'ar';
		    }
		    else{
			    $this->language = 'en';
		    }
		}
		public function view() {
			$this->getLanguage();
			if(isset($_GET['chainId'])&&($_GET['collection']))
			{
				
				$chainId = $_GET['chainId'];
				$collection = $_GET['collection'];
				$pest = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
				//get this chain data
				$resp = $pest->get('/chains/'.$chainId.'?oauth_token='.$this->auth_token.'&language='.$this->language);
				$resp = json_decode($resp);
				$this->set('chainData', $resp[0]);
				//get all data under that chain
				$resp = $pest->get('/'.$collection.'?oauth_token='.$this->auth_token.'&limit=all&chainId='.$chainId.'&language='.$this->language);
				$resp = json_decode($resp);
				$this->set('data', $resp);
				
				$this->set('auth_token',$this->auth_token);
				
			}
			else {
				$this->set('data', array());
			}
			
		}
		
	}
	
?>