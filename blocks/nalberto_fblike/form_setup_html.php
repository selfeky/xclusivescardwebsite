<?php    defined('C5_EXECUTE') or die(_("Access Denied.")); 
?> 
<style>
table#fblikeBlockSetup th {font-weight: bold; text-style: normal; padding-right: 8px; white-space: nowrap; vertical-align:top ; padding-bottom:8px}
table#fblikeBlockSetup td{ font-size:12px; vertical-align:top; padding-bottom:8px;}
</style> 


  <table id="fblikeBlockSetup" width="100%">
    <tr>
  		<th><?php   echo t('First time use instructions')?>
      </th>
  		<td>
        <div class="note"><a href="http://developers.facebook.com/setup" target="_blank"><?php   echo t('You need to sign up for your key if you dont have one yet.')?></a>
        </div>
      </td>
  	</tr>
  	<tr>
  		<th><?php   echo t('Facebook Application Id')?>:
        <div class="note"><a href="http://developers.facebook.com/setup" target="_blank"><?php   echo t('Facebook App ID')?></a>
        </div>
      </th>
  		<td>
  			<input id="ccm_fblike_block_api_key" name="App_ID" value="<?php   echo $fblikeObj->App_ID?><?php   echo($this->App_ID);?>" maxlength="255" type="text" style="width:200">
  		</td>
  	</tr>
  	<tr>
  		<th><?php   echo t('Show Faces?')?><div class="note"><?php   echo t('Show profile faces.')?></div></th>
  		<td>
  		<!--input id="ccm_fblike_block_show_faces" name="show_faces" value="<?php   echo $fblikeObj->show_faces?><?php   echo($this->show_faces);?>" maxlength="255" type="text" style="width:50"-->
  			<?php  
        $showfaceoption =   $fblikeObj->show_faces . $this->show_faces;
        //echo ($showfaceoption == "True") ? "selected = yes": "selected = No";
        ?>
        <input type="radio" name="show_faces" value="True" <?php   echo ($showfaceoption == "True") ? "checked='checked'": ""?>> Yes    <br />
        <input type="radio" name="show_faces" value="False" <?php   echo ($showfaceoption == "False") ? "checked='checked'": ""?>> No 
      </td>
  	</tr>	
  	<tr>
  		<th><?php   echo t('Show Send Button?')?><div class="note"><?php   echo t('Show send to friend button.')?></div></th>
  		<td>
  			<?php  
        $showsendbutton =   $fblikeObj->send_button . $this->send_button;
        //echo ($showfaceoption == "True") ? "selected = yes": "selected = No";
        ?>
        <input type="radio" name="send_button" value="true" <?php   echo ($showsendbutton == "true") ? "checked='checked'": ""?>> Yes    <br />
        <input type="radio" name="send_button" value="false" <?php   echo ($showsendbutton == "false") ? "checked='checked'": ""?>> No 
      </td>
  	</tr>
  	<tr>
  		<th><?php   echo t('Layout Style')?><div class="note"><?php   echo t('Show send to friend button.')?></div></th>
  		<td>
  			<?php  
        $layoutstyle =   $fblikeObj->layout_style . $this->layout_style;
        //echo ($showfaceoption == "True") ? "selected = yes": "selected = No";
        ?>
        <select name="layout_style">
        <option value="standard" <?php   echo ($layoutstyle == "standard") ? "checked='checked'": ""?>>Standard</option>
        <option value="button_count" <?php   echo ($layoutstyle == "button_count") ? "checked='checked'": ""?>>Button Count</option>
        <option value="box_count" <?php   echo ($layoutstyle == "box_count") ? "checked='checked'": ""?>>Box Count</option>
        </select> 
      </td>
  	</tr>
  	<tr>
  		<th><?php   echo t('Color Scheme')?><div class="note"><?php   echo t('Background Color Scheme')?></div></th>
  		<td>
  			<?php  
        $colorscheme =   $fblikeObj->color_scheme . $this->color_scheme;
        //echo ($showfaceoption == "True") ? "selected = yes": "selected = No";
        ?>
        <select name="color_scheme">
        <option value="light" <?php   echo ($colorscheme == "light") ? "checked='checked'": ""?>>Light</option>
        <option value="dark" <?php   echo ($colorscheme == "dark") ? "checked='checked'": ""?>>Dark</option>
        </select> 
      </td>
  	</tr>			
  	<tr>
  		<th><?php   echo t('Width')?><div class="note"><?php   echo t('Ex: 600')?></div></th>
  		<td><input id="ccm_fblike_block_width" name="width" value="<?php   echo $fblikeObj->width?><?php   echo($this->width);?>" maxlength="255" type="text" style="width:50"></td>
  	</tr>	
	
  </table>

