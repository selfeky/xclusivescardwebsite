<?php   
defined('C5_EXECUTE') or die(_("Access Denied."));
	class NalbertoFblikeBlockController extends BlockController {
		
		var $pobj;
		
		protected $btDescription = "Add a Facebook Like button to your pages to promote your site.";
		protected $btName = "Facebook Like";
		protected $btTable = 'btnalbertoFbLike';
		protected $btInterfaceWidth = "420";
		protected $btInterfaceHeight = "300";
		
		public $App_ID = "";
		public $show_faces = "";
		public $width = "";
		public $layout_style = "";
		public $color_scheme = "";
		public $send_button = "";


		public function getBlockTypeDescription() {
			return t("Add a Facebook Like button to your pages to promote your site.");
		}
		
		public function getBlockTypeName() {
			return t("Facebook Like Button");
		}		
		
		public function getJavaScriptStrings() {
			return array(
				'fblike-api-key' => t('Please enter a valid Facebook API key.')
			);
		}
		
		function __construct($obj = null) {		
			parent::__construct($obj);	 
		}		
		
		public function add(){
			$db = Loader::db();		
			$q = 'SELECT App_ID FROM '.$this->btTable.' WHERE App_ID!=""';
			$this->set('App_ID',$db->getOne($q));
			$this->set('width', '600');
			$this->set('show_faces', 'True');
			$this->set('layout_style', 'standard');
			$this->set('color_scheme', 'light');
			$this->set('send_button', 'true');
		}
		
		function view(){ 
			$this->set('bID', $this->bID);
			$this->set('App_ID', $this->App_ID);	
			$this->set('width', $this->width);
			$this->set('show_faces', $this->show_faces);
			$this->set('layout_style', $this->layout_style);
			$this->set('color_scheme', $this->color_scheme);
			$this->set('send_button', $this->send_button);
		
		}
		
		function save($data) { 
			$args['App_ID'] = isset($data['App_ID']) ? trim($data['App_ID']) : '';
			$args['width'] = isset($data['width']) ? trim($data['width']) : '';
			$args['show_faces'] = isset($data['show_faces']) ? trim($data['show_faces']) : '';
			$args['layout_style'] = isset($data['layout_style']) ? trim($data['layout_style']) : '';
			$args['color_scheme'] = isset($data['color_scheme']) ? trim($data['color_scheme']) : '';
			$args['send_button'] = isset($data['send_button']) ? trim($data['send_button']) : '';
			
			parent::save($args);
		}
		
  }

	
?>
