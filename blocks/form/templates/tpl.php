<?php    
defined('C5_EXECUTE') or die("Access Denied.");
$survey=$controller;  
//echo $survey->surveyName.'<br>';
$miniSurvey=new MiniSurvey($b);
$miniSurvey->frontEndMode=true;
?>
<style>
	.form-horizontal .ccm-input-captcha {
		width: 150px;
	}
	.form-horizontal .ccm-captcha-image {
		width: 160px;
	}
	
	.form-horizontal label {
		width: 160px;
		margin-left: 8px;
	}
	.form-horizontal .formBlockSurveyTable {
		width:160px;
	}
	
</style>
<a name="<?php  echo $survey->questionSetId ?>"></a><br/>
<?php  if ($invalidIP) { ?>
<div class="ccm-error"><p><?php echo $invalidIP?></p></div>
<?php  } ?>
<h2>Get Your Card</h2>
<form class="form-horizontal" enctype="multipart/form-data" id="miniSurveyView<?php echo intval($bID)?>" class="miniSurveyView" method="post" action="<?php  echo $this->action('submit_form').'#'.$survey->questionSetId?>">
	<?php   if( $_GET['surveySuccess'] && $_GET['qsid']==intval($survey->questionSetId) ){ ?>
		<!--<div id="msg"><?php  //echo $survey->thankyouMsg ?></div>-->
		<script>
			$(document).ready(function() {
				$('#myModalt').modal({
					backdrop : true,
					keyboard : true
				}).css({
					width : 'auto',
					'margin-left' : function() {
						return -($(this).width() / 2);
					}
				});
				
				setTimeout(myTimer, 5000);
				function myTimer() {
					window.location.href = "http://www.xclusivescard.com";
				}
			});
		</script>
	    
	<?php   }elseif(strlen($formResponse)){ ?>
		<div id="msg">
			<?php  echo $formResponse ?>
			<?php  
			if(is_array($errors) && count($errors)) foreach($errors as $error){
				echo '<div class="error">'.$error.'</div>';
			} ?>
		</div>
	<?php  } ?>
	<input name="qsID" type="hidden" value="<?php echo  intval($survey->questionSetId)?>" />
	<input name="pURI" type="hidden" value="<?php echo  $pURI ?>" />
	<?php   $miniSurvey->loadSurvey( $survey->questionSetId, 0, intval($bID) );  ?>
	
	<input name="source" type="hidden" value="5" /> 
</form>

		<div class="modal fade" id="myModalt">
		    <div class="modal-header">
			    <a class="close" data-dismiss="modal">×</a>
				<h3>Thank you</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="span5">
						Thank you for signing up for an Xclusives Card.
						<br/>
					</div>
				</div>
				<div class="row">
					<div class="span5">
						An Xclusives card representative will contact you with in 24 hours.
						<br/>
					</div>
				</div>
				<div class="row">
					<div class="span5">
						You will receive an email confirming your sign up.
					</div>
				</div>
			</div>
		    
		    <div class="modal-footer">
            	
		    </div>
		    
	    </div>

<script type='text/javascript'>//<![CDATA[ 
$(function(){
/**
 * Project:     Jquery Inputlabels
 * Date:         23/03/2011 
 * Company:     Doghouse Media
 * URL:         http://www.dhmedia.com.au
 * Author:         Alex Parker
 */
( function($) {
    $.fn.inputLabels = function(){
    
        if (this.is('label')) {
            /* If this element is a label then convert it directly */
            convertLabel(this);
        } else {
            /* If this element is not a label then find any children that are and operate upon them */
            this.find('label').each(function(index,value) {
                convertLabel($(this));
            });
        }
    
        /* Remove label from DOM and initialise input value with label's title */
        function initialiseLabel(labelElement, inputField, labelTitle) {
            labelElement.remove();
            inputField.val(labelTitle);        
        }
    
        /* This function does all the crazy magic */        
        function convertLabel(labelElement) {
    
            /* Fetch the input field the label is for */
            var labelFor = labelElement.attr('for');
            if (labelFor) {
                var inputField = $("#" + labelFor);
                var labelTitle = labelElement.text();
                
                /* Fetch the parent form in which these fields reside */
                var parentForm = inputField.parents('form');

                /* We're choosy about which input types we want to apply this transform to */
                if (inputField.is('input') && inputField.attr('type') == 'text' || inputField.attr('type') == 'textarea' || inputField.attr('type') == 'email') {
                
                    /* Remove label from DOM and initialise input value with label's title */
                    initialiseLabel(labelElement, inputField, labelTitle);
                    
                    /* 
                     * Bind click event 
                     * - Clear the input field if it contains the default value
                     */
                    inputField.click( function() {
                        if (inputField.val() == labelTitle) {
                            inputField.val("");
                        }            
                    });
                    
                    /* 
                     * Bind blur event 
                     * - Restore the label value if the field is empty
                     */
                    inputField.blur( function() {
                        if ($(this).val() == "") {
                            $(this).val(labelTitle);
                        }            
                    });
                    
                    /* 
                     * Bind submit event
                     * - Clear any default label values prior to submit (we don't want them cluttering up our data!)
                     */
                    parentForm.submit( function() {
                        if ( inputField.val() == labelTitle ) {
                            inputField.val("");
                        }
                    });    
                } 
            }
        }
    }
})(jQuery);
});//]]>  

$(document).ready(function() {
   $('form').inputLabels()
});

</script>
