<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	//this will get language of the current page
	$lh = Loader::helper('section', 'multilingual');
	
	if($lh->getLanguage() == "ar_EG")
    {
       $navPageRoot = Page::getByPath('/ar');
    }
    elseif($lh->getLanguage() == "en_US"){
	   $navPageRoot = Page::getByPath('/en');
    }
    
    if ($navPageRoot->cID) {
	    $controller->displayPages = 'custom';
	    $controller->displayPagesCID = $navPageRoot->getCollectionID();
    } else {
       $controller->displayPages = 'top';
    }

    $controller->orderBy = 'display_asc';                    
    $controller->displaySubPages = 'all'; 
    $controller->displaySubPageLevels = 'custom';

    $aBlocks = $controller->generateNav();
	
	$containsPages = false;
	
	
	$nh = Loader::helper('navigation');
	
	//this will create an array of parent cIDs 
	$inspectC=$navPageRoot;
	$selectedPathCIDs=array( $inspectC->getCollectionID() );
	$parentCIDnotZero=true;	
	while($parentCIDnotZero){
		$cParentID=$inspectC->cParentID;
		if(!intval($cParentID)){
			$parentCIDnotZero=false;
		}else{
			$selectedPathCIDs[]=$cParentID;
			$inspectC=Page::getById($cParentID);
		}
	} 	
	$count = 0;
	foreach($aBlocks as $ni) {
		$_c = $ni->getCollectionObject();
		if (!$_c->getCollectionAttributeValue('exclude_nav')) {
			
			$count ++;
			
			$target = $ni->getTarget();
			if ($target != '') {
				$target = 'target="' . $target . '"';
			}
			if (!$containsPages) {
				// this is the first time we've entered the loop so we print out the UL tag
				echo("<ul class=\"nav\">");
			}
			
			$containsPages = true;
			
			$thisLevel = $ni->getLevel();
			if ($thisLevel > $lastLevel) {
				echo("<ul>");
			} else if ($thisLevel < $lastLevel) {
				for ($j = $thisLevel; $j < $lastLevel; $j++) {
					if ($lastLevel - $j > 1) {
						echo("</li></ul>");
					} else {
						echo("</li></ul></li>");
					}
				}
			} else if ($i > 0) {
				echo("</li>");
			}

			$pageLink = false;
			
			if ($_c->getCollectionAttributeValue('replace_link_with_first_in_nav')) {
				$subPage = $_c->getFirstChild();
				if ($subPage instanceof Page) {
					$pageLink = $nh->getLinkToCollection($subPage);
				}
			}
			
			if (!$pageLink) {
				$pageLink = $ni->getURL();
			}

			if ($navPageRoot->getCollectionID() == $_c->getCollectionID()) { 
				echo('<li class="nav-selected nav-path-selected"><a class="nav-selected nav-path-selected" ' . $target . ' href="' . $pageLink . '">' . $ni->getName() . '</a>');
				echo('<li class="divider-vertical"></li>');
			} elseif ( in_array($_c->getCollectionID(),$selectedPathCIDs) && ($_c->getCollectionID() != HOME_CID) ) {
				echo('<li class="nav-path-selected"><a class="nav-path-selected" href="' . $pageLink . '" ' . $target . '>' . $ni->getName() . '</a>');
				echo('<li class="divider-vertical"></li>');
			} else {
				echo('<li><a href="' . $pageLink . '" ' . $target . ' >' . $ni->getName() . '</a>');
				echo('<li class="divider-vertical"></li>');
			}	
			$lastLevel = $thisLevel;
			$i++;
			
			
		}
	}
	
	$thisLevel = 0;
	if ($containsPages) {
		for ($i = $thisLevel; $i <= $lastLevel; $i++) {
			echo("</li></ul>");
		}
	}

?>