<script type="text/javascript">
	var center = null;
	var map = null;
	var currentPopup;
	// var bounds = new google.maps.LatLngBounds();
	var limit = 10;
	
	var completeLoading = true;
	
	var startAjaxCall = null;
	var foodAjaxCall = null;
	var fashionAjaxCall = null;
	var specialsAjaxCall = null;
	var defaultAjaxCall = null;
	
	var offsetFood = 0;
	var offsetFashion = 0;
	var offsetSpecial = 0;
	
	var mapCounter = 1;
	var lookupJsonLocations = {"6thOctober": {"lat": "29.954935", "lon": "30.94574"}
								,"Alexandria": {"lat": "31.212801", "lon": "30.08606"}
								,"Assiut": {"lat": "27.200985", "lon": "31.17478"}
								,"Beni Sueif": {"lat": "29.058571", "lon": "31.228638"}
								,"Cairo": {"lat": "30.133333", "lon": "31.400000"}
								,"Dubai": {"lat": "25.275745", "lon": "55.346375"}
								,"El Mahalla El Kobra": {"lat": "30.967306", "lon": "31.16272"}
								,"El Mansoura": {"lat": "31.092926", "lon": "31.559601"}
								,"El Menya": {"lat": "28.101058", "lon": "30.761719"}
								,"El Qalyubiya": {"lat": "31.117251", "lon": "31.573763"}
								,"Giza": {"lat": "30.013368", "lon": "31.217651"}
								,"Hurghada": {"lat": "27.36461", "lon": "33.860937"}
								,"Ismailia": {"lat": "30.596252", "lon": "32.287788"}
								,"Luxor": {"lat": "25.668846", "lon": "32.542578"}
								,"North Coast": {"lat": "31.22895", "lon": "30.015335"}
								,"Port Said": {"lat": "31.263438", "lon": "32.295341"}
								,"Sharm El Sheikh": {"lat": "27.936181", "lon": "34.84314"}
								,"Shebin El Koum": {"lat": "30.559896", "lon": "31.018696"}
								,"Sohag": {"lat": "26.555365", "lon": "31.70929"}
								,"Suez": {"lat": "29.966834", "lon": "32.549807"}
								,"Tanta": {"lat": "30.559896", "lon": "31.018696"}};
	function initMap(cityName)
	{
		var latitude = "30.133333";
		var longitude = "31.400000";
		for (var key in lookupJsonLocations) {
			if (lookupJsonLocations.hasOwnProperty(key)) {
		  		if(key == cityName)
		  		{
		  			latitude = lookupJsonLocations[key].lat;
				    longitude = lookupJsonLocations[key].lon;
		  		}
			}
		}
		map = new google.maps.Map(document.getElementById("map"), {
			zoom: 9,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			mapTypeControlOptions:
			{
			    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
			},
			navigationControl: true,
			navigationControlOptions:
			{
				style: google.maps.NavigationControlStyle.SMALL
			}
		});
		map.setCenter(new google.maps.LatLng(latitude,longitude));
		return map;
	}
   
	function filterFoodData(category , offsetFood , foodMap){
		var markers = [];
		foodAjaxCall = jQuery.ajax({
			url: "<?php echo str_replace('&amp;', '&', $this->action('majax_router_filter_Data')); ?>",
			data: {category:category , offset:offsetFood}
		}).done(function(response) {
			$.each($.parseJSON(response), function(key,value){
				var info = '<div style="float:left;width:90%"><b>' + value.name + "</b><br />" + value.address + "<br />" + value.district + "<br />" + value.city + "<br />" + value.phones + '</div><div style="float:left;width:10%"><a href="" onclick="currentPopup.close();return false;"><img src="/themes/xc_theme/img/closeButton.gif"></a></div>';
				offsetFood ++;
				var latLng = new google.maps.LatLng(value.lat,value.long);
		        var marker = new google.maps.Marker({
		            position: latLng,
		            icon:value.icon,
            		animation: google.maps.Animation.DROP
		        });
		        
		        //var myOptions = {
                        // disableAutoPan: false
                      //  ,maxWidth: 0
                      //  ,pixelOffset: new google.maps.Size(-140, 0)
                     //   ,zIndex: null
                      //  ,boxStyle: { 
                     //     background: "url('http://www.designkindle.com/wp-content/uploads/2010/08/aura-background-large-1.jpg') no-repeat"
                     //     ,opacity: 0.75
                     //     ,width: "280px"
                    //     }
                    //    ,closeBoxMargin: "10px 2px 2px 2px"
                    //    ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
                    //    ,infoBoxClearance: new google.maps.Size(1, 1)
                    //    ,isHidden: false
                    //    ,pane: "floatPane"
                    //    ,enableEventPropagation: false
                    //    ,content: info
               // };
                
		        var popup = new google.maps.InfoWindow(
				{
				    content: info,
				    maxWidth: 300
				});
			
				//var ib = new InfoBox(myOptions);

				google.maps.event.addListener(marker, "click", function()
				{
					marker.setAnimation(google.maps.Animation.BOUNCE);
				    if (currentPopup != null)
				    {
				        currentPopup.close();
				        currentPopup = null;
				    }
				   // ib.open(foodMap, marker);
				    popup.open(foodMap, marker);
				    currentPopup = popup;
				    //currentPopup = ib;
				    setTimeout(function(){marker.setAnimation(null);},2500);
				    
			   	});
		        markers.push(marker);
			});
			
			$(document).ready(function(){
				mcOptions = {
					styles: [
						{
							// blue
							height: 53,
							url: "/themes/xc_theme/img/m1.png",
							width: 53
						}
				]}
			  	if(completeLoading == true)
				{
					$('#loadingImg').fadeIn("slow");
					var markerCluster = new MarkerClusterer(foodMap, markers , mcOptions);
					filterFoodData(category , offsetFood , foodMap);
				}
				else
				{
					$('#loadingImg').fadeOut("slow");
					var markerCluster = new MarkerClusterer(foodMap, markers , mcOptions);
				}
			});
		});		
	}
	
	function filterFashionData(category , offsetFashion , fashionMap){
		var markers = [];
		foodAjaxCall = jQuery.ajax({
			url: "<?php echo str_replace('&amp;', '&', $this->action('majax_router_filter_Data')); ?>",
			data: {category:category , offset:offsetFashion}
		}).done(function(response) {
			$.each($.parseJSON(response), function(key,value){
				var info = '<div style="float:left;width:90%"><b>' + value.name + "</b><br />" + value.address + "<br />" + value.district + "<br />" + value.city + "<br />" + value.phones + '</div><div style="float:left;width:10%"><a href="" onclick="currentPopup.close();return false;"><img src="/themes/xc_theme/img/closeButton.gif"></a></div>';
				offsetFashion ++;
				var latLng = new google.maps.LatLng(value.lat,value.long);
		        var marker = new google.maps.Marker({
		            position: latLng,
		            icon:value.icon,
            		animation: google.maps.Animation.DROP
		        });
		        var popup = new google.maps.InfoWindow(
				{
				    content: info,
				    maxWidth: 300
				});
			
				
				google.maps.event.addListener(marker, "click", function()
				{
					marker.setAnimation(google.maps.Animation.BOUNCE);
				    if (currentPopup != null)
				    {
				        currentPopup.close();
				        currentPopup = null;
				    }
				    popup.open(fashionMap, marker);
				    currentPopup = popup;
				    setTimeout(function(){marker.setAnimation(null);},2500);
				    
			   	});
		        markers.push(marker);
			});
			
			$(document).ready(function(){
				mcOptions = {
					styles: [
						{
							// red
							height: 66,
							url: "/themes/xc_theme/img/m3.png",
							width: 66
						}
				]}
			  	if(completeLoading == true)
				{
					$('#loadingImg').fadeIn("slow");
					var markerCluster = new MarkerClusterer(fashionMap, markers , mcOptions);
					filterFashionData(category , offsetFashion , fashionMap);
				}
				else
				{
					$('#loadingImg').fadeOut("slow");
					var markerCluster = new MarkerClusterer(fashionMap, markers , mcOptions);
				}
			});
		});		
	}
	
	function filterSpecialData(category , offsetSpecial , specialMap){
		var markers = [];
		fashionAjaxCall = jQuery.ajax({
			url: "<?php echo str_replace('&amp;', '&', $this->action('majax_router_filter_Data')); ?>",
			data: {category:category , offset:offsetSpecial}
		}).done(function(response) {
			$.each($.parseJSON(response), function(key,value){
				var info = '<div style="float:left;width:90%"><b>' + value.name + "</b><br />" + value.address + "<br />" + value.district + "<br />" + value.city + "<br />" + value.phones + '</div><div style="float:left;width:10%"><a href="" onclick="currentPopup.close();return false;"><img src="/themes/xc_theme/img/closeButton.gif"></a></div>';
				offsetSpecial ++;
				var latLng = new google.maps.LatLng(value.lat,value.long);
		        var marker = new google.maps.Marker({
		            position: latLng,
		            icon:value.icon,
            		animation: google.maps.Animation.DROP
		        });
		        var popup = new google.maps.InfoWindow(
				{
				    content: info,
				    maxWidth: 300
				});
			
				
				google.maps.event.addListener(marker, "click", function()
				{
					marker.setAnimation(google.maps.Animation.BOUNCE);
				    if (currentPopup != null)
				    {
				        currentPopup.close();
				        currentPopup = null;
				    }
				    popup.open(specialMap, marker);
				    currentPopup = popup;
				    setTimeout(function(){marker.setAnimation(null);},2500);
				    
			   	});
		        markers.push(marker);
			});
			
			$(document).ready(function(){
				mcOptions = {
					styles: [
						{
							// yellow
							height: 56,
							url: "/themes/xc_theme/img/m2.png",
							width: 56
						}
				]}
			  	if(completeLoading == true)
				{
					$('#loadingImg').fadeIn("slow");
					var markerCluster = new MarkerClusterer(specialMap, markers , mcOptions);
					filterSpecialData(category , offsetSpecial , specialMap);
				}
				else
				{
					$('#loadingImg').fadeOut("slow");
					var markerCluster = new MarkerClusterer(specialMap, markers , mcOptions);
				}
			});
		});		
	}
	
	function filterDataByCity(category , cityName , offsetCities , citiesMap){
		var iconForClustering = "";
		if(category == "Food")
		{
			iconForClustering = "/themes/xc_theme/img/m1.png";
		}
		else if(category == "Fashion")
		{
			iconForClustering = "/themes/xc_theme/img/m3.png";
		}
		else if(category == "Specials")
		{
			iconForClustering = "/themes/xc_theme/img/m2.png";
		}
		var markers = [];
		defaultAjaxCall = jQuery.ajax({
			url: "<?php echo str_replace('&amp;', '&', $this->action('majax_router_filter_Data')); ?>",
			data: {category:category , cityName:cityName , offset:offsetCities}
		}).done(function(response) {
			$.each($.parseJSON(response), function(key,value){
				var info = '<div style="float:left;width:90%"><b>' + value.name + "</b><br />" + value.address + "<br />" + value.district + "<br />" + value.city + "<br />" + value.phones + '</div><div style="float:left;width:10%"><a href="" onclick="currentPopup.close();return false;"><img src="/themes/xc_theme/img/closeButton.gif"></a></div>';
				
				offsetCities ++;
				var latLng = new google.maps.LatLng(value.lat,value.long);
		        var marker = new google.maps.Marker({
		            position: latLng,
		            icon:value.icon,
            		animation: google.maps.Animation.DROP
		        });
		        var popup = new google.maps.InfoWindow(
				{
				    content: info,
				    maxWidth: 300
				});
			
				
				google.maps.event.addListener(marker, "click", function()
				{
					marker.setAnimation(google.maps.Animation.BOUNCE);
				    if (currentPopup != null)
				    {
				        currentPopup.close();
				        currentPopup = null;
				    }
				    popup.open(citiesMap, marker);
				    currentPopup = popup;
				    setTimeout(function(){marker.setAnimation(null);},2500);
				    
			   	});
		        markers.push(marker);
			});
			
			$(document).ready(function(){
				mcOptions = {
					styles: [
						{
							height: 56,
							url: iconForClustering,
							width: 56
						}
				]}
			  	if(completeLoading == true)
				{
					$('#loadingImg').fadeIn("slow");
					if(iconForClustering == "")
					{
						var markerCluster = new MarkerClusterer(citiesMap, markers);
					}
					else
					{
						var markerCluster = new MarkerClusterer(citiesMap, markers , mcOptions);
					}
					filterDataByCity(category , cityName , offsetCities , citiesMap);
				}
				else
				{
					$('#loadingImg').fadeOut("slow");
					var markerCluster = new MarkerClusterer(citiesMap, markers , mcOptions);
				}
			});
		});		
	}
	
	
   	function filterChains(category,cityName)
	{
		switch(category)
		{
			case "All":
				$('#All').removeClass('active').addClass('active');
				$('#Food').removeClass('active');
			  	$('#Fashion').removeClass('active');
			  	$('#Specials').removeClass('active');
			  	
			  	if(cityName == null)
			  	{
			  		$('#mapDefault').fadeIn('slow');
					$('#map').fadeOut('slow');
			  	}
			  	else
			  	{
			  		$('#mapDefault').fadeOut('slow');
			  		$('#map').fadeIn('slow');
			  	}
				// stop the other ajax calls except the start call
				if(foodAjaxCall != null)
				{
					foodAjaxCall.abort();
				}
				if(fashionAjaxCall != null)
				{
					fashionAjaxCall.abort();
				}
				if(specialsAjaxCall != null)
				{
					specialsAjaxCall.abort();
				}
				if(defaultAjaxCall != null)
				{
					defaultAjaxCall.abort();
				}
				
				if(cityName != null)
				{
					var citiesMap = initMap(cityName);
					var offsetCity = 0;
					filterDataByCity(category,cityName,offsetCity,citiesMap);
				}
				
			break;
			case "Food":
				$('#map').fadeIn('slow');
				var offsetFood = 0;
				$('#mapDefault').fadeOut('slow');
				$('#loadingImg').fadeIn("slow");
				$('#Food').removeClass('active').addClass('active');
			  	$('#Fashion').removeClass('active');
			  	$('#Specials').removeClass('active');
			  	$('#All').removeClass('active');
			  	
			  	// stop the other ajax calls except the start call
				if(foodAjaxCall != null)
				{
					foodAjaxCall.abort();
				}
				if(fashionAjaxCall != null)
				{
					fashionAjaxCall.abort();
				}
				if(specialsAjaxCall != null)
				{
					specialsAjaxCall.abort();
				}
				if(defaultAjaxCall != null)
				{
					defaultAjaxCall.abort();
				}
				
				
				if(cityName == null)
				{
					var foodMap = initMap(null);
					filterFoodData("Food" , offsetFood , foodMap);
				}
				else
				{
					var foodMap = initMap(cityName);
					filterDataByCity(category,cityName,offsetFood,foodMap);
				}
		  		
			  	
			break;
			case "Fashion":
				$('#map').fadeIn('slow');
				var offsetFashion = 0;
				$('#mapDefault').fadeOut('slow');
				$('#loadingImg').fadeIn("slow");
				$('#Food').removeClass('active');
			 	$('#Fashion').removeClass('active').addClass('active');
			 	$('#Specials').removeClass('active');
			 	$('#All').removeClass('active');
			 	
				// stop the other ajax calls except the start call
				if(foodAjaxCall != null)
				{
					foodAjaxCall.abort();
				}
				if(fashionAjaxCall != null)
				{
					fashionAjaxCall.abort();
				}
				if(specialsAjaxCall != null)
				{
					specialsAjaxCall.abort();
				}
				if(defaultAjaxCall != null)
				{
					defaultAjaxCall.abort();
				}
				
				
				if(cityName == null)
				{
					var fashionMap = initMap(null);
					filterFashionData("Fashion",offsetFashion , fashionMap);
				}
				else
				{
					var fashionMap = initMap(cityName);
					filterDataByCity(category,cityName,offsetFashion,fashionMap);
				}
			  	
			break;
			case "Specials":
				$('#map').fadeIn('slow');
				var offsetSpecial = 0;
				$('#mapDefault').fadeOut('slow');
				$('#loadingImg').fadeIn("slow");
				$('#Food').removeClass('active');
			  	$('#Fashion').removeClass('active');
			  	$('#Specials').removeClass('active').addClass('active');
				$('#All').removeClass('active');
				
				// stop the other ajax calls except the start call
				if(foodAjaxCall != null)
				{
					foodAjaxCall.abort();
				}
				if(fashionAjaxCall != null)
				{
					fashionAjaxCall.abort();
				}
				if(specialsAjaxCall != null)
				{
					specialsAjaxCall.abort();
				}
				if(defaultAjaxCall != null)
				{
					defaultAjaxCall.abort();
				}
				
				
				if(cityName == null)
				{
					var specialMap = initMap(null);
					filterSpecialData("Specials",offsetSpecial , specialMap);
				}
				else
				{
					var specialMap = initMap(cityName);
					filterDataByCity(category,cityName,offsetSpecial,specialMap);
				}
			  	
			  break;
			default:	
		}
	}
	
	
	function getAllChains()
	{
		$('#All').removeClass('active').addClass('active');
		
		$('#mapDefault').fadeIn();
		$('#map').fadeOut();
		
		$('#loadingDiv').fadeIn("slow");
		$('#loadingImg').fadeIn("slow");
		
		var mapDefault = new google.maps.Map(document.getElementById("mapDefault"), {
			center: new google.maps.LatLng(30.133333, 31.400000),
			zoom: 9,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			mapTypeControlOptions:
			{
			    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
			},
			navigationControl: true,
			navigationControlOptions:
			{
				style: google.maps.NavigationControlStyle.SMALL
			}
		});
		mapDefault.setCenter(new google.maps.LatLng(30.133333,31.400000));
		
		
		jQuery.ajax({
			url: "<?php echo str_replace('&amp;', '&', $this->action('majax_router')); ?>",
			data: {}
		}).done(function(response) {
			getAllRestaurantsStores(limit , 0 , mapDefault);
		});
	}
	
	function getAllRestaurantsStores(limit , offset , mapDefault)
	{
		var dataCount = 0;
		var markers = [];
		
		startCall = jQuery.ajax({
			url: "<?php echo str_replace('&amp;', '&', $this->action('majax_router_rest_stores')); ?>",
			data: {limit : limit , offset : offset},
			success: function(response) {
				var count = 10;
				$.each($.parseJSON(response), function(key,value){
				var info = '<div style="float:left;width:90%"><b>' + value.name + "</b><br />" + value.address + "<br />" + value.district + "<br />" + value.city + "<br />" + value.phones + '</div><div style="float:left;width:10%"><a href="" onclick="currentPopup.close();return false;"><img src="/themes/xc_theme/img/closeButton.gif"></a></div>';
				dataCount ++;
					
				var latLng = new google.maps.LatLng(value.lat,value.long);
		        var marker = new google.maps.Marker({
		            position: latLng,
		            icon : value.icon,
            		animation: google.maps.Animation.DROP
		        });
		        var popup = new google.maps.InfoWindow(
				{
				    content: info,
				    maxWidth: 300
				});
				
					
				google.maps.event.addListener(marker, "click", function()
				{
					marker.setAnimation(google.maps.Animation.BOUNCE);
				    if (currentPopup != null)
				    {
				        currentPopup.close();
				        currentPopup = null;
				    }
				    popup.open(mapDefault, marker);
				    currentPopup = popup;
				    setTimeout(function(){marker.setAnimation(null);},2500);
				    
			   	});
		        markers.push(marker);
				});
			}
		}).done(function() {
			$('#loadingDiv').fadeOut("slow");
			$("#All").fadeIn("slow");
	  		$("#Food").fadeIn("slow");
			$("#Fashion").fadeIn("slow");
			$("#Specials").fadeIn("slow");
			$("#citiesDiv").fadeIn("slow");
				
			$(document).ready(function(){
			  	if(dataCount >= (limit/2))
				{
					var markerCluster = new MarkerClusterer(mapDefault, markers);
					offset = offset + 10;
			  		getAllRestaurantsStores(limit , offset , mapDefault);
				}
				else
				{
					$('#loadingImg').fadeOut("slow");
					completeLoading = false;
				}
			});
		});
	}
	
	jQuery(document).ready(function(){
		getAllChains();
	});
	
   </script>
   	
   <img id="loadingImg" width="20px;" height="20px" style="display:none;position:absolute;margin-left:640px;margin-top:60px;" src="/themes/xc_theme/img/loading.gif">
	<h2>Outlets Finder</h2>
    <ul class="nav nav-pills">
	  <!-- <li class="<?=$clsFood?>"><a href="/outlet-finder?category=506addfe9b6286e82a000015" onclick="ajaxCall();return false;">Food</a></li>
	  <li class="<?=$clsFashion?>"><a href="/outlet-finder?category=506ade239b6286e72a00001c" onclick="ajaxCall();return false;">Fashion</a></li>
	  <li class="<?=$clsOther?>"><a href="/outlet-finder?category=506ade359b62869b70000017" onclick="ajaxCall();return false;">Specials</a></li> -->
	  <li style="display:none" id="All">
	  	<a href="" onclick="filterChains('All');
	  	$('#allCities').show('slow');
	  	$('#foodCities').hide('slow');
	  	$('#fashionCities').hide('slow');
	  	$('#specialsCities').hide('slow');
	  	return false;">All</a>
	  	
	  </li>
	  <li style="display:none" id="Food">
	  	<a href="" onclick="filterChains('Food');
	  	$('#allCities').hide('slow');
	  	$('#foodCities').show('slow');
	  	$('#fashionCities').hide('slow');
	  	$('#specialsCities').hide('slow');
	  	return false;">
	  		<img src="/themes/xc_theme/img/blue.png" width="20px;" height="20px;" style="margin-top:-5px;">
	  		Food
	  	</a>
	  	
	  </li>
	  <li style="display:none" id="Fashion">
	  	<a href="" onclick="filterChains('Fashion');
	  	$('#allCities').hide('slow');
	  	$('#foodCities').hide('slow');
	  	$('#fashionCities').show('slow');
	  	$('#specialsCities').hide('slow');
	  	return false;">
	  		<img src="/themes/xc_theme/img/red.png" width="20px;" height="20px;" style="margin-top:-5px;">
	  		Fashion
	  	</a>
	  	
	  </li>
	  <li style="display:none" id="Specials">
	  	<a href="" onclick="filterChains('Specials');
	  	$('#allCities').hide('slow');
	  	$('#foodCities').hide('slow');
	  	$('#fashionCities').hide('slow');
	  	$('#specialsCities').show('slow');
	  	return false;">
	  		<img src="/themes/xc_theme/img/yellow.png" width="20px;" height="20px;" style="margin-top:-5px;">
	  		Specials
	  	</a>
	  </li>
	</ul>
    <div class="sep"></div>
    
    <div class="hpBlocksContainer clearfix" style="width:100%">
   
    	<div id="mapDefault" style="display:none;width:70%;float:left;height: 400px; border: 0px; padding: 0px;">
    		
    	</div>
    	<div id="map" style="display:none;width: 70%;float:left; height: 400px; border: 0px; padding: 0px;">
    		
    	</div>
    	<div style="float:left;width:20%;display:none" id="citiesDiv">
    		<h3 style="margin-left:10px;">Cities</h3>
    		<ul id="allCities">
		  		<?php 
		  		if(isset($_SESSION['cities']))
				{
					$cities = $_SESSION['cities'];
					foreach ($cities as $key => $value) {
						echo "<li><a href='' onclick=\"filterChains('All','".$value['name']."');return false;\">";
						echo $value['name'];
						echo "</a></li>";
					}
				}
		  		?>
		  	</ul>
    		<ul id="foodCities" style="display:none">
		  		<?php 
		  		if(isset($_SESSION['cities']))
				{
					$cities = $_SESSION['cities'];
					foreach ($cities as $key => $value) {
						echo "<li><a href='' onclick=\"filterChains('Food','".$value['name']."');return false;\">";
						echo $value['name'];
						echo "</a></li>";
					}
				}
		  		?>
		  	</ul>
    		<ul id="fashionCities" style="display:none">
		  		<?php 
		  		if(isset($_SESSION['cities']))
				{
					$cities = $_SESSION['cities'];
					foreach ($cities as $key => $value) {
						echo "<li><a href='' onclick=\"filterChains('Fashion','".$value['name']."');return false;\">";
						echo $value['name'];
						echo "</a></li>";
					}
				}
		  		?>
		  	</ul>
    		<ul id="specialsCities" style="display:none;">
		  		<?php 
		  		if(isset($_SESSION['cities']))
				{
					$cities = $_SESSION['cities'];
					foreach ($cities as $key => $value) {
						echo "<li><a href='' onclick=\"filterChains('Specials','".$value['name']."');return false;\">";
						echo $value['name'];
						echo "</a></li>";
					}
				}
		  		?>
		  	</ul>
		  	
    	</div>
    </div>              