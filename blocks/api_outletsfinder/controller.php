<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

	require_once('libraries/pest/PestJSON.php');

	class ApiOutletsfinderBlockController extends BlockController {
		
		var $pobj;
		
		protected $btDescription = "A simple block for outlets finder";
		protected $btName = "Api Outlets Finder";
		protected $btTable = 'btApiOutlets';
		protected $btInterfaceWidth = "350";
		protected $btInterfaceHeight = "300";
		
		//protected $auth_token = "bb81e7bfda7df5899af4c5316bac9942";
		protected $auth_token = "8ff0096821120a0f2975dc3f0b8ff9ff";
		protected $limit = 9;
		protected $offset = 0;
		protected $count = 0;
		protected $currentPage = 1;
		
		protected $prevPage = 0;
		protected $nextPage = 0;
		
		//protected $foodCategoryId = '5017d2db9b62860159000187';
		//protected $fashionCategoryId = '5017d2eb9b62866b50000169';
		//protected $otherCategoryId = '50290cd09b6286bb41000023';
		
		protected $foodCategoryId = '506addfe9b6286e82a000015';
		protected $fashionCategoryId = '506ade239b6286e72a00001c';
		protected $otherCategoryId = '506ade359b62869b70000017';
		
		protected $category = '506addfe9b6286e82a000015';
		
		protected $apiKey = 'AIzaSyAWb1G31tDUFYEZHGGDRsPNazLWG8gf-lE';
			
		protected $chains = "";
		
		public function view() {
			// clear the sessions before start
			$_SESSION['mergedJsons'] = "";
			$_SESSION['chains'] = "";
			$_SESSION['cities'] = "";
			
			// get cities
			$pest = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
			$cities = $pest->get('/cities?oauth_token='.$this->auth_token.'&limit=all&definition=name');
			$cities = json_decode($cities,true);
			$_SESSION['cities'] = $cities;
			
			// session_destroy();
			//show loading div till the php scripts finishes loading
			echo '<div id="loadingDiv" style="display:none;position:absolute;width:660px;height:400px;margin-top:55px;margin-left:0px;z-index:100;background-color:black;opacity: 0.95;color:white">
					<span style="font-family: times, serif; font-size:20px; font-style:italic;position:absolute;margin-left:180px;margin-top:170px;">
						Loading Data, please be patient ...
					</span>
					<img width="60px;" height="60px" src="/themes/xc_theme/img/loading_transparent.gif" style="margin-left:300px;margin-top:200px;">
				</div>';
				
			// show loading div till the php scripts finishes loading
			// echo '<div id="startingDiv" style="display:block;position:absolute;width:658px;height:400px;margin-top:88px;margin-left:0px;z-index:100;background-color:black;opacity: 0.85;color:white">
					// <span style="font-family: times, serif; font-size:20px; font-style:italic;position:absolute;margin-left:180px;margin-top:170px;">
						// Select Category to display it\'s outlets
					// </span>
				// </div>';
		}

		public function action_majax_router(){
			//get ALL Data
			echo $this->getAllData();exit();
		}

		public function getAllData(){
			$mergedArray = array();
			$pest = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
			$chains = $pest->get('/chains?oauth_token='.$this->auth_token.'&limit=all&definition=categories,collections,images');
			$_SESSION['chains'] = $chains;
			
			return $chains;
		}
		
		public function action_majax_router_rest_stores(){
			$limit = $_GET['limit'];
			$offset = $_GET['offset'];
			//get ALL Data
			echo $this->getAllRestaurantStoresData($limit , $offset);exit();
		}
		
		public function getAllRestaurantStoresData($limit , $offset){
			$mergedArrays = array();
			
			$pestRestaurant = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
			$restaurantsResponse = $pestRestaurant->get('/restaurants?oauth_token='.$this->auth_token.'&definition=location,name,address.street,phones,address.city,address.district,chainId&limit='.$limit.'&offset='.$offset);
			$restaurantsResponse = json_decode($restaurantsResponse,true);
			
			$pestStore = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');
			$storesResponse = $pestStore->get('/stores?oauth_token='.$this->auth_token.'&definition=location,name,address.street,phones,address.city,address.district,chainId&limit='.$limit.'&offset='.$offset);
			
			$storesResponse = json_decode($storesResponse,true);
			
			if($restaurantsResponse && $storesResponse)
			{
				$mergedArrays = array_merge($restaurantsResponse,$storesResponse);
			}
			else if($storesResponse)
			{
				$mergedArrays = array_merge(array(),$storesResponse);
			}
			else if($restaurantsResponse)
			{
				$mergedArrays = array_merge(array(),$restaurantsResponse);
			}
			
			$mergedJsons = $this->generateJsonObject($mergedArrays);
			
			$_SESSION['mergedJsons'] = $_SESSION['mergedJsons'] . $mergedJsons;
			$_SESSION['mergedJsons'] = str_replace("[]", "", $_SESSION['mergedJsons']);
			$_SESSION['mergedJsons'] = str_replace("][", ",", $_SESSION['mergedJsons']);
			return $mergedJsons;
		}
		
		public function generateJsonObject($mergedArrays)
		{
			$dataArray = array();
			foreach ($mergedArrays as $keyMerged => $valueMerged) 
			{
				$long = $valueMerged['location']['loc']['lon'];
        		$lat = $valueMerged['location']['loc']['lat'];
				
				$name = $valueMerged['name'];
				$phones = isset($valueMerged['phones'])? $valueMerged['phones']: array();
				$desc = $valueMerged['address']['street'];
				$city = $valueMerged['address']['city']['name'];
				$district = $valueMerged['address']['district']['name'];
				
        		$name = str_replace("'", "",$name);
				$name = str_replace("\r", "",$name);
				$name = str_replace("\n", "",$name);
				
        		$desc = str_replace("'", "",$desc);
				$desc = str_replace("\r", "",$desc);
				$desc = str_replace("\n", "",$desc);
				
				$city = str_replace("'", "",$city);
				$city = str_replace("\r", "",$city);
				$city = str_replace("\n", "",$city);
				
				$district = str_replace("'", "",$district);
				$district = str_replace("\r", "",$district);
				$district = str_replace("\n", "",$district);
				
				$chainData = $this->getChainDataBasedOnRestaurantStore($valueMerged['chainId']['_id']);
				
				if($chainData["category"] =="Food")
				{
					$icon = "/themes/xc_theme/img/blue.png";
				}
				else if($chainData["category"] =="Fashion"){
					$icon = "/themes/xc_theme/img/red.png";
				}
				else {
					$icon = "/themes/xc_theme/img/yellow.png";
				}
				
        		array_push($dataArray,array("lat"=>$lat,
        									"long"=>$long,
        									"name"=>$name,
        									"address"=>$desc,
        									"phones"=>implode(",",$phones),
        									"city"=>$city,
        									"district"=>$district,
        									"icon"=>$icon,
        									"category"=>$chainData["category"],
        									"collection"=>$chainData["collection"],
        									"label"=>$chainData["icon"]
											)
							);
        	}
	    	return json_encode($dataArray);
		}

		public function getChainDataBasedOnRestaurantStore($chainId){
			$chains = json_decode($_SESSION['chains']);
			$category = "";
			$collection = "";
			$icon = "";
			foreach ($chains as $key => $value) {
				if($value->_id == $chainId)
				{
					$category = array_pop($value->categories)->name;
					$collection = array_pop($value->collections);
					foreach ($value->images as $keyImage => $valueImage) {
						if($valueImage->label == "logocircle")
							$icon = $valueImage->url;
					}
				}
			}
			return array("category"=>$category,"collection"=>$collection,"icon"=>$icon);
		}
		
		public function action_majax_router_filter_Data(){
			$category = $_GET['category'];
			// $limit = $_GET['limit'];
			$offset = $_GET['offset'];
			$cityName = null;
			if(isset($_GET['cityName']))
			{
				$cityName = $_GET['cityName'];
			}
			//get Data based on category Only
			echo $this->filterData($category , $cityName , $offset);exit();
		}
		
		public function filterData($category , $cityName , $offset){
			$filterdData = array();
			$mergedJsons = json_decode($_SESSION['mergedJsons']);
			
			if(($category == "Food" || $category == "Fashion" || $category == "Specials") && $cityName == null)
			{
				foreach ($mergedJsons as $key => $value) {
					if($value->category == $category)
					{
						// filtered data based on the category
						array_push($filterdData,$value);
					}
				}
				$filterdData = array_slice($filterdData, $offset);
			}
			// trying to filter data by city
			else if($category == "All")
			{
				foreach ($mergedJsons as $key => $value) {
					if($value->city == $cityName)
					{
						// filtered data based on the category
						array_push($filterdData,$value);
					}
				}
				$filterdData = array_slice($filterdData, $offset);
			}
			else {
				foreach ($mergedJsons as $key => $value) {
					if($value->city == $cityName && $value->category == $category)
					{
						// filtered data based on the category
						array_push($filterdData,$value);
					}
				}
				$filterdData = array_slice($filterdData, $offset);
			}
			return json_encode($filterdData);
		}
	}
	
?>