<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

	require_once('libraries/pest/PestJSON.php');

	class ApiVouchersBlockController extends BlockController {
		
		var $pobj;
		
		protected $btDescription = "A simple testing block for developers.";
		protected $btName = "Api Vouchers";
		protected $btTable = 'btApiVouchers';
		protected $btInterfaceWidth = "350";
		protected $btInterfaceHeight = "300";
		
		//protected $auth_token = "bb81e7bfda7df5899af4c5316bac9942";
		protected $auth_token = "8ff0096821120a0f2975dc3f0b8ff9ff";
		
		protected $prevPage = 0;
		protected $nextPage = 0;
		
		//protected $foodCategoryId = '5017d2db9b62860159000187';
		//protected $fashionCategoryId = '5017d2eb9b62866b50000169';
		//protected $otherCategoryId = '50290cd09b6286bb41000023';
		
		protected $foodCategoryId = '506addfe9b6286e82a000015';
		protected $fashionCategoryId = '506ade239b6286e72a00001c';
		protected $otherCategoryId = '506ade359b62869b70000017';
		
		protected $category = '506addfe9b6286e82a000015';
		public $language;
		
		
		private function getLanguage(){
			//this will get language of the current page
			$lh = Loader::helper('section', 'multilingual');
			
			if($lh->getLanguage() == "ar_EG")
		    {
		       $this->language = 'ar';
		    }
		    else{
			    $this->language = 'en';
		    }
		}
		public function view() {
			$this->getLanguage();
		
			if(isset($_GET['offset']))
				$this->offset = $_GET['offset'];
				
			if(isset($_GET['category']))
				$this->category = $_GET['category'];
			
			$pest = new Pest('http://api.olitintl.com/APIPlatform/index.php/Version2');

			//get vouchers
			$resp = $pest->get('/vouchers?oauth_token='.$this->auth_token.'&limit=all&language='.$this->language);
			$vouchers = json_decode($resp);
			
			$this->fetchVouchers($vouchers);
			
			$this->set('auth_token',$this->auth_token);
			
			
		}
		
		public function fetchVouchers($vouchers){
			$foodVouchers = array();
			$fashionVouchers = array();
			$specialsVouchers = array();
			
			foreach($vouchers as $voucher){
				if(isset($voucher->chainId->categories)){
					foreach($voucher->chainId->categories as $voucherCategory){
						if($voucherCategory->_id == $this->foodCategoryId){
							array_push($foodVouchers, $voucher);	
						}
						elseif($voucherCategory->_id == $this->fashionCategoryId){
							array_push($fashionVouchers, $voucher);
						}
						elseif($voucherCategory->_id == $this->otherCategoryId){
							array_push($specialsVouchers, $voucher);
						}
					}
				}
				
			}
			
			$this->set('foodVouchers', $foodVouchers);
			$this->set('fashionVouchers', $fashionVouchers);
			$this->set('specialsVouchers', $specialsVouchers);
			
			
		}
		
		
	}
	
?>