<!-- alternating-table-color-rows -->
<style type="text/css">
.myTable { width:100%; border-collapse:collapse;  }
.myTable td { padding:8px; border:#999 1px solid; }
 
.alternateRowTable tr:nth-child(even) { /*(even) or (2n 0)*/
	background: #EBECEE;
}
.alternateRowTable tr:nth-child(odd) { /*(odd) or (2n 1)*/
	background: #ffffff;
}
</style>

<h2><?php echo $controller->language == 'en' ? "Vouchers" : "قسائم"; ?></h2>
<div class="tabbable">
<ul class="nav nav-pills">
  <li class="active"><a href="#pane1" data-toggle="tab"><?php echo $controller->language == 'en' ? "Restaurants & Cafès" : "مطاعم و كافيهات"; ?></a></li>
  <li><a href="#pane2" data-toggle="tab"><?php echo $controller->language == 'en' ? "Fashion" : "الموضه"; ?></a></li>
  <li><a href="#pane3" data-toggle="tab"><?php echo $controller->language == 'en' ? "Special" : "أخرى"; ?></a></li>
</ul>
   
<div class="tab-content">
<!--Food Table-->

<div class='tab-pane active' id='pane1' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:12px;'>
	<!-- <h3>Food</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="foodTableContent" width="100%" name="foodTableContent">
		<tr>
			<th><?php echo $controller->language == 'en' ? "Logo" : "شعار"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Outlet" : "منفذ"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Instant Discount" : "خصم فوري"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Description" : "وصف"; ?></th>
		</tr>
		<?php
			$foodCounter = 2;
			foreach ($foodVouchers as $key => $voucher) {
        	
        		$path = $this->getThemePath().'/dummy/truLogohp.jpg';
        		$path2 = $this->getThemePath().'/dummy/truhp.jpg';
        		
            	if(isset($voucher->chainId->images))
            	{
            		foreach($voucher->chainId->images as $img)
            		{
/*             			var_dump($img); */
            			if(isset($img))
            			{
            				if(isset($img->path))
            				{
            					if(isset($img->label))
            					{
            						switch($img->label)
            						{
                						case "logocircle":
								        	$path = $img->path;
								        	$path = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path.'&width=72&height=72&gravity=center&oauth_token='.$auth_token;
								        break;
								    
								        case "voucher":
								        	$path2 = $img->path;					                					
								        	$path2 = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path2.'&gravity=center&width=180&height=120&oauth_token='.$auth_token;
								        break;
								    
            						}
            					}
            						
            				}
            			}
            			
            			
            					

            		}
            	}

				echo "<tr>";
				echo "<td style='text-align:center;' width='72'>";
				echo "<img src='$path;' alt='truLogohp' width='72' height='72' />";
				echo "</td>";
				echo "<td style='text-align:center;'>";
				echo $voucher->name;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->value;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->description;
				echo "</td>";
				echo "</tr>";   
				
				$foodCounter ++;
			}
	   	?>
	</table>
</div>

<!--Fashion Table-->
<div class='tab-pane' id='pane2' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:12px;'>
	<!-- <h3>Food</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="foodTableContent" width="100%" name="foodTableContent">
		<tr>
			<th><?php echo $controller->language == 'en' ? "Logo" : "شعار"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Outlet" : "منفذ"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Instant Discount" : "خصم فوري"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Description" : "وصف"; ?></th>
		</tr>
		<?php
			$fashionCounter = 2;
			foreach ($fashionVouchers as $key => $voucher) {
        	
        		$path = $this->getThemePath().'/dummy/truLogohp.jpg';
        		$path2 = $this->getThemePath().'/dummy/truhp.jpg';
        		
            	if(isset($voucher->chainId->images))
            	{
            		foreach($voucher->chainId->images as $img)
            		{
/*             			var_dump($img); */
            			if(isset($img))
            			{
            				if(isset($img->path))
            				{
            					if(isset($img->label))
            					{
            						switch($img->label)
            						{
                						case "logocircle":
								        	$path = $img->path;
								        	$path = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path.'&width=72&height=72&gravity=center&oauth_token='.$auth_token;
								        break;
								    
								        case "voucher":
								        	$path2 = $img->path;					                					
								        	$path2 = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path2.'&gravity=center&width=180&height=120&oauth_token='.$auth_token;
								        break;
								    
            						}
            					}
            						
            				}
            			}
            			
            			
            					

            		}
            	}

				echo "<tr>";
				echo "<td style='text-align:center;' width='72'>";
				echo "<img src='$path;' alt='truLogohp' width='72' height='72' />";
				echo "</td>";
				echo "<td style='text-align:center;'>";
				echo $voucher->name;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->value;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->description;
				echo "</td>";
				echo "</tr>";   
			}
	   	?>
	</table>
</div>


<!--Special Table-->     
<div class='tab-pane' id='pane3' style='background:#f2f2f2;width:90%;padding:10px;border-radius:12px;border-top-left-radius:12px;'>
	<!-- <h3>Food</h3> -->
	<table class="alternateRowTable" border="2" cellpadding="2" cellspacing="2" id="foodTableContent" width="100%" name="foodTableContent">
		<tr>
			<th><?php echo $controller->language == 'en' ? "Logo" : "شعار"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Outlet" : "منفذ"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Instant Discount" : "خصم فوري"; ?></th>
			<th><?php echo $controller->language == 'en' ? "Description" : "وصف"; ?></th>		</tr>
		<?php
			foreach ($specialsVouchers as $key => $voucher) {
        	
        		$path = $this->getThemePath().'/dummy/truLogohp.jpg';
        		$path2 = $this->getThemePath().'/dummy/truhp.jpg';
        		
            	if(isset($voucher->chainId->images))
            	{
            		foreach($voucher->chainId->images as $img)
            		{
/*             			var_dump($img); */
            			if(isset($img))
            			{
            				if(isset($img->path))
            				{
            					if(isset($img->label))
            					{
            						switch($img->label)
            						{
                						case "logocircle":
								        	$path = $img->path;
								        	$path = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path.'&width=72&height=72&gravity=center&oauth_token='.$auth_token;
								        break;
								    
								        case "voucher":
								        	$path2 = $img->path;					                					
								        	$path2 = 'http://api.olitintl.com/APIPlatform/index.php/getImage?image='.$path2.'&gravity=center&width=180&height=120&oauth_token='.$auth_token;
								        break;
								    
            						}
            					}
            						
            				}
            			}
            			
            			
            					

            		}
            	}

				echo "<tr>";
				echo "<td style='text-align:center;' width='72'>";
				echo "<img src='$path;' alt='truLogohp' width='72' height='72' />";
				echo "</td>";
				echo "<td style='text-align:center;'>";
				echo $voucher->name;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->value;
				echo "</td>";
				echo "<td style='text-align:center;' class='privilegeValue'>";
				echo $voucher->description;
				echo "</td>";
				echo "</tr>";   
			}
	   	?>
	</table>
</div>  <!--Special Table-->     
</div><!-- /.tab-content -->
</div><!-- /.tabbable -->

<!--
<div class="pagination clearfix">
	  <ul>
	  	<li><a href="?offset=<?=$prevPage;?>">Prev</a></li>
	  	<?php for($i=1;$i<=$totalPages;$i++): ?>
	  		<?php
	  			$cls = '';
	  			if($currentPage == $i) 
		  			$cls = 'class="active"';
	  		?>
		    <li <?=$cls?>><a href="?offset=<?=($i-1)*$limit?>"><?=$i?></a></li>
	    <?php endFor;?>
	    <li><a href="?offset=<?=$nextPage?>">Next</a></li>
	  </ul>
</div>
-->
