<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>


        <div class="row">
	        <div class="span9" style="padding-left:20px;">
	        	<div class="row">
		            <div class="span9">
		                <h2>
		                <?php
			                echo $c->getCollectionName();
			            ?>
			            </h2>
			            
			            
		                <div class="sep"></div>
		                
		                <div class="span8" style="padding-top:20px;padding-bottom:20px;">
		                <?php
				    		$a = new Area('Page Content');
				    		//$a->setBlockLimit(1);
				    		$a->display($c);
						?>
		                </div>
		                
		                <div class="clearfix" style="visibility:hidden;">breaker!</div>
		                
		                <div class="span8" style="padding-top:20px;padding-bottom:20px;">
		                <?php
				    		$a = new Area('Page Content 2');
				    		//$a->setBlockLimit(1);
				    		$a->display($c);
						?>
		                </div>
		                
		                
		                <div class="clearfix" style="visibility:hidden;">breaker!</div>
		                
		                <div class="sep"></div>
		            </div>
		        </div><!-- end row-->
		
		        
		        <div style="height:20px;"></div>
		        
		        <div class="sep"></div>
		        
		        <div class="row">
		            <div class="span9"><img src="<?=$this->getThemePath()?>/dummy/bottomad.jpg" alt="bottomad"></div>
		        </div><!-- end row-->
		        
		        <div style="height:20px;"></div>
		        
	        </div>
	        
	        <div class="span3 hpSideBar" style="margin-left:-20px;padding-top:20px;width:239px;border-left:1px solid #666666;">
	        	<?php $this->inc('elements/side.php'); ?>
	        </div>

	        
        </div>
        
        
    <?php $this->inc('elements/footer.php'); ?>