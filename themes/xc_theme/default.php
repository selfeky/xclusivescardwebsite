<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>


        <div class="row">
	        <div class="span9" style="padding-left:20px;">
	        	<div class="row">
		            <div class="span9">
		                <!--<h2>Hot deals</h2>
		                <div class="sep"></div>-->
		                
		                <div class="hpBlocksContainer clearFix">
		               
			                <div class="hpBlocks">
				               
					                 <?php
							    		$a = new Area('Merchants');
							    		$a->setBlockLimit(1);
							    		$a->display($c);
									?>
				               
			                </div>
			                
			                <div class="hpBlocks">
				                <?php
							    		$a = new Area('Vouchers');
							    		$a->setBlockLimit(1);
							    		$a->display($c);
									?>
			                </div>
			                
			                <div class="hpBlocks">
			                	<?php
							    		$a = new Area('Calculator');
							    		$a->setBlockLimit(1);
							    		$a->display($c);
									?>
			                </div>
			                
		                </div>
		                
		                <div style="visibility:hidden;">breaker!</div>
		                
		                <div class="sep"></div>
		            </div>
		        </div><!-- end row-->
		
		        <div class="row">
		            <div class="span9">
		            <h2>Featured</h2>
		            
		            <?php
							    		$a = new Area('Featured');
							    		$a->setBlockLimit(1);
							    		$a->display($c);
						?>
		           
		            </div>
		        </div><!-- end row-->
		        
		        <div style="height:20px;"></div>
		        
		        <div class="sep"></div>
		        
		        <div class="row">
		            <div class="span9">
		            	<?php
							    		$a = new Area('Ads');
							    		$a->setBlockLimit(1);
							    		$a->display($c);
						?>
		            </div>

		        </div><!-- end row-->
		        
		        <div style="height:60px;"></div>    
		              
		        <div class="row">
		            <div class="span9" style="padding-left:355px;">
		            	<a href="https://itunes.apple.com/eg/app/xclusivescard/id639390165?mt=8" target="_blank"> <img src="/themes/xc_theme/img/appstore.jpeg"> </a>
		            	<a href="https://play.google.com/store/apps/details?id=com.olit.xclusivescard" target="_blank"> <img src="/themes/xc_theme/img/googleplay.jpeg"> </a>
		            </div>
		        </div>
		        
		        <div style="height:20px;"></div>
		        
	        </div>
	        
	        <div class="span3 hpSideBar" style="margin-left:-20px;padding-top:20px;width:239px;border-left:1px solid #666666;">
	        	<?php $this->inc('elements/side.php'); ?>
	        </div>
	        
        </div>
        
        
    <?php $this->inc('elements/footer.php'); ?>