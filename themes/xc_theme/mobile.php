<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<?php Loader::element ( 'header_required' ); ?>

    <meta charset="utf-8">

    <title>Xclusives Card</title>
    <meta property="og:image" content="http://www.xclusivescard.com/themes/xc_theme/img/logo.png"/>
    <meta property="og:title" content="Xclusives Card"/>
    <!--<meta property="og:url" content="http://www.xclusivescard.com"/>-->
    <meta property="og:site_name" content="Xclusives Card"/>
    <meta property="og:type" content="website"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""><!-- Le styles -->

</head>

<body>
	<div style="padding:10px;">
					<?php
						$a = new Area('text');
						$a->setBlockLimit(1);
						$a->display($c);
					?>
	</div>		
	
	<?php Loader::element('footer_required'); ?>
    
</body>
</html>