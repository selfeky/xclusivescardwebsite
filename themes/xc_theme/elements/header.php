<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<?php Loader::element ( 'header_required' ); ?>

    <meta charset="utf-8">

    <title>Xclusives Card</title>
    <meta property="og:image" content="http://www.xclusivescard.com/themes/xc_theme/img/logo.png"/>
    <meta property="og:title" content="Xclusives Card"/>
    <!--<meta property="og:url" content="http://www.xclusivescard.com"/>-->
    <meta property="og:site_name" content="Xclusives Card"/>
    <meta property="og:type" content="website"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""><!-- Le styles -->
    <link href="<?=$this->getThemePath()?>/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?=$this->getThemePath()?>/css/style.css" rel="stylesheet" type="text/css"><!--<link href="/css/bootstrap-responsive.css" rel="stylesheet">-->
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <!--
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    -->
    <?php
    	$navcls = "navbar-fixed-top";
    	$u = new User(); //gets current user object
    	if($u->isLoggedIn()) {
	?>
    <style>
    .navbar-fixed-top {top:50px;}
    body {padding-top:40px;}
    </style>
	<?php }?>
    <?php global $c; if ($c->isEditMode()) { $navcls = ""; } ?>  
    
     <!-- Script for google maps -->
   	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
   	<script type="text/javascript">
      var script = '<script type="text/javascript" src="<?=$this->getThemePath()?>/js/markerclusterer';
      if (document.location.search.indexOf('compiled') !== -1) {
        script += '_compiled';
      }
      script += '.js"><' + '/script>';
      document.write(script);
      // var $j = jQuery.noConflict();
   	</script>
	<script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
</head>

<body>
	<?php 
	//this will get language of the current page
	$lh = Loader::helper('section', 'multilingual');
	
	if($lh->getLanguage() == "ar_EG")
    {
       $href = "http://xclusivescard.com/ar";
    }
    else{
	   $href = "http://xclusivescard.com";
    }
	?>

    <div class="navbar <?=$navcls?>">
        <div class="navbar-inner">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"></a> <!-- Be sure to leave the brand out there if you want it shown -->
                        <a class="brand" href=<?php echo $href;?>>
	                        <?php
				    			$a = new Area('Logo');
				    			//$a->setBlockLimit(1);
				    			$a->display($c);
							?>
	                        <!--<img src="<?=$this->getThemePath()?>/img/logo.png" alt="logo" width="178" height="39" />-->
                        </a> <!-- Everything you want hidden at 940px or less, place within here -->
                        
			                <?php
			    			$a = new Area('Nav Bar');
			    			$a->setBlockLimit(1);
							$a->display($c);
							?>

                        <div class="nav-collapse">
                            <!-- .nav, .navbar-search, .navbar-form, etc -->

                            <ul class="nav-pills" style="margin:0;list-style:none;">
                                <li class="dropdown" id="mLang">
                                <?php 
									//this will get language of the current page
									$lh = Loader::helper('section', 'multilingual');
									
									if($lh->getLanguage() == "ar_EG")
								    {
							    ?>
								 <a class="dropdown-toggle language ع" data-toggle="dropdown" href="#">ع</a>								       
							    <?php  }
								    else{
								 ?>
								 <a class="dropdown-toggle language ع" data-toggle="dropdown" href="#">en</a>
								 <?php } ?>
                                    <ul class="dropdown-menu">
                                        <li><a href="http://xclusivescard.com/ar">Arabic</a></li>
                                        <li><a href="http://xclusivescard.com/en">English</a></li>
                                    </ul>
                                </li>
                                 <form method="get" target="_blank" action="<?=$this->getThemePath()?>/files/Xc booklet 01-2014.pdf" style="margin: 0 0 0px;">
                                	 <input type="image" src="<?=$this->getThemePath()?>/img/btn_download_pdf.png" style="margin-top:5px;margin-bottom:5px;">
                                 </form>
                            </ul>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container wbgc">
        <!-- container -->

        <div class="row">
            <div class="span12">
            		<!--
<div class="alert" style="margin-bottom:0;">
					  <button class="close" data-dismiss="alert">×</button>
					  <strong>ALert!</strong> Website is currently beta.
					</div>
		
-->
                  
            	            	<?php
				
					    	$a = new Area('Slider');
				    	$a->setBlockLimit(1);
						$a->display($c);
					?>
					
				<!--
                <div id="myCarousel" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="active item"><img src="<?=$this->getThemePath()?>/dummy/slider.jpg"></div>
                        <div class="item"><img src="<?=$this->getThemePath()?>/dummy/slider.jpg"></div>
                    </div>
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a> <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
                -->	
            </div>
        </div><!-- end row -->
