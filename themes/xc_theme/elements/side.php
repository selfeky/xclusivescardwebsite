<style type="text/css">
		.applyImg{
			cursor: pointer;
			}
		
		/* Z-index of #mask must lower than #boxes .window */
		#mask {
		  position:absolute;
		  left:0;
		  top:0;
		  z-index:9000;
		  background-color:#000;
		  display:none;
		}
		  
		.window {
		  position:fixed;
		  width:700px;
		  height:700px;
/* 		  display:none; */
		  z-index:9999;
		  padding:20px;
		}
		/* Customize your modal window here, you can add background image too */
/*
		.form {
		  background:url(/themes/xc_theme/img/en/form_background.png) no-repeat 0 0 transparent;
		  width:700px; 
		  height:700px;
		  background-repeat:no-repeat;
		  background-color: #ffffff;
		}
*/
</style>


<?php  defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="sideBlock" style="padding-bottom: 10px;padding-left:20px;">
	<?php 
	//this will get language of the current page
	$lh = Loader::helper('section', 'multilingual');
	
	if($lh->getLanguage() == "ar_EG")
    {
       echo "<h2>تابعونا</h2>";
    }
    else{
	   echo "<h2>Follow Us</h2>";
    }
	?>

<!--
	$a = new Area('Follow Us');
	$a->setBlockLimit(2);
	$a->display($c);
-->

	<a href="https://www.facebook.com/Xclusivescard" target="_blank">
		<img src="/themes/xc_theme/img/socialIcons/facebook.png" alt="facebook" height="35" width="35">
	</a>
	<a href="https://twitter.com/Xclusives_Card" target="_blank">
		<img src="/themes/xc_theme/img/socialIcons/twitter.png" alt="twitter" height="35" width="35">
	</a>
	<a href="http://www.instagram.com/xclusivescard" target="_blank">
		<img src="/themes/xc_theme/img/socialIcons/instagram.png" alt="linkedIn" height="35" width="35">
	</a>
	<a href="https://plus.google.com/b/115948666670930774512/115948666670930774512/posts" target="_blank">
		<img src="/themes/xc_theme/img/socialIcons/googleplus.png" alt="google-plus" height="35" width="35">
	</a>
	<a href="http://www.linkedin.com/company/xclusives-card?trk=nav_account_sub_nav_company_admin" target="_blank">
		<img src="/themes/xc_theme/img/socialIcons/linkedin.png" alt="linkedIn" height="35" width="35">
	</a>
		
</div>
<div id="apply" class="sideBlock applyImg" onclick="openWin();" style="padding-left:0px;">
	<?php
    	$a = new Area('Get Your Card');
    	$a->setBlockLimit(1);
		$a->display($c);
		?>
</div>
<!--
<div id="form" class="window form">
	<a href="#" class="close">
		<img src="/themes/xc_theme/img/monotone_close_exit_delete.png"width="42" height="42">
	</a>
	<?php
    	$a = new GlobalArea('Get Your Card form');
    	$a->setBlockLimit(1);
		$a->display($c);
		?>
</div>
-->
<div id="mask" style="display: none; opacity: 0.8;"></div>
<div class="sideBlock" style="padding-left:0px;">
	<div class="sideBlockSpacer"></div>
	<?php
		$a = new Area('Hotline');
		$a->setBlockLimit(1);
		$a->display($c);
	?>
</div>
<!--
<div class="sideBlock">
	<div class="sideBlockSpacer"></div>
	<?php if($lh->getLanguage() == "ar_EG")
    {
       echo "<h2>خريطة المحلات</h2>";
    }
    else{
	   echo "<h2>Outlet Finder</h2>";
    } ?>
	<?php
		$a = new Area('Outlet Finder');
		$a->setBlockLimit(1);
		$a->display($c);
	?>
	<div style="clear:both;" class="sideBlockSpacer"></div>
	<div style="clear:both;" class="sideBlockSpacer"></div>
</div>
-->
				
<div class="sideBlock">
	<div class="sideBlockSpacer"></div>
	<?php if($lh->getLanguage() == "ar_EG")
    {
       echo "<h2>إنضم</h2>";
    }
    else{
	   echo "<h2>Join</h2>";
    } ?>
	<?php
		$a = new Area('Newsletter');
		$a->setBlockLimit(1);
		$a->display($c);
	?>
</div>

<script>

function openWin()
{
	
	myWindow = window.open("http://xclusivescard.com/<?php echo $lh->getLanguage() == 'en_US' ? "en" : "ar"; ?>/getyourcard","", "width=800,height=757");
/* 	myWindow = window.open("http://www.instagram.com/xclusivescard"); */
}

/*
$(document).ready(function(){
    
  $("#apply").click(function(){
	    
	        //Get the screen height and width
	        var maskHeight = $(document).height();
	        var maskWidth = $(window).width();

	        //Set height and width to mask to fill up the whole screen
	        $('#mask').css({'width':maskWidth,'height':maskHeight});
	        
	        //transition effect        
	        $('#mask').fadeIn(1000);    
	        $('#mask').fadeTo("slow",0.8);    
	    
	        //Get the window height and width
	        var winH = 796;
	        var winW = $(window).width();
	              
	        //Set the popup window to center
	        $("#form").css('top',  winH/2-$("#form").height()/2);
	        $("#form").css('left', winW/2-$("#form").width()/2);
	    
	        //transition effect
	        $("#form").fadeIn(2000); 
	  });
	  
	  //if close button is clicked
    $('.window .close').click(function (e) {
        //Cancel the link behavior
        e.preventDefault();
        $('#mask, .window').hide();
    });        
    
    //if mask is clicked
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });
});
*/
</script>